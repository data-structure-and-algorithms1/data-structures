package com.company.algorithm.recursion;

public class DecimalToBinaryProblem {

    /**
     * deyeri 0 1 lere cevirmek
     */
    public static void main(String[] args) {

        DecimalToBinaryProblem decimalToBinaryProblem = new DecimalToBinaryProblem();
        int a = decimalToBinaryProblem.decimalToBinary(16);
        System.out.println(a);
    }

    public int decimalToBinary(int n){
        if(n == 0){
            return 0;
        }
        return n%2 + 10*decimalToBinary(n/2);
    }
}

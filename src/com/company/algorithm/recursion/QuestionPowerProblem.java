package com.company.algorithm.recursion;

public class QuestionPowerProblem {

    public static void main(String[] args) {
        QuestionPowerProblem powerProblem = new QuestionPowerProblem();
        int power = powerProblem.power(2,5);
        System.out.println(power);

    }

    public int power(int base, int exp){
        if(exp == 0){
            return 1;
        }
        return base * power(base, exp-1);
    }
}

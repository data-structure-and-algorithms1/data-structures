package com.company.algorithm.recursion;

public class CapitalizeWordSolution {
    public static void main(String[] args) {
        System.out.println(capitalizeWord("salam"));
    }

    public static String capitalizeWord(String word) {
        if (word.isEmpty()) return word;
        char chr = word.charAt(word.length() - 1);
        if (word.length() == 1) {
            return Character.toString(Character.toUpperCase(chr));
        }
        return capitalizeWord(word.substring(0, word.length() - 1)) + chr;
    }
}

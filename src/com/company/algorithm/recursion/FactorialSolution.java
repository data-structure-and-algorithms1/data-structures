package com.company.algorithm.recursion;

public class FactorialSolution {

    public static void main(String[] args) {
        FactorialSolution factorialSolution = new FactorialSolution();
        int factorial = factorialSolution.factorial(5);
        System.out.println(factorial);
    }

    public int factorial(int num){
        if(num <= 1){
            return 1;
        }
        return num * factorial(num - 1);
    }
}

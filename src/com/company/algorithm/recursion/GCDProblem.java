package com.company.algorithm.recursion;

// Greatest Common Divisor
public class GCDProblem {

    public static void main(String[] args) {

        GCDProblem gcdProblem = new GCDProblem();
        int gcd = gcdProblem.gcd(8, 4);
        System.out.println(gcd);
    }

    public int gcd(int a, int b) {
        if (a < 0 || b < 0){
            return -1;
        }
            if (b == 0) {
                return a;
            }
        return gcd(b, a % b);
    }
}

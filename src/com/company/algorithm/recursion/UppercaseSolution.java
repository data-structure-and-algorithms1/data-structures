package com.company.algorithm.recursion;

public class UppercaseSolution {

    public static void main(String[] args) {

        UppercaseSolution uppercaseSolution = new UppercaseSolution();
        System.out.println(uppercaseSolution.firstUpperCase("salAm"));
    }

    public char firstUpperCase(String str){
        if(str.isEmpty()) return ' ';
        if(Character.isUpperCase(str.charAt(0))) {
            return str.charAt(0);
        }
        else {
            return firstUpperCase(str.substring(1));
        }
    }
}

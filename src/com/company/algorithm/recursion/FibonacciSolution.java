package com.company.algorithm.recursion;

public class FibonacciSolution {

    public static void main(String[] args) {
        FibonacciSolution solution = new FibonacciSolution();
        int fib = solution.fibonacci(28);
        System.out.println(fib);
    }

    private int fibonacci(int i) {
        if(i < 0) return -1;
        if(i ==0 || i == 1) return i;
        return fibonacci(i-1) + fibonacci(i-2);
    }
}

package com.company.algorithm.recursion;

public class ValueIsPalindrome {

    public static void main(String[] args) {
        ValueIsPalindrome valueIsPalindrome = new ValueIsPalindrome();
        System.out.println(valueIsPalindrome.isPalindrome("SOS"));
    }

    public boolean isPalindrome(String s) {
        if (s.length() == 0 || s.length() == 1)
            return true;
        if (s.charAt(0) == s.charAt(s.length() - 1))
            return isPalindrome(s.substring(1, s.length() - 1));
        return false;
    }

}

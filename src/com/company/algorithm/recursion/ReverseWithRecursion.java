package com.company.algorithm.recursion;

public class ReverseWithRecursion {

    public static void main(String[] args) {

        ReverseWithRecursion recursion = new ReverseWithRecursion();
        System.out.println(recursion.reverse("Hello"));
    }

    public String reverse (String str){
        if(str.isEmpty()){
            return str;
        }
        return reverse(str.substring(1)) + str.charAt(0);
    }
}

package com.company.algorithm.recursion;

public class ProductOfArrayProblem {

    public static void main(String[] args) {
        int [] A = {1,2,3,4,5};
        ProductOfArrayProblem productOfArrayProblem = new ProductOfArrayProblem();
        int sum = productOfArrayProblem.productOfArray(A,A.length);
        System.out.println(sum);
    }

    public int productOfArray(int A[], int N) {
        if (N <= 0)
            return 1;
        return (productOfArray(A, N - 1) * A[N - 1]);
    }
}

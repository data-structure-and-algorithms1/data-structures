package com.company.algorithm.recursion;

public class SumOfDigitsInstance {

    public static void main(String[] args) {
        SumOfDigitsInstance instance = new SumOfDigitsInstance();
        int sum = instance.sumOfDigits(1000);
        System.out.println(sum);
    }

    public int sumOfDigits(int n) {
        if (n == 0 || n < 0) {
            return 0;
        }
        return n % 10 + sumOfDigits(n / 10);
    }
}

package com.company.algorithm.recursion;

public class RecursiveRange {

    public static void main(String[] args) {
        RecursiveRange recursiveRange = new RecursiveRange();
        int range = recursiveRange.range(10);
        System.out.println(range);
    }

    public int range(int num){
        if(num <= 0) return 0;
        return num + range(num - 1);
    }
}

package com.company.algorithm.sort.merge;

public class MergeSort {

    public static int[] buildLeftArray(int[] A, int left, int middle) {
        int[] leftArray = new int[middle - left + 2];
        if (middle - left + 1 >= 0) System.arraycopy(A, left, leftArray, 0, middle - left + 1);
        leftArray[middle - left + 1] = Integer.MAX_VALUE;
        return leftArray;
    }

    public static int[] buildRightArray(int[] A, int middle, int right) {
        int[] rightArray = new int[right - middle + 1];
        for (int i = 0; i < right - middle; i++) {
            rightArray[i] = A[middle + 1 + i];
        }
        rightArray[right - middle] = Integer.MAX_VALUE;
        return rightArray;
    }

    public static void merge(int[] A, int left, int middle, int right) {
        int[] leftArray = buildLeftArray(A, left, middle);
        int[] rightArray = buildRightArray(A, middle, right);

        int i = 0, j = 0;
        for (int k = left; k <= right; k++) {
            if (leftArray[i] < rightArray[j]) {
                A[k] = leftArray[i];
                i++;
            } else {
                A[k] = rightArray[j];
                j++;
            }
        }
    }

    public static void mergeSort(int[] Array, int left, int right) {
        if (right > left) {
            int m = (left + right) / 2;
            mergeSort(Array, left, m);
            mergeSort(Array, m + 1, right);
            merge(Array, left, m, right);
        }
    }

    public static void print(int[] arr) {
        for (int j : arr) {
            System.out.print(j + " ");
        }
    }

    public static void main(String[] args) {
        int[] arr = {3, 7, 1, 9, 2, 5, 4, 8, 6};
        mergeSort(arr, 0, arr.length - 1);
        print(arr);
    }
}

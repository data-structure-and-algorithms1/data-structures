package com.company.algorithm.sort.quick;

public class QuickSort {

    public static int partition(int[] arr, int start, int end) {
        int pivot = end;
        int i = start-1;
        for (int j = start; j <= end; j++) {
            if (arr[j] <= arr[pivot]) {
                i++;
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
        return i;
    }

    public static void quickSort(int[] arr, int start, int end) {
        if (start < end) {
            int pivot = partition(arr, start, end);
            quickSort(arr, start, pivot - 1);
            quickSort(arr, pivot + 1, end);
        }
    }

    public static void print(int[] arr) {
        for (int j : arr) {
            System.out.print(j + " ");
        }
    }

    public static void main(String[] args) {
        int[] arr = {3, 6, 1, 2, 9, 8, 5, 4, 7};
        quickSort(arr, 0, arr.length - 1);
        print(arr);
    }
}

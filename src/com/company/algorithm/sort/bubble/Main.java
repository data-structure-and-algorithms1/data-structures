package com.company.algorithm.sort.bubble;

public class Main {

    public static void main(String[] args) {
        int[] arr = {3, 1, 7, 5, 4, 9, 8, 6, 2};
        BubbleSort bubbleSort = new BubbleSort(arr);
        bubbleSort.sort();
        bubbleSort.print();

    }
}

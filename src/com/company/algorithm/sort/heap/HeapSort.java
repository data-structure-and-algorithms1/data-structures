package com.company.algorithm.sort.heap;

public class HeapSort {
    int[] arr = null;

    public HeapSort(int[] arr) {
        this.arr = arr;
    }

    public void sort() {
        BinaryHeap bp = new BinaryHeap(arr.length);
        for (int j : arr) {
            bp.insertInHeap(j);
        }

        for (int i = 0; i < arr.length; i++) {
            arr[i] = bp.extractHeadOfHeap();
        }
    }

    public void print() {
        for (int j : arr) {
            System.out.print(j + " ");
        }
    }

    public static void main(String[] args) {
        int[] arr = {4, 3, 7, 1, 8, 5, 9, 2, 6};
        HeapSort heapSort = new HeapSort(arr);
        heapSort.sort();
        heapSort.print();
    }
}

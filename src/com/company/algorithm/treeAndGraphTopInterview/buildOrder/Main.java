package com.company.algorithm.treeAndGraphTopInterview.buildOrder;

public class Main {

    public static void main(String[] args) {
        String[] projects = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};
        String[][] dependencies = {
                {"a", "b"},
                {"b", "c"},
                {"a", "c"},
                {"a", "c"},
                {"d", "e"},
                {"b", "d"},
                {"e", "f"},
                {"a", "f"},
                {"h", "i"},
                {"h", "j"},
                {"i", "j"},
                {"g", "j"}};

        String[] buildOrder = BuildOrder.buildOrderWrapper(projects,dependencies);
        if(buildOrder == null){
            System.out.println("Circular Dependency");
        }
        else {
            for(String s : buildOrder){
                System.out.println(s);
            }
        }
    }
}

package com.company.algorithm.treeAndGraphTopInterview.minimalTreeNode;

public class Main {
    /**
     * bir array yaradiq unique elementleri ile sort olunmus veziyyetde
     * ve bir serach algoritmasi yazin hansiki minimal hundurluk olsun
     */

    public static void main(String[] args) {
        int[] arr = {10, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80};

        TreeNode treeNode = TreeNode.createMinimalBST(arr);
        System.out.println(treeNode);
    }
}

package com.company.algorithm.treeAndGraphTopInterview.minimalTreeNode;

public class TreeNode {
    int data;
    TreeNode left;
    TreeNode right;
    TreeNode parent;
    int size = 0;

    public TreeNode(int d) {
        data = d;
        size = 1;
    }

    public boolean isBST() {
        if (left != null) {
            if (data < left.data || !left.isBST())
                return false;
        }
        if (right != null) {
            if (data >= right.data && !right.isBST())
                return false;
        }
        return true;
    }

    public int height() {
        int leftHeight = left != null ? left.height() : 0;
        int rightHeight = right != null ? right.height() : 0;
        return 1 + Math.max(leftHeight, rightHeight);
    }

    private void setLeftChild(TreeNode left) {
        this.left = left;
        if (left != null) {
            left.parent = this;
        }
    }

    private void setRightChild(TreeNode right) {
        this.right = right;
        if (right != null) {
            right.parent = this;
        }
    }

    public static TreeNode createMinimalBST(int[] arr, int start, int end) {
        if (end < start) {
            return null;
        }

        int mid = (start + end) / 2;
        TreeNode n = new TreeNode(arr[mid]);
        n.setLeftChild(createMinimalBST(arr, start, mid - 1));
        n.setRightChild(createMinimalBST(arr, mid + 1, end));
        return n;
    }

    public static TreeNode createMinimalBST(int[] arr) {
        return createMinimalBST(arr, 0, arr.length - 1);
    }
}

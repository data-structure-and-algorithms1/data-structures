package com.company.algorithm.treeAndGraphTopInterview.checkTreeBalance;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        TreeNode treeNode = new TreeNode(1);
        treeNode.insertInOrder(2);
        treeNode.insertInOrder(3);
        treeNode.insertInOrder(4);
        treeNode.insertInOrder(5);
        treeNode.insertInOrder(6);
        treeNode.insertInOrder(7);
        treeNode.insertInOrder(8);
        treeNode.insertInOrder(9);
        treeNode.insertInOrder(10);
        boolean balanced = CheckBalanced.isBalanced(treeNode);
        System.out.println(balanced);
    }
}

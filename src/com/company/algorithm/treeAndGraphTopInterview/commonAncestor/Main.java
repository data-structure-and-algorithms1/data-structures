package com.company.algorithm.treeAndGraphTopInterview.commonAncestor;

public class Main {

    public static void main(String[] args) {
        int[] arr = {5, 3, 6, 1, 9, 11};
        TreeNode root = new TreeNode(20);
        for (int a : arr) {
            root.insertInOrder(a);
        }

        TreeNode n1 = root.find(1);
        TreeNode n2 = root.find(9);
        TreeNode ancestor = CommonAncestor.commonAncestor(root,n1,n2);
        System.out.println(ancestor.data);
    }
}

package com.company.algorithm.treeAndGraphTopInterview.commonAncestor;

public class CommonAncestor {

    public static TreeNode commonAncestor(TreeNode root, TreeNode n1, TreeNode n2) {
        if ((n1 == null) || (n2 == null)) {
            return null;
        }
        TreeNode ap = n1.parent;
        while (ap != null) {
            TreeNode aq = n2.parent;
            while (aq != null) {
                if (aq == ap) {
                    return aq;
                }
                aq = aq.parent;
            }
            ap = ap.parent;
        }
        return null;
    }
}

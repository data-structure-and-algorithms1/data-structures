package com.company.algorithm.treeAndGraphTopInterview.validateBST;

public class Main {

    public static void main(String[] args) {
        TreeNode treeNode = new TreeNode(1);
        treeNode.insertInOrder(2);
        treeNode.insertInOrder(3);
        treeNode.insertInOrder(4);
        treeNode.insertInOrder(5);
        treeNode.insertInOrder(6);
        treeNode.insertInOrder(7);
        treeNode.insertInOrder(8);
        treeNode.insertInOrder(9);
        treeNode.insertInOrder(10);
        System.out.println(ValidateBST.checkBST(treeNode));

    }
}

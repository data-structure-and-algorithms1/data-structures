package com.company.algorithm.treeAndGraphTopInterview.validateBST;

public class ValidateBST {
    public static Integer lastPrinted = null;

    public static boolean checkBST(TreeNode node) {
        return checkBST(node, true);
    }

    private static boolean checkBST(TreeNode node, boolean isLeft) {
        if (node == null) {
            return true;
        }

        // check recurse left
        if (!checkBST(node.left, true)) {
            return false;
        }

        // check current
        if (lastPrinted != null) {
            if (isLeft) {
                if (node.data < lastPrinted) {
                    return false;
                }
            } else {
                if (node.data <= lastPrinted) {
                    return false;
                }
            }
        }
        lastPrinted = node.data;

        // check recurse right
        if (!checkBST(node.right, false)) {
            return false;
        }
        return true;
    }
}

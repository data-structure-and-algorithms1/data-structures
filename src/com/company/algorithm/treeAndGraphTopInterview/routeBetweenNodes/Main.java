package com.company.algorithm.treeAndGraphTopInterview.routeBetweenNodes;

import java.util.ArrayList;

public class Main {
    /**
     * 2 route arasindaki nodelarin true false oldugunu yoxlamaq lazimdir
     * elqeler directed edge oldugu ucun getdiyi yerden geri done bilmir, misal
     * A -> B true B den A a ise false sayilir
     * ele metod yazki bunu tapib true yoxsa false oldugunu bize desin
     * @param args
     */
    public static void main(String[] args) {
        ArrayList<GraphNode> nodeList = new ArrayList<>();
        nodeList.add(new GraphNode("A", 0));
        nodeList.add(new GraphNode("B", 1));
        nodeList.add(new GraphNode("C", 2));
        nodeList.add(new GraphNode("D", 3));
        nodeList.add(new GraphNode("E", 4));
        nodeList.add(new GraphNode("F", 5));
        nodeList.add(new GraphNode("G", 6));
        nodeList.add(new GraphNode("H", 7));
        nodeList.add(new GraphNode("I", 8));
        nodeList.add(new GraphNode("J", 9));

        Graph graph = new Graph(nodeList);
        graph.addDirectedEdge(0, 1);
        graph.addDirectedEdge(0, 2);
        graph.addDirectedEdge(0, 3);
        graph.addDirectedEdge(1, 9);
        graph.addDirectedEdge(2, 6);
        graph.addDirectedEdge(4, 0);
        graph.addDirectedEdge(4, 5);
        graph.addDirectedEdge(5, 8);
        graph.addDirectedEdge(6, 7);
        graph.addDirectedEdge(6, 3);
        boolean result = graph.search(nodeList.get(1), nodeList.get(0));
        System.out.println(result);
    }
}

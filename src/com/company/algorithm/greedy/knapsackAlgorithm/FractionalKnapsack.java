package com.company.algorithm.greedy.knapsackAlgorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FractionalKnapsack {
    public static void knapSack(ArrayList<KnapsackItem> items, int capacity) {
        Comparator<KnapsackItem> comparator = (t1, t2) -> {
            if (t2.getRatio() > t1.getRatio()) return 1;
            else return -1;
        };
        items.sort(comparator);
        int usedCapacity = 0;
        double totalValue = 0;

        for (KnapsackItem item : items) {
            if (usedCapacity + item.getWeight() <= capacity) {
                usedCapacity += item.getWeight();
                System.out.println("Taken: " + item);
                totalValue += item.getValue();
            } else {
                int usedWeight = capacity - usedCapacity;
                double value = item.getRatio() * usedWeight;
                System.out.println("Taken item index = " + item.getIndex() +
                        ", obtained value = " + value + " used weight = " + usedWeight +
                        ", ratio = " + item.getRatio());
                usedCapacity += usedWeight;
                totalValue += value;
            }
            if (usedCapacity == capacity) break;
        }
        System.out.println("\nTotal value obtained: " + totalValue);
    }

    public static void main(String[] args) {
        ArrayList<KnapsackItem> items = new ArrayList<>();
        int[] value = {100, 120, 60};
        int[] weight = {20, 30, 10};
        int capacity = 50;

        for (int i = 0; i < value.length; i++) {
            items.add(new KnapsackItem(i + 1, value[i], weight[i]));
        }

        knapSack(items, capacity);
    }
}

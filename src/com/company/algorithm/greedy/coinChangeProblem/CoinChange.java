package com.company.algorithm.greedy.coinChangeProblem;

import java.util.Arrays;

public class CoinChange {

    public static void coinChange(int[] coins, int N) {
        Arrays.sort(coins);
        int index = coins.length - 1;
        do {
            int coinValue = coins[index];
            index--;
            int maxAmount = (N / coinValue) * coinValue;
            if (maxAmount > 0) {
                System.out.println("Coin value: " + coinValue + " taken count: " + (N / coinValue));
                N = N - maxAmount;
            }
        } while (N != 0);
    }

    public static void main(String[] args) {
        int[] coins = {1, 5, 10, 20, 50, 100, 200, 500};
        int N = 2035;
        coinChange(coins, N);
    }
}

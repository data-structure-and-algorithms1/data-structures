package com.company.algorithm.greedy.activitySelectionProblem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ActivitySelection {

    public static void activitySelection(ArrayList<Activity> activities) {
        activities.sort(Comparator.comparingInt(Activity::getEndTime));
        Activity previousActivity = activities.get(0);
        System.out.println("\n\nRecommended Schedule: " + "\n " + activities.get(0));
        for (int i = 1; i < activities.size(); i++) {
            Activity activity = activities.get(i);
            if (activity.getStartTime() >= previousActivity.getEndTime()) {
                System.out.println(activity);
                previousActivity = activity;
            }
        }
    }

    public static void main(String[] args) {
        ArrayList<Activity> activities = new ArrayList<>();
        activities.add(new Activity("A1",0,6));
        activities.add(new Activity("A2",3,4));
        activities.add(new Activity("A3",1,2));
        activities.add(new Activity("A4",5,8));
        activities.add(new Activity("A5",5,7));
        activities.add(new Activity("A6",8,9));

        activitySelection(activities);
    }
}

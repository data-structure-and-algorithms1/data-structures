package com.company.algorithm.search;

public class Searching {

    public static int linearSearch(int[] arr, int value) {
        for (int i = 0; i < arr.length; i++) {
            if (value == arr[i]) {
                System.out.println("The Element is found at index: " + i);
                return i;
            }
        }
        System.out.println("The element: " + value + " not found in the array");
        return -1;
    }

    public static int binarySearch(int[] arr, int value) {
        int start = 0;
        int end = arr.length - 1;
        int middle = (start + end) / 2;
        while (arr[middle] != value && start <= end) {
            if (value < arr[middle]) {
                end = middle - 1;
            } else {
                start = middle + 1;
            }
            middle = (start + end) / 2;
        }
        if (arr[middle] == value) {
            System.out.println("The Element is found at index " + middle);
            return middle;
        }
        System.out.println("The element: " + value + " not found in the array");
        return -1;
    }

    public static int binarySearchV2(int[] arr, int start, int end, int value) {
        if (end >= start) {
            int mid = (start + end) / 2;
            if (arr[mid] == value)
                return mid;


            if (arr[mid] > value)
                return binarySearchV2(arr, start, mid - 1, value);


            return binarySearchV2(arr, mid + 1, end, value);
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
        int start = 0;
        int end = arr.length - 1;
        int value = 34;
        linearSearch(arr, value);
        binarySearch(arr, value);
        System.out.println(binarySearchV2(arr,start,end,value));
    }
}

package com.company.algorithm.linkedlist.interviews;

import com.company.dataStructure.linkedlist.singlylinkedlist.SinglyLinkedList;

public class Main {

    public static void main(String[] args) {
        SinglyLinkedList  linkedList = new SinglyLinkedList();
        linkedList.insertLinkedList(1,0);
        linkedList.insertLinkedList(5,1);
        linkedList.insertLinkedList(32,2);
        linkedList.insertLinkedList(14,3);
        linkedList.insertLinkedList(2,4);
        linkedList.insertLinkedList(9,5);
        linkedList.insertLinkedList(7,6);
        linkedList.traverseSinglyLinkedList();

        Questions questions = new Questions();

//        questions.deleteDuplicates(linkedList);
//        linkedList.traverseSinglyLinkedList();

       // System.out.println(questions.nthToLast(linkedList,3).value);

//        linkedList = questions.partition(linkedList,9);
//        linkedList.traverseSinglyLinkedList();



        SinglyLinkedList  linkedList2 = new SinglyLinkedList();
        linkedList2.insertLinkedList(12,0);
        linkedList2.insertLinkedList(2,1);
        linkedList2.insertLinkedList(16,2);
        linkedList2.insertLinkedList(44,3);
        linkedList2.insertLinkedList(29,4);

        SinglyLinkedList sumList = questions.sumLists(linkedList,linkedList2);
        sumList.traverseSinglyLinkedList();

    }
}

package com.company.algorithm.linkedlist.interviews;

import com.company.dataStructure.linkedlist.singlylinkedlist.Node;
import com.company.dataStructure.linkedlist.singlylinkedlist.SinglyLinkedList;

import java.util.HashSet;

public class Questions {

    public void deleteDuplicates(SinglyLinkedList linkedList) {
        HashSet<Integer> hs = new HashSet<>();
        Node current = linkedList.head;
        Node prev = null;
        while (current != null) {
            int curval = current.value;
            if (hs.contains(curval)) {
                prev.next = current.next;
                linkedList.size--;
            } else {
                hs.add(curval);
                prev = current;
            }
            current = current.next;
        }
    }

    public Node nthToLast(SinglyLinkedList linkedList, int n) {
        Node p1 = linkedList.head;
        Node p2 = linkedList.head;
        for (int i = 0; i < n; i++) {
            if (p2 == null) return null;
            p2 = p2.next;
        }
        while (p2 != null) {
            p1 = p1.next;
            p2 = p2.next;
        }
        return p1;
    }

    public SinglyLinkedList partition(SinglyLinkedList ll, int x) {
        Node currentNode = ll.head;
        ll.tail = ll.head;
        while (currentNode != null) {
            Node next = currentNode.next;
            if (currentNode.value < x) {
                currentNode.next = ll.head;
                ll.head = currentNode;
            } else {
                ll.tail.next = currentNode;
                ll.tail = currentNode;
            }
            currentNode = next;
        }
        ll.tail.next = null;
        return ll;
    }

    public SinglyLinkedList sumLists(SinglyLinkedList llA, SinglyLinkedList llB) {
        Node n1 = llA.head;
        Node n2 = llB.head;
        int carry = 0;
        SinglyLinkedList resultLl = new SinglyLinkedList();
        while (n1 != null || n2 != null) {
            int result = carry;
            if (n1 != null) {
                result += n1.value;
                n1 = n1.next;
            }
            if (n2 != null) {
                result += n2.value;
                n2 = n2.next;
            }
            resultLl.insertLinkedList(result % 10, resultLl.size - 1);
            carry = result / 10;
        }
        return resultLl;
    }

    public Node getKthNode(Node head, int k) {
        Node current = head;
        while (k > 0 && current != null) {
            current = current.next;
            k--;
        }
        return current;
    }

    public Node findIntersection(SinglyLinkedList list1, SinglyLinkedList list2) {
        if (list1.head == null || list2.head == null) return null;
        if (list1.tail != list2.tail) return null;

        Node shorter = new Node();
        Node longer = new Node();
        if (list1.size > list2.size) {
            longer = list1.head;
            shorter = list2.head;
        } else {
            longer = list2.head;
            shorter = list1.head;
        }

        longer = getKthNode(longer, Math.abs(list1.size - list2.size));
        while (shorter != longer) {
            shorter = shorter.next;
            longer = longer.next;
        }
        return longer;
    }

    public void addSameNode(SinglyLinkedList llA, SinglyLinkedList llB, int nodeValue){
        Node newNode = new Node();
        newNode.value = nodeValue;
        llA.tail.next = newNode;
        llA.tail = newNode;
        llB.tail.next = newNode;
        llB.tail = newNode;
    }

}

package com.company.algorithm.linkedlist.interviews;

import com.company.dataStructure.linkedlist.singlylinkedlist.Node;
import com.company.dataStructure.linkedlist.singlylinkedlist.SinglyLinkedList;

public class Main2 {

    public static void main(String[] args) {
        SinglyLinkedList linkedList = new SinglyLinkedList();
        linkedList.insertLinkedList(3, 0);
        linkedList.insertLinkedList(1, 1);
        linkedList.insertLinkedList(5, 2);
        linkedList.insertLinkedList(9, 3);

        SinglyLinkedList linkedList2 = new SinglyLinkedList();
        linkedList2.insertLinkedList(2, 0);
        linkedList2.insertLinkedList(4, 1);
        linkedList2.insertLinkedList(6, 2);

        Questions questions = new Questions();
        questions.addSameNode(linkedList, linkedList2, 7);
        questions.addSameNode(linkedList, linkedList2, 2);
        questions.addSameNode(linkedList, linkedList2, 1);
        Node inter = questions.findIntersection(linkedList, linkedList2);
        System.out.println(inter.value);


    }
}

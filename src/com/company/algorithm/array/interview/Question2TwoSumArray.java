package com.company.algorithm.array.interview;

public class Question2TwoSumArray {

    public static int[] findPair(int[] nums, int target) throws IllegalAccessException {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    return new int[]{i, j};
                }
            }
        }
        throw new IllegalAccessException("No Solution Found");
    }

    public static void main(String[] args) throws IllegalAccessException {
        int[] nums = {1, 2, 3, 4, 5};
        int[] arr = findPair(nums, 9);

        System.out.println(nums[arr[0]]);
        System.out.println(nums[arr[1]]);
    }
}

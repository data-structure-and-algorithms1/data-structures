package com.company.algorithm.array.interview;

import java.util.Arrays;

public class Question7RotateMatrix {



    // 90 degree rotate  {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
    //                   {{7, 4, 1}, {8, 5, 2}, {9, 6, 3}}

    public static boolean rotateMatrix(int[][] matrix) {
        if (matrix.length == 0 || matrix.length != matrix[0].length) return false; // step 1
        int n = matrix.length;  // step 2

        for (int layer = 0; layer < n/2; layer++) {
            int last = n - 1 - layer;  // en son indexi verirem ve getdikce yuxaridna asagiya dogru azaldiram
            for (int i = layer; i < last; i++) {
                int offset = i - layer;
                int top = matrix[layer][i];
                matrix[layer][i] = matrix[last-offset][layer];
                matrix[last-offset][layer] = matrix[last][last-offset];
                matrix[last][last-offset] = matrix[i][last];
                matrix[i][last] = top;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        rotateMatrix(matrix);
        System.out.println(Arrays.deepToString(matrix));
    }
}

package com.company.algorithm.array.interview;

public class Question3FindingNumber {

    public static void main(String[] args) {

        int[] arr = {1, 2, 3, 4, 5, 6};
        System.out.println(search(arr,7));
    }

    public static int search(int[] arr, int value){
        for (int i = 0; i <arr.length ; i++) {
            if(arr[i] == value){
                return i;
            }
        }
        return -1;
    }
}

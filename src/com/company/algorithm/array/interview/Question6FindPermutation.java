package com.company.algorithm.array.interview;


// permutasiya 2 massivdeki deyerlerin eyni olub olmadigini yoxluyur
public class Question6FindPermutation {

    public static boolean isPermutation(int[] arr1, int[] arr2) {
        if (arr1.length != arr2.length)
            return false;

        int sum1 = 0;
        int sum2 = 0;

        for (int i = 0; i < arr1.length; i++) {
            sum1 += arr1[i];
            sum2 += arr2[i];
        }
        return sum1 == sum2;
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 4, 5, 6};
        int[] arr2 = {1, 4, 6, 2, 5, 3};

        System.out.println(isPermutation(arr1, arr2));
    }
}

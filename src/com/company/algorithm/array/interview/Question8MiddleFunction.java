package com.company.algorithm.array.interview;

import java.util.Arrays;

// array de ortada olan 2 deyeri tap
public class Question8MiddleFunction {

    static int[] middleV2(int[] arr) {
        return Arrays.copyOfRange(arr, 1, arr.length-1);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6}; // 2,3,4,5  ilk index ve axirinci indexi cixmaq serti ile ortadaki arrayleri goturmek
        System.out.println(Arrays.toString(middleV2(arr)));
    }
}

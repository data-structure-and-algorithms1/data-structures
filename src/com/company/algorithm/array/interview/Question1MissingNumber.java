package com.company.algorithm.array.interview;

public class Question1MissingNumber {

    public static int getMissingNoV1(int arr[]) {
        int total = 1;
        for (int i = 2; i <= arr.length + 1; i++) {
            total += i;
            total -= arr[i - 2];
        }
        return total;
    }

    // xor dan istifade ederek tapaq
    public static int getMissingNoV2(int arr[]) {
        int x1 = arr[0];
        int x2 = 1;

        for (int i = 1; i < arr.length; i++) {
            x1 = x1 ^ arr[i];
        }

        for (int i = 2; i <= arr.length + 1; i++) {
            x2 = x2 ^ i;
        }

        return (x1 ^ x2);
    }

    public static int getMissingNoV3(int arr[]) {
        int n = arr.length;
        int nElementsSum = n * (n + 1) / 2;
        int sum = 0;
        for (int i = 0; i < n - 1; i++) {
            sum += arr[i];
        }
        return nElementsSum - sum;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 8, 9, 10};
        System.out.println(getMissingNoV1(arr));
        System.out.println(getMissingNoV2(arr));
        System.out.println(getMissingNoV3(arr));
    }
}

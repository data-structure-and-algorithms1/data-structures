package com.company.algorithm.array.interview;

public class Question4maxProductOfInteger {

    // 2 integerin vurulmasindan alinan hasilin en yuksek olan deyerleir goster
    // misal {10,20,30,80,70,50,40}
    //  80 ve 70 in vurulmasindan alinan hasil en yuksek cutlukdu
    public static String maxProduct(int[] arr) {
        int max = 0;
        String pairs = "";
        for (int i = 0; i <arr.length; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if(arr[i] * arr[j] > max){
                    max = arr[i] * arr[j];
                    pairs = arr[i] + ", " + arr[j];
                }
            }
        }
        return pairs;
    }

    public static void main(String[] args) {
        int[] arr = {10,20,30,80,70,50,40};
        System.out.println(maxProduct(arr));
    }
}

package com.company.algorithm.array.interview;

import java.util.Arrays;
import java.util.LinkedHashSet;

public class Question11RemoveDuplicates {



    // funksional olmuyan versiya budu burda duplicatlar yan yana olmasa yanlis olacaq
    public static int removeDuplicates(int[] arr, int n) {
        if (n == 0 || n == 1) {
            return n;
        }
        int[] temp = new int[n];
        int j = 0;
        for (int i = 0; i < n - 1; i++) {
            if (arr[i] != arr[i + 1]) {
                temp[j++] = arr[i];
            }
        }
        temp[j++] = arr[n - 1];
        if (j >= 0) System.arraycopy(temp, 0, arr, 0, j);
        return j;
    }

    public static void removeDuplicateWithSet(int[] arr) {
        LinkedHashSet<Integer> set = new LinkedHashSet<>();
        for (int j : arr)
            set.add(j);

        System.out.println(set);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 2, 3, 3, 4};
        System.out.println(Arrays.toString(arr));
//        int n = removeDuplicates(arr, arr.length);
//        for (int i = 0; i < n; i++) {
//            System.out.print(arr[i] + " ");
//        }

        removeDuplicateWithSet(arr);
    }
}

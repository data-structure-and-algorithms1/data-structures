package com.company.algorithm.array.interview;

import java.util.Arrays;
import java.util.Collections;

public class Question10BestScore {

    public static void firstTwoBestScore(Integer[] arr) {
        Integer[] a = arr;
        Arrays.sort(a, Collections.reverseOrder());
        int first = a[0];
        int second = a[1];

        System.out.println(first);
        System.out.println(second);
    }

    public static void main(String[] args) {
        Integer[] arr = {76, 34, 89, 92, 23, 48, 99, 95, 86};
        firstTwoBestScore(arr);
    }

}

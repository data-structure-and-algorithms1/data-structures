package com.company.algorithm.array.rotation;

//Maximum number of 0s placed consecutively at the start and end in any rotation of a Binary String
public class RotationProblem21 {

    private static void findMaximumZeros(String str, int n) {
        int c0 = 0;
        for (int i = 0; i < n; i++) {
            if (str.charAt(i) == '0')
                c0++;
        }

        if (c0 == n) {
            System.out.println(n);
            return;
        }

        int max = 0;

        int cnt = 0;
        for (int i = 0; i < n; i++) {
            if (str.charAt(i) == '0')
                cnt++;
            else {
                max = Math.max(max, cnt);
                cnt = 0;
            }
        }

        max = Math.max(max, cnt);
        int start = 0;
        int end = n - 1;
        cnt = 0;

        while (str.charAt(start) != '1' && start < n) {
            cnt++;
            start++;
        }

        while (str.charAt(end) != '1') {
            cnt++;
            end--;
        }

        max = Math.max(max, cnt);
        System.out.println(max);
    }

    public static void main(String[] args) {
        String s = "1001";
        int n = s.length();
        findMaximumZeros(s, n);
    }
}

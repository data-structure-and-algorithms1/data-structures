package com.company.algorithm.array.rotation;

// Left Rotation and Right Rotation of a String
public class RotationProblem89 {

    private static void leftRotate(String s, int d) {
        s = s.substring(d) + s.substring(0, d);
        System.out.println(s);
    }

    private static void rightRotate(String s, int d) {
        int n = s.length();
        s = s.substring(n-d) + s.substring(0,n-d);
        System.out.println(s);
    }

    public static void main(String[] args) {
        String str = "GeeksforGeeks";
        int d = 2;
        leftRotate(str, d);
        rightRotate(str,d);
    }
}

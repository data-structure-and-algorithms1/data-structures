package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Reversal algorithm for right rotation of an array
public class RotationProblem12 {

    private static void reverse(int[] arr, int start, int end) {
        while (start < end) {
            int temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }

    private static void rightRotate(int[] arr, int k, int n) {
        reverse(arr, 0, n - 1);
        reverse(arr, 0, k - 1);
        reverse(arr, k, n - 1);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int n = arr.length;
        int k = 3;
        rightRotate(arr, k, n);
        System.out.println(Arrays.toString(arr));
    }
}

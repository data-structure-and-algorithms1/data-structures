package com.company.algorithm.array.rotation;

//Quickly find multiple left rotations of an array | Set 1
public class RotationProblem10 {

    // -----------------  VERSION 1 -----------------------
    private static void leftRotate(int[] arr, int n, int k) {
        for (int i = 0; i < k; i++) {
            int temp = arr[0];
            if (n - 1 >= 0) System.arraycopy(arr, 1, arr, 0, n - 1);
            arr[n - 1] = temp;
        }
    }

    // ----------------- VERSION 2 --------------------------
    private static void leftRotateV2(int[] arr, int n, int k) {
        for (int i = k; i < k + n; i++) {
            System.out.print(arr[i % n] + " ");
        }
    }

    // ------------------- VERSION 3 --------------------------
    private static void preprocess(int[] arr, int n, int[] temp) {
        for (int i = 0; i < n; i++) {
            temp[i] = temp[i + n] = arr[i];
        }
    }

    private static void leftRotateV3(int[] arr, int n, int k, int[] temp) {
        int start = k % n;
        for (int i = start; i < start + n; i++) {
            System.out.print(temp[i] + " ");
        }
        System.out.println();
    }

    //----------------------------------------------------------
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6};

        int n = arr.length;
        //leftRotate(arr, arr.length, k);
        //leftRotateV2(arr, arr.length, k);
        // System.out.println(Arrays.toString(arr));

        int[] temp = new int[2 * n];
        preprocess(arr, n, temp);
        int k = 2;
        leftRotateV3(arr, n, k, temp);
        k = 3;
        leftRotateV3(arr, n, k, temp);
    }
}

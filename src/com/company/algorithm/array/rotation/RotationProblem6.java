package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Given a sorted and rotated array, find if there is a pair with a given sum
public class RotationProblem6 {


    private static void pairSortedRotated(int[] arr, int n, int x) {
        // elementin pivotunu tapiriq
        int pivot = 0;
        for (int i = 0; i < n - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                pivot = i;
            }
        }

        int left = (pivot + 1) % n;
        int right = pivot;

        while (left != right) {
            if (arr[left] + arr[right] == x) {
                System.out.println("[" + arr[left] + ", " + arr[right] + "]");
                break;
            }
            else if (arr[left] + arr[right] < x) {
                left = (left + 1) % n;
            }
            else {
                right = (n + right - 1) % n;
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {11, 15, 6, 8, 9, 10};
        int n = arr.length;
        int x = 19;
        pairSortedRotated(arr, n, x);
        System.out.println(Arrays.toString(arr));
    }
}

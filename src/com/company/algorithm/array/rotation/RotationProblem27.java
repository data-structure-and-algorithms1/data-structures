package com.company.algorithm.array.rotation;

//Maximum value possible by rotating digits of a given number
public class RotationProblem27 {

    // ------------------------- VERSION 1 ------------------------------
    private static void getMaxNum(String num) {
        int len = num.length();
        int max = Integer.parseInt(num);
        String s = num;
        for (int i = 0; i < len; i++) {
            s = s.substring(1) + s.charAt(0);
            max = Math.max(Integer.parseInt(s), max);
        }
        System.out.println(max);
    }
    // ---------------------------------------------------------------------

    // -------------------- VERSION 2 -----------------------------------

    private static void getMaxNum(int num) {
        int answer = num;
        int len = (int) (Math.floor(Math.log10(num)) + 1);
        int x = (int) Math.pow(10, len - 1);

        for (int i = 1; i < len; i++) {
            int lastDigit = num % 10;
            num = num / 10;
            num += lastDigit * x;

            if (num > answer)
                answer = num;
        }
        System.out.println(answer);
    }

    public static void main(String[] args) {
        String num = "657";
        getMaxNum(num);

        int num2 = 657;
        getMaxNum(num2);
    }
}

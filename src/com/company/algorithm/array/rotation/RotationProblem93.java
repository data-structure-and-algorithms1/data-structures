package com.company.algorithm.array.rotation;

//Find the Rotation Count in Rotated Sorted array
public class RotationProblem93 {

    // ===================== VERSION 1 ==========================
    private static int rotateSort(int[] arr) {
        int min = arr[0], minIndex = -1;
        for (int i = 0; i < arr.length; i++) {
            if (min > arr[i]) {
                min = arr[i];
                minIndex = i;
            }
        }
        return minIndex;
    }

    // ======================= VERSION 2 ============================
    private static int countRotations(int[] arr, int low, int high) {
        if (high < low) return 0;
        if (high == low) return low;

        int mid = low + (high - low) / 2;

        if (mid < high && arr[mid+1] < arr[mid]) {
            return mid + 1;
        }

        if (mid > low && arr[mid] < arr[mid - 1]) {
            return mid;
        }

        if (arr[high] > arr[mid]) {
            return countRotations(arr, low, mid - 1);
        }

        return countRotations(arr, mid + 1, high);
    }

    public static void main(String[] args) {
        int[] arr = {15, 18, 2, 3, 6, 12};
        //System.out.println(rotateSort(arr));
        System.out.println(countRotations(arr, 0, arr.length - 1));
    }
}

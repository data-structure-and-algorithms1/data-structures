package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Block swap algorithm for array rotation
public class RotationProblem3 {

    // ----------------------- VERSION 1 ------------------------------
    private static void leftRotate(int[] arr, int d, int n) {
        leftRotateRec(arr, 0, d, n);
    }

    private static void leftRotateRec(int[] arr, int i, int d, int n) {

        if (d == 0 || d == n)
            return;

        if (n - d == d) {
            swap(arr, i, n - d + i, d);
            return;
        }

        if (d < n - d) {
            swap(arr, i, n - d + i, d);
            leftRotateRec(arr, i, d, n - d);
        } else {
            swap(arr, i, d, n - d);
            leftRotateRec(arr, n - d + i, 2 * d - n, d);
        }
    }

    private static void swap(int[] arr, int fi, int si, int d) {
        for (int i = 0; i < d; i++) {
            int temp = arr[fi + i];
            arr[fi + i] = arr[si + i];
            arr[si + i] = temp;
        }
    }
    // -------------------------------------------------------------------

    // ---------------------------- VERSION 2 ----------------------------
    private static void leftRotateV2(int[] arr, int d, int n) {
        int i, j;
        if (d == 0 || d == n)
            return;
        i = d;
        j = n - d;
        while (i != j) {
            if (i < j) {
                swap(arr, d - i, d + j - i, i);
                j -= i;
            } else {
                swap(arr, d - i, d, j);
                i -= j;
            }
        }
        swap(arr, d - i, d, i);
    }
    // ------------------------------------------------------------------

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        int d = 2; // nece defe rotate etsin
        int n = arr.length;
        leftRotate(arr, d, n);
        System.out.println(Arrays.toString(arr));
    }
}

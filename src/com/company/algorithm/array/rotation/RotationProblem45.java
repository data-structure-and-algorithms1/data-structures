package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Print all possible rotations of a given Array
public class RotationProblem45 {

    // ==================== VERSION 1 ==========================
    private static void rotateAllPossibleArray(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            System.out.println(Arrays.toString(arr));
            int temp = arr[0];
            System.arraycopy(arr, 1, arr, 0, n - 1);
            arr[n - 1] = temp;
        }
    }

    // ===============================================================

    // ===================== VERSION 2 =============================
    private static void rotateAllPossibleArrayV2(int[] arr, int n) {
        int[] possibleArray = new int[n * 2];
        for (int i = 0, j = 0; i < possibleArray.length; i++, j++) {
            if (j == n) j = 0;
            possibleArray[i] = arr[j];
        }

        for (int i = 0; i < n; i++) {
            for (int j = i; j < n + i; j++) {
                System.out.print(possibleArray[j] + " ");
            }
            System.out.println();
        }
    }
    // ================================================================

    // ========================= VERSION 3 ============================

    private static void rotateAllPossibleArrayV3(int[] arr, int n) {
        StringBuilder str = new StringBuilder();
        for (int i = 0, j = 0; i < n * 2; i++, j++) {
            if (j == n) j = 0;
            str.append(arr[j]);
        }

        for (int i = 0; i < n; i++) {
            System.out.println(str.substring(i, n + i));
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        //rotateAllPossibleArray(arr, arr.length);
        //rotateAllPossibleArrayV2(arr, arr.length);
        rotateAllPossibleArrayV3(arr, arr.length);
    }
}

package com.company.algorithm.array.rotation;

//Minimum move to end operations to make all strings equal
public class RotationProblem78 {

    private static int rotate(String[] arr, int n) {
        int count = 0;
        for (int i = 0; i < n - 1; i++) {
            if (!isEqual(arr[i], arr[i + 1]))
                for (int j = 0; j < arr[i].length(); j++) {
                    arr[i] = arr[i].substring(1) + arr[i].charAt(0);
                    count++;
                    if (isEqual(arr[i], arr[i + 1])) {
                        return count;
                    }
                }
        }
        return count;
    }

    private static boolean isEqual(String s1, String s2) {
        return s1.equals(s2);
    }

    // ==============================================================

    // ======================== VERSION 2 ============================
    private static int minimumMoves(String[] arr, int n) {
        int res = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            int currentCount = 0;

            String temp = "";
            for (int j = 0; j < n; j++) {
                temp = arr[j] + arr[j];

                int index = temp.indexOf(arr[i]);

                if (index != -1)
                    currentCount += index;
                else
                    currentCount = -1;
            }

            res = Math.min(currentCount, res);
        }
        return res;
    }


    public static void main(String[] args) {
        String[] arr = {"abcde", "cdeab"};
//        int n = arr.length;
//        System.out.println(rotate(arr, n));

//        String arr[] = {"xzzwo", "zwoxz",
//                "zzwox", "xzzwo"};
        int n = arr.length;
        System.out.println(minimumMoves(arr, n));
    }
}

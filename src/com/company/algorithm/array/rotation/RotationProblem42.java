package com.company.algorithm.array.rotation;

import java.util.Arrays;

//How to Left or Right rotate an Array in Java
public class RotationProblem42 {

    private static void rotateArray(int[] arr, int d, int n) {
        int[] rotatedArr = new int[d];
        System.arraycopy(arr, 0, rotatedArr, 0, d);
        if (n - d >= 0) System.arraycopy(arr, d, arr, 0, n - d);
        for (int i = n - d, j = 0; i < n; i++,j++) {
            arr[i] = rotatedArr[j];
        }
        System.out.println(Arrays.toString(arr));
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int n = arr.length;
        int d = 2;
        rotateArray(arr,d,n);
    }
}

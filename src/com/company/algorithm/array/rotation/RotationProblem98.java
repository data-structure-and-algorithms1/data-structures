package com.company.algorithm.array.rotation;

//Rotate a Linked List
public class RotationProblem98 {

    Node head;
    private static class Node {
        int data;
        Node next;
        Node(int d) {
            data = d;
            next = null;
        }
    }

    private void rotate(int k) {
        if (k == 0) return;

        Node current = head;
        int count = 1;
        while (count < k && current != null) {
            current = current.next;
            count++;
        }

        if (current == null) return;

        Node kthNode = current;
        while (current.next != null)
            current = current.next;

        current.next = head;
        head = kthNode.next;
        kthNode.next = null;
    }

    private void push(int data) {
        Node newNode = new Node(data);
        newNode.next = head;
        head = newNode;
    }

    private void printList() {
        Node temp = head;
        while (temp != null) {
            System.out.print(temp.data + " ");
            temp = temp.next;
        }
        System.out.println();
    }

    public static void main(String[] args) {
        RotationProblem98 rotationProblem98 = new RotationProblem98();
        for (int i = 60; i >= 10; i -= 10) {
             rotationProblem98.push(i);
        }
        System.out.println("Given List");
        rotationProblem98.printList();

        rotationProblem98.rotate(2);

        System.out.println("Rotated Linked List");
        rotationProblem98.printList();
    }
}

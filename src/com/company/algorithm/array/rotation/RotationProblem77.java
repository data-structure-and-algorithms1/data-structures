package com.company.algorithm.array.rotation;

//Rotate a Matrix by 180 degree
public class RotationProblem77 {

    // ================== VERSION 1 ====================
    private static void rotateMatrix(int[][] matrix) {
        int n = matrix.length;
        for (int i = n - 1; i >= 0; i--) {
            for (int j = n - 1; j >= 0; j--) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
    // ==========================================================

    // ================== VERSION 2 ========================
    private static void reverseColumns(int[][] mat) {
        int n = mat.length;

        int temp;
        for (int i = 0; i < n; i++) {
            for (int j = 0, k = n - 1; j < k; j++, k--) {
                temp = mat[j][i];
                mat[j][i] = mat[k][i];
                mat[k][i] = temp;
            }
        }
    }

    private static void transpose(int[][] matrix) {
        int n = matrix.length;
        int temp;
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
    }

    private static void printMatrix(int[][] matrix) {
        for (int[] ints : matrix) {
            for (int j = 0; j < matrix.length; j++)
                System.out.print(ints[j] + " ");
            System.out.println();
        }
    }

    private static void rotate180(int[][] matrix) {
        transpose(matrix);
        reverseColumns(matrix);
        transpose(matrix);
        reverseColumns(matrix);
    }

    public static void main(String[] args) {
        int[][] mat = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}
        };
        rotate180(mat);
        printMatrix(mat);
    }
}

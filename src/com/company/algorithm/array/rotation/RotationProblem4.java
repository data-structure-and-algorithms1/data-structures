package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Program to cyclically rotate an array by one
// 1 defe rotate edecek
public class RotationProblem4 {

    private static void rotate(int[] arr) {
        int temp = arr[0];
        System.arraycopy(arr, 1, arr, 0, arr.length - 1);
        arr[arr.length - 1] = temp;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        rotate(arr);
        System.out.println(Arrays.toString(arr));
    }
}

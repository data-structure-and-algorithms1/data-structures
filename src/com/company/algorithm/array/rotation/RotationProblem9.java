package com.company.algorithm.array.rotation;

//Find the Rotation Count in Rotated Sorted array
public class RotationProblem9 {

    // ------------ VERSION 1 ---------------------
    private static int rotateCount(int[] arr, int n) {
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (arr[i] > arr[n - 1]) {
                count++;
            }
        }
        return count;
    }


    // --------------------- VERSION 2 -----------------------
    private static int rotateCountV2(int[] arr, int n) {
        int min = arr[0];
        int minIndex = -1;
        for (int i = 0; i < n; i++) {
            if (min > arr[i]) {
              min = arr[i];
              minIndex = i;
            }
        }
        return minIndex;
    }

    public static void main(String[] args) {
        int[] arr = {15, 18, 2, 3, 6, 12};
        System.out.println(rotateCount(arr, arr.length));
        System.out.println(rotateCountV2(arr, arr.length));
    }
}

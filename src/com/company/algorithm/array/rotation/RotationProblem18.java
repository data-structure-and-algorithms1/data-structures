package com.company.algorithm.array.rotation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Find a rotation with maximum hamming distance | Set 2
//Time complexity: O(N)
public class RotationProblem18 {

    private static int getMinValue(int[] numbers) {
        int minValue = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] < minValue) {
                minValue = numbers[i];
            }
        }
        return minValue;
    }

    private static int getMaxHammingDistance(int[] arr, int n) {
        int[] repetitionsOnRotations = new int[n - 1];
        Map<Integer, List<Integer>> indexesOfElements = new HashMap<>();

        // indexsleri toplamaq ve map a yigmaq
        for (int i = 0; i < n; i++) {
            int key = arr[i];
            List<Integer> indexes = null;
            if (indexesOfElements.containsKey(key)) {
                indexes = indexesOfElements.get(key);
            } else {
                indexes = new ArrayList<>();
            }
            indexes.add(i);
            indexesOfElements.put(key, indexes);
        }

        for (Map.Entry<Integer, List<Integer>> keys : indexesOfElements.entrySet()) {
            List<Integer> indexes = keys.getValue();

            for (int i = 0; i < indexes.size() - 1; i++) {
                for (int j = i + 1; j < indexes.size(); j++) {
                    int diff = indexes.get(i) - indexes.get(j);

                    if (diff < 0) {
                        repetitionsOnRotations[(-diff) - 1] += 1;
                        //diff = n + diff;
                    }
                    repetitionsOnRotations[i] += 1;
                }
            }
        }
        return n - (getMinValue(repetitionsOnRotations));
    }

    public static void main(String[] args) {
        int[] array = { 1, 4, 1 };
        int result1 = getMaxHammingDistance(array,array.length);
        System.out.println(result1);

        // Example 2
        int[] array2 = {2, 4, 8, 0};
        int result2 = getMaxHammingDistance(array2, array2.length);
        System.out.println(result2);
    }
}

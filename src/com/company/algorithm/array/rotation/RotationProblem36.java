package com.company.algorithm.array.rotation;

//Minimize characters to be changed to make the left and right rotation of a string same
public class RotationProblem36 {

    private static int getMinRemoval(String str) {
        int n = str.length();
        int answer = n;
        // is even
        if (n % 2 == 0) {
            int[] freqEven = new int[128];  // ascii table 128 deki kimi
            int[] freqOdd = new int[128];

            for (int i = 0; i < n; i++) {
                if (i % 2 == 0) {
                    freqEven[str.charAt(i)]++;
                } else {
                    freqOdd[str.charAt(i)]++;
                }
            }

            int evenMax = 0, oddMax = 0;

            for (char chr = 'a'; chr <= 'z'; chr++) {
                evenMax = Math.max(evenMax, freqEven[chr]);
                oddMax = Math.max(oddMax, freqOdd[chr]);
            }

            answer = answer - evenMax - oddMax;
        }
        else {
            int[] freq = new int[128];
            for (int i = 0; i < n; i++) {
                freq[str.charAt(i)]++;
            }
            int strMax = 0;
            for (char chr = 'a'; chr <= 'z'; chr++) {
                strMax = Math.max(strMax, freq[chr]);
            }
            answer = answer - strMax;
        }
        return answer;
    }

    //Time Complexity: O(N)
    //Auxiliary Space: O(1)
    public static void main(String[] args) {
        String str = "geeksgeek";
        System.out.println(getMinRemoval(str));
    }
}

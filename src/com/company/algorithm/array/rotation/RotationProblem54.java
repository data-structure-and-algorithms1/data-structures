package com.company.algorithm.array.rotation;

//Clockwise rotation of Linked List
public class RotationProblem54 {

    //link list
    private static class Node {
        int data;
        Node next;
    }

    private static Node push(Node headRef, int newData) {
        Node newNode = new Node();
        newNode.data = newData;
        newNode.next = headRef;
        headRef = newNode;
        return headRef;
    }

    private static void printList(Node node) {
        while (node != null) {
            System.out.print(node.data + " -> ");
            node = node.next;
        }
        System.out.println("null");
    }

    private static Node rightRotate(Node head, int k) {
        if (head == null) return head;

        Node tmp = head;
        int len = 1;
        while (tmp.next != null) {
            tmp = tmp.next;
            len++;
        }

        if (k > len)
            k = k % len;

        k = len - k;

        if (k == 0 || k == len) {
            return head;
        }

        Node current = head;
        int cnt = 1;
        while (cnt < k && current != null) {
            current = current.next;
            cnt++;
        }

        if (current == null)
            return head;

        Node kthNode = current;
        tmp.next = head;
        head = kthNode.next;
        kthNode.next = null;
        return head;
    }

    public static void main(String[] args) {
      Node head = null;
      head = push(head,5);
      head = push(head,4);
      head = push(head,3);
      head = push(head,2);
      head = push(head,1);

      int k = 2;

      Node updatedHead = rightRotate(head,k);
      printList(updatedHead);
    }
}

package com.company.algorithm.array.rotation;

//Maximize difference between sum of prime and non-prime array elements by
// left shifting of digits minimum number of times
public class RotationProblem23 {

    private static boolean isPrime(int n) {
        if (n <= 1)
            return false;
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    private static int[] rotateElement(int e) {
        String strN = Integer.toString(e);

        int maxPrime = -1;
        int cost = 0;
        String temp = strN;

        for (int i = 0; i < strN.length(); i++) {
            if (isPrime(Integer.parseInt(temp)) &&
                    Integer.parseInt(temp) > maxPrime) {
                maxPrime = Integer.parseInt(temp);
                cost = i;
            }
            temp = temp.substring(1) + temp.charAt(0);
        }

        int optElement = maxPrime;

        if (optElement == -1) {
            optElement = Integer.MAX_VALUE;
            temp = strN;

            for (int i = 0; i < strN.length(); i++) {
                if (Integer.parseInt(temp) < optElement) {
                    optElement = Integer.parseInt(temp);
                    cost = i;
                }
                temp = temp.substring(1) + temp.charAt(0);
            }
            optElement *= (-1);
        }
        return new int[]{optElement, cost};
    }

    private static void getMaxSum(int[] arr) {
        int maxSum = 0, cost = 0;

        for(int x : arr){
            int[] ret = rotateElement(x);
            int optElement = ret[0];
            int optCost = ret[1];

            maxSum += optElement;
            cost += optCost;
        }

        System.out.println("Difference = " + maxSum + " , "
                + "Count of operations = "
                + cost);
    }

    public static void main(String[] args) {
        int[] arr = { 541, 763, 321, 716, 143 };
        getMaxSum(arr);
    }
}

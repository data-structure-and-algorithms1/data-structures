package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Modify given array to a non-decreasing array by rotation
public class RotationProblem26 {

    private static void rotateArray(int[] arr, int n) {
        int[] tempArray = new int[n];
        System.arraycopy(arr, 0, tempArray, 0, n);
        Arrays.sort(tempArray);

        for (int i = 1; i <= n; i++) {
            int temp = arr[0];
            System.arraycopy(arr, 1, arr, 0, n - 1);
            arr[n - 1] = temp;
            if (Arrays.equals(arr, tempArray)) {
                System.out.println("YES");
                return;
            }
        }
        System.out.println("NO");
    }

    public static void main(String[] args) {
        int[] arr = {3, 4, 5, 1, 2};
        rotateArray(arr, arr.length);
    }
}

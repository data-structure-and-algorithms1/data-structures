package com.company.algorithm.array.rotation;

//Print array after it is right rotated K times
public class RotationProblem53 {

    // ====================== VERSION 1 =======================
    private static int countAllPossibleRotate(int number) {
        int count = 0;
        int len = (int) (Math.floor(Math.log10(number)) + 1);
        int x = (int) Math.pow(10, len - 1);

        for (int i = 0; i < len; i++) {
            if (number % 10 == 0) {
                count++;
            }
            int last = number % 10;
            number = number / 10;
            number += last * x;
        }
        return count;
    }

    // ========================== VERSION 2 ============================
    private static int countRotation(int number) {
        int count = 0;

        do {
            int digit = number % 10;
            if (digit == 0)
                count++;
            number = number / 10;
        } while (number != 0);

        return count;
    }

    public static void main(String[] args) {
        int num = 10203;
        System.out.println(countAllPossibleRotate(num));
        System.out.println(countRotation(num));
    }
}

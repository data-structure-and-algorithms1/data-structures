package com.company.algorithm.array.rotation;

//Count rotations required to sort given array in non-increasing order
public class RotationProblem31 {

    public static void rotateCountSort(int[] arr, int n) {
        int count = 0;
        int index = 0;
        for (int i = 0; i < n - 1; i++) {
            if (arr[i] < arr[i + 1]) {
                count++;
                index = i;
            }
        }

        if (count == 0) {
            System.out.println(0);
        } else if (count == n - 1) {
            System.out.println(n - 1);
        } else if (count == 1 && arr[0] <= arr[n - 1]) {
            System.out.println(index + 1);
        } else {
            System.out.println(-1);
        }
    }

    public static void main(String[] args) {
        int[] arr = {2, 1, 5, 4, 3};
        rotateCountSort(arr, arr.length);
    }
}

package com.company.algorithm.array.rotation;

//1 - Program for array rotation
//Time complexity : O(n)
//Auxiliary Space : O(d)

import java.util.Arrays;

/**
 * verilen array i rotate ederek surusdurruk 2 metod ile elemek olar
 * ya temporary arraya yigmaqla yada one by one ile
 */
public class RotationProblem1 {

    // version 1 ------------------------------------------
    public static void rotateV1(int[] arr, int d, int n) {
        for (int i = 0; i < d; i++) {
            oneByOne(arr, n);
        }
    }

    public static void oneByOne(int[] arr, int n) {
        int temp = arr[0];
//        for (int i = 0; i < n - 1; i++) {
//            arr[i] = arr[i + 1];
//        }
        if (n - 1 >= 0) System.arraycopy(arr, 1, arr, 0, n - 1); // for evezine bunu isledebilerik
        arr[n - 1] = temp;
    }
    //------------------------------------------------------------

    // version 2 -------------------------------------------------
    public static void rotateV2(int[] arr, int d, int n) {
        int[] temp = new int[d];
        System.arraycopy(arr, 0, temp, 0, d);
        for (int i = d; i < n; i++) {
            arr[i - d] = arr[i];
        }
        //if (n - d >= 0) System.arraycopy(arr, d, arr, 0, n - d);  yuxaridaki for evezine bunu istifade etmek olar
        for (int i = n - d, j = 0; i < n; i++, j++) {
            arr[i] = temp[j];
        }
    }
    // ------------------------------------------------------------

    // version 3 -------------------------------------------------
    public static void rotateV3(int[] arr, int d, int n) {
        d = d % n;
        int i, j, k, temp;
        int gcd = gcd(d, n);
        for (i = 0; i < gcd; i++) {
            temp = arr[i];
            j = i;
            while (true) {
                k = j + d;
                if (k >= n)
                    k = k - n;

                if (k == i)
                    break;

                arr[j] = arr[k];
                j = k;
            }
            arr[j] = temp;
        }
    }

    public static int gcd(int a, int b) {
        if (b == 0) return a;
        else return gcd(b, a % b);
    }
    // ------------------------------------------------------------


    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        int d = 2; // nece defe roatte etsin
        int n = arr.length;

        // rotateV1(arr, d, n);
        // rotateV2(arr, d, n);
        rotateV3(arr, d, n);
        System.out.println(Arrays.toString(arr));
    }
}

package com.company.algorithm.array.rotation;

//Generate a matrix having sum of secondary diagonal equal to a perfect square
public class RotationProblem33 {

    // ----------------- VERSION 1  -----------------------------
    // eger massivi doldurmaq istesen bu versiya yaxsidi
    private static int[] fillArray(int N) {
        int[] arr = new int[N];
        for (int i = 0; i < N; i++) {
            arr[i] = i + 1;
        }
        return arr;
    }

    private static void matrix(int N) {
        int[] arr = fillArray(N);
        int[][] mat = new int[N][N];

        for (int i = 0; i < N; i++) {
            if (i != 0)
                rotateArray(arr);
            System.arraycopy(arr, 0, mat[i], 0, N);
        }

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                System.out.print(mat[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void rotateArray(int[] arr) {
        int temp = arr[0];
        System.arraycopy(arr, 1, arr, 0, arr.length - 1);
        arr[arr.length - 1] = temp;
    }
    // -------------------------------------------------------------

    // --------------------- VERSION 2 ------------------------------
    // sadece capa vermek ucun ise

    private static void diagonalSumPerfectSquare(int n) {
        // call fillArray method
        int[] arr = fillArray(n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arr[(i + j) % n] + " ");
            }
            System.out.println();
        }
    }


    public static void main(String[] args) {
        int N = 5;
        //matrix(N);
        diagonalSumPerfectSquare(N);
    }
}

package com.company.algorithm.array.rotation;

import java.util.HashMap;

//Maximize count of corresponding same elements in given permutations using cyclic rotations
public class RotationProblem44 {

    private static int maximumMatchingPairs(int[] perm1, int[] perm2, int n) {
        int[] left = new int[n];
        int[] right = new int[n];

        HashMap<Integer, Integer> mp = new HashMap<>();
        for (int j = 0; j < n; j++)
            mp.put(perm2[j], j);

        for (int i = 0; i < n; i++) {
            int idx2 = mp.get(perm1[i]);
            if (i == idx2) {
                left[i] = 0;
                right[i] = 0;
            } else if (i < idx2) {
                left[i] = (n - (idx2 - i));
                right[i] = (idx2 - i);
            } else {
                left[i] = i - idx2;
                right[i] = (n - (i - idx2));
            }
        }

        HashMap<Integer, Integer> freq1 = new HashMap<>();
        HashMap<Integer, Integer> freq2 = new HashMap<>();

        for (int i = 0; i < n; i++) {
            if (freq1.containsKey(left[i]))
                freq1.put(left[i], freq1.get(left[i]) + 1);
            else
                freq1.put(left[i], 1);
            if (freq2.containsKey(right[i]))
                freq2.put(right[i], freq2.get(right[i]) + 1);
            else
                freq2.put(right[i], 1);
        }

        int result = 0;
        for (int i = 0; i < n; i++) {
            result = Math.max(result, Math.max(freq1.get(left[i]),
                    freq2.get(right[i])));
        }
        return result;
    }

    //Time Complexity: O(N)
    //Auxiliary Space: O(N)
    public static void main(String[] args) {
        int[] P1 = {5, 4, 3, 2, 1};
        int[] P2 = {1, 2, 3, 4, 5};
        int n = P1.length;

        System.out.print(maximumMatchingPairs(P1, P2, n));
    }
}

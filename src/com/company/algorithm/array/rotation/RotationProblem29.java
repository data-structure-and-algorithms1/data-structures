package com.company.algorithm.array.rotation;

//Rearrange array elements into alternate even-odd sequence by anticlockwise rotation of digits
public class RotationProblem29 {

    private static boolean isPossible(int[] arr, int check) {
        boolean exists = true;
        boolean flag;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == check) {
                check = check == 0 ? 1 : 0;
                continue;
            }

            flag = false;
            String str = Integer.toString(arr[i]);
            for (int j = 0; j < str.length(); j++) {
                if ((str.charAt(j) - '0') % 2 == check) {
                    arr[i] = Integer.parseInt(str.substring(j + 1) + str.substring(0, j + 1));
                    flag = true;
                    break;
                }
            }

            if (!flag) {
                exists = false;
                break;
            }
            check = check == 0 ? 1 : 0;
        }
        return exists;
    }


    private static void convertArr(int[] arr) {
        if (isPossible(arr, 0)) {
            for (int i : arr) {
                System.out.print(i + " ");
            }
        }
        else if(isPossible(arr,1)){
            for (int i : arr) {
                System.out.print(i + " ");
            }
        }
        else {
            System.out.println(-1);
        }
    }

    //Time Complexity: O(N)
    //Auxiliary Space: O(N)
    public static void main(String[] args) {
        int[] arr = {143, 251, 534, 232, 854};
        convertArr(arr);
    }

}

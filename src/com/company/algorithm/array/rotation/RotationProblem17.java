package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Split the array and add the first part to the end
public class RotationProblem17 {

    // one by one
    //Time complexity of the above solution is O(nk).
    private static void rotate(int[] arr, int n, int k) {
        for (int i = 0; i < k; i++) {
            int temp = arr[0];
            if (n - 1 >= 0) System.arraycopy(arr, 1, arr, 0, n - 1);
            arr[n - 1] = temp;
        }
    }

    //An efficient O(n)
    private static void splitRotate(int[] arr, int n, int k) {
        int[] tmp = new int[n * 2];
        System.arraycopy(arr, 0, tmp, 0, n); // 1ci partiya tmp arrayi doldurur
        System.arraycopy(arr, 0, tmp, n, n); // 2ci partiya tekrar eyni datani doldurur
        System.arraycopy(tmp, k, arr, 0, k + n - k);
        // for (int i = k; i < k + n; i++) {
        //            arr[i - k] = tmp[i];
        //        }
        // yuxzridaki ile eyni isi gorur
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int n = arr.length;
        int k = 2;
        //rotate(arr, n, k);
        splitRotate(arr, n, k);
        System.out.println(Arrays.toString(arr));
    }
}

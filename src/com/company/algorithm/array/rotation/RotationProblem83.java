package com.company.algorithm.array.rotation;

//Rotate Linked List block wise
public class RotationProblem83 {

    private static class Node {
        int data;
        Node next;
    }
    private static Node tail;

    private static Node rotateHelper(Node blockHead, Node blockTail, int d, int k) {
        if (d == 0) return blockHead;

        // rotate clockwise
        if (d > 0) {
            Node temp = blockHead;
            for (int i = 1; temp.next.next != null &&
                    i < k - 1; i++) {
                temp = temp.next;
                blockTail.next = blockHead;
                tail = temp;
                return rotateHelper(blockTail, temp, d - 1, k);
            }
        }

        if (d < 0) {
            blockTail.next = blockHead;
            tail = blockHead;
            return rotateHelper(blockHead.next, blockHead, d + 1, k);
        }
        return blockHead;
    }

    private static Node rotateByBlocks(Node head, int k, int d) {
        if (head == null || head.next == null)
            return head;
        if (d == 0)
            return head;

        Node temp = head;
        tail = null;

        int i;
        for (i = 1; temp.next != null && i < k; i++) {
            temp = temp.next;
        }
        Node nextBlock = temp.next;

        if (i < k)
            head = rotateHelper(head, temp, d % k, i);
        else
            head = rotateHelper(head, temp, d % k, k);

        tail.next = rotateByBlocks(nextBlock, k, d % k);
        return head;
    }

    private static Node push(Node head, int data) {
        Node newNode = new Node();
        newNode.data = data;
        newNode.next = head;
        head = newNode;
        return head;
    }

    private static void printList(Node node) {
        while (node != null) {
            System.out.print(node.data + " ");
            node = node.next;
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Node head = null;

        for (int i = 9; i > 0; i--) {
            head = push(head, i);
        }
        printList(head);
        System.out.println("=========================");

        int k = 3, d = 2;
        head = rotateByBlocks(head, k, d);
        printList(head);
    }
}

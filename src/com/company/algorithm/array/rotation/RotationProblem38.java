package com.company.algorithm.array.rotation;

import java.util.ArrayList;
import java.util.List;

//Rotate matrix by 45 degrees
public class RotationProblem38 {

    private static void matrix(int[][] mat, int n, int m) {
        int ctr = 0;
        while (ctr < 2 * n - 1) {
            addSpace(mat, ctr);
            print(addValues(mat, ctr));
            ctr++;
        }
    }

    private static void addSpace(int[][] matrix, int ctr) {
        for (int i = 0; i < Math.abs(matrix.length - ctr - 1); i++) {
            System.out.print(" ");
        }
    }

    private static void print(List<Integer> list) {
        for (int i = list.size() - 1; i >= 0; i--) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();
    }

    private static List<Integer> addValues(int[][] matrix, int ctr) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (i + j == ctr) {
                    list.add(matrix[i][j]);
                }
            }
        }
        return list;
    }


    //Time Complexity: O(N2)
    //Auxiliary Space: O(1)
    public static void main(String[] args) {
        int n = 8;
        int m = n;

        // Given matrix
        int[][] mat = {
                {1, 2, 3, 4, 5, 6, 7, 8},
                {1, 2, 3, 4, 5, 6, 7, 8},
                {1, 2, 3, 4, 5, 6, 7, 8},
                {1, 2, 3, 4, 5, 6, 7, 8},
                {1, 2, 3, 4, 5, 6, 7, 8},
                {1, 2, 3, 4, 5, 6, 7, 8},
                {1, 2, 3, 4, 5, 6, 7, 8},
                {1, 2, 3, 4, 5, 6, 7, 8}};

        matrix(mat, n, m);
    }
}

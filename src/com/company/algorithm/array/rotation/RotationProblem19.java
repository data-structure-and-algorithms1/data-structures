package com.company.algorithm.array.rotation;

//Reduce the given Array of [1, N] by rotating left or right based on given conditions
public class RotationProblem19 {

    // --------------------- VERSION 1 ---------------------------------
    public static int rotate(int[] arr, int x) {
        //  =  1 oldugu halda bu metodlar 2 olarsa bunun tersi olan metodlar olmalidi
        // ancaq bundna daha suretli isleyen versiyani yazaq
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            int tpm = arr[arr.length - 1];
            System.arraycopy(arr, 0, arr, 1, arr.length - 1);
            arr[0] = tpm;
            arr = deleteLastElement(arr);
        }
        return arr[0];
    }

    public static int[] deleteLastElement(int[] arr) {
        int[] temp = new int[arr.length - 1];
        if (arr.length - 1 >= 0) System.arraycopy(arr, 0, temp, 0, arr.length - 1);
        return temp;
    }
    // -------------------------------------------------------------------------


    // --------------------- VERSION 2 ---------------------------
    // Time Complexity: O(log N)
    //Auxiliary Space: O(1)
    private static int rotateV2(int[] arr, int n, int x) {
        long nextPower = 1;

        while (nextPower <= n)
            nextPower *= 2;

        if (x == 1)
            return (int) (nextPower - n);

        long prevPower = nextPower / 2;
        return (int) (2 * (n - prevPower) + 1);
    }

    /**
     * 1ci versiyada massivin deyerleri nedirse onun icindekini cixardib verir
     * 2ci versiyada ise sadece natural edede gore duz cavab verir
     * eger massivin deyerleri basqacur etsen duzgun cavab almazsan
     * Buna gore eger massivin deyerleri standartdan kenardisa 1ci versiya daha duzdu
     */
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};
        int n = array.length;
        int x = 1;
        System.out.println(rotateV2(array, n, x));
        //System.out.println(rotate(array, 1));
    }
}

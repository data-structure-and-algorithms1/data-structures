package com.company.algorithm.array.rotation;

//Longest subsequence of a number having same left and right rotation
public class RotationProblem37 {

    private static int findAltSubSeq(String s) {
        int n = s.length();
        int res = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {

                int cur = 0, f = 0;

                for (int k = 0; k < n; k++) {
                    if (f == 0 && s.charAt(k) - '0' == i) {
                        f = 1;
                        cur++;
                    } else if (f == 1 && s.charAt(k) - '0' == j) {
                        f = 0;
                        cur++;
                    }
                }

                if (i != j && cur % 2 == 1)
                    cur--;

                res = Math.max(cur, res);
            }
        }
        return res;
    }

    public static void main(String[] args) {
        String s = "100210601";
        System.out.println(findAltSubSeq(s));
    }
}

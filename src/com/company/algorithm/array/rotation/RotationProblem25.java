package com.company.algorithm.array.rotation;

//Modify given array to a non-decreasing array by rotation
public class RotationProblem25 {

    private static void stringShift(String s, int[][] shift) {
        int val = 0;
        for (int[] row : shift) {
            if (row[0] == 0)
                val -= row[1];
            else
                val += row[1];
        }
        int len = s.length();
        val = val % len;

        String result;

        if (val > 0) {
            result = s.substring(len - val, (len - val) + val) + s.substring(0, len - val);
        } else {
            result = s.substring(-val, len + val) + s.substring(0, -val);
        }

        System.out.println(result);
    }

    public static void main(String[] args) {
        String s = "abcdefg";
        int[][] shift = {{1, 4}, {0, 2}, {1, 3}};
        stringShift(s, shift);
    }
}

package com.company.algorithm.array.rotation;

//Check if two numbers are bit rotations of each other or not
public class RotationProblem88 {

    private static boolean isRotation(long x, long y) {
        long x64 = x | (x << 32);

        while (x64 >= y) {
            if (x64 == y) {
                return true;
            }

            x64 >>= 1;
        }
        return false;
    }

    public static void main(String[] args) {
        long x = 122;
        long y = 2147483678L;

        System.out.println(isRotation(x, y) ? "no" : "yes");
    }

}

package com.company.algorithm.array.rotation;

import java.util.Arrays;
import java.util.Collections;

//Print left rotation of array in O(n) time and O(1) space
public class RotationProblem84 {

    private static void printRotate(int[] arr, int n, int k) {
        System.out.print("[ ");
        for (int i = 0; i < n; i++) {
            System.out.print(arr[(k + i) % n] + " ");
        }
        System.out.println("]");
    }

    // ==================== VERSION 2 ========================
    private static void printRotateV2(Integer[] arr, int n, int k) {

        Collections.rotate(Arrays.asList(arr), n - k);

        System.out.print("[ ");
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("]");
    }


    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int k = 2;
        printRotate(arr, arr.length, k);

//        Integer[] arr = {1, 2, 3, 4, 5};
//        int k = 2;
//        printRotateV2(arr,arr.length,k);
    }
}

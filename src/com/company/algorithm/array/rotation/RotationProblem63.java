package com.company.algorithm.array.rotation;

//Rotate the sub-list of a linked list from position M to N to the right by K places
public class RotationProblem63 {

    private static class ListNode {
        int data;
        ListNode next;
    }

    private static void rotateSubList(ListNode listNode, int m, int n, int k) {
        int size = n - m + 1;

        if (k > size) {
            k = k % size;
        }

        if (k == 0 || k == size) {
            ListNode head = listNode;
            while (head != null) {
                System.out.print(head.data);
                head = head.next;
            }
            return;
        }

        ListNode link = null;
        if (m == 1) {
            link = listNode;
        }

        ListNode current = listNode;
        int count = 0;
        ListNode end = null;
        ListNode pre = null;
        while (current != null) {
            count++;

            if (count == m - 1) {
                pre = current;
                link = current.next;
            }

            if (count == n - k) {
                if (m == 1) {
                    end = current;
                    listNode = current.next;
                } else {
                    end = current;
                    pre.next = current.next;
                }
            }

            if (count == n) {
                ListNode d = current.next;
                current.next = link;
                end.next = d;
                ListNode head = listNode;
                while (head != null) {
                    System.out.print(head.data + " ");
                    head = head.next;
                }
                return;
            }
            current = current.next;
        }
    }

    private static ListNode push(ListNode head, int val) {
        ListNode newNode = new ListNode();
        newNode.data = val;
        newNode.next = head;
        head = newNode;
        return head;
    }

    public static void main(String[] args) {
        ListNode head = null;
        head = push(head, 70);
        head = push(head, 60);
        head = push(head, 50);
        head = push(head, 40);
        head = push(head, 30);
        head = push(head, 20);
        head = push(head, 10);
        ListNode tmp = head;
        System.out.print("Given List: ");
        while (tmp != null) {
            System.out.print(tmp.data + " ");
            tmp = tmp.next;
        }
        System.out.println();

        int m = 3;
        int n = 2;
        int k = 2;
        System.out.println("After rotation");
        rotateSubList(head, m, n, k);
    }
}

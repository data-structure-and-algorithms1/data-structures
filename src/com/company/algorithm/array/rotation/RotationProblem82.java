package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Split the array and add the first part to the end
public class RotationProblem82 {

    // ==================== VERSION 1 ======================
    private static void rotate(int[] arr, int k) {
        int[] split = new int[k];
        for (int i = 0; i < k; i++) {
            split[i] = arr[i];
        }

        for (int i = 0; i < arr.length - k; i++) {
            arr[i] = arr[k + i];
        }

        for (int i = 0, j = arr.length - k; j < arr.length; i++, j++) {
            arr[j] = split[i];
        }
    }

    // ====================== VERSION 2 ========================
    private static void rotateV2(int[] arr, int k) {
        int[] split = new int[k];
        if (k >= 0) System.arraycopy(arr, 0, split, 0, k);

        if (arr.length - k >= 0) System.arraycopy(arr, k, arr, 0, arr.length - k);

        for (int i = 0, j = arr.length - k; j < arr.length; i++, j++) {
            arr[j] = split[i];
        }
    }

    // ======================= VERSION 3 =============================

    private static void rotateV3(int[] arr, int k) {
        for (int i = 0; i < k; i++) {
            int temp = arr[0];
            for (int j = 0; j < arr.length - 1; j++) {
                arr[j] = arr[j + 1];
            }
            arr[arr.length - 1] = temp;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        int k = 2;

//        rotate(arr,k);
//        rotateV2(arr, k);
        rotateV3(arr, k);
        System.out.println(Arrays.toString(arr));
    }

}

package com.company.algorithm.array.rotation;

//Check if a string can be obtained by rotating another string 2 places
public class RotationProblem91 {
    // ====================== VERSION 1 =========================
    private static boolean rotate(String s1, String s2) {
        int n = s1.length();
        int leftCount = 0;
        int rightCount = 0;
        String temp = s2;
        for (int i = 0; i < n; i++) {
            if (leftCount < 2) {
                temp = leftRotation(temp);
                leftCount++;
            } else if (rightCount < 2) {
                if (rightCount == 0) temp = s2;
                temp = rightRotation(temp);
                rightCount++;
            }

            if (s1.equals(temp)) return true;
        }
        return false;
    }

    private static String leftRotation(String s2) {
        return s2.substring(1) + s2.charAt(0);
    }

    private static String rightRotation(String s2) {
        return s2.charAt(s2.length() - 1) + s2.substring(0, s2.length() - 1);
    }
    // ===============================================================

    // ====================== VERSION 2 ==============================
    private static boolean isRotated(String s1, String s2) {
        if (s1.length() != s2.length()) return false;

        if (s1.length() < 2) return s1.equals(s2);

        String left = "";
        String right = "";
        int len = s2.length();

        left = left + s2.substring(2) +
                s2.substring(0, 2);

        right = right + s2.substring(len - 2, len) +
                s2.substring(0, len - 2);

        return s1.equals(right) || s1.equals(left);
    }

    public static void main(String[] args) {
        String s1 = "amazon";
        String s2 = "azonam"; //onamaz

        //System.out.println(rotate(s1, s2));
        System.out.println(isRotated(s1, s2));
    }
}

package com.company.algorithm.array.rotation;

//Check if a string can be formed from another string by at most X circular clockwise shifts
public class RotationProblem51 {

    private static void isConversionPossible(String s1, String s2, int x) {
        int diff = 0;
        int n = s1.length();

        for (int i = 0; i < n; i++) {

            if (s1.charAt(i) == s2.charAt(i))
                continue;

            diff = ((s2.charAt(i) - s1.charAt(i)) + 26) % 26;

            if(diff > x){
                System.out.println("NO");
                return;
            }
        }
        System.out.println("YES");
    }

    public static void main(String[] args) {
        String s1 = "abcd";
        String s2 = "dddd";
        int x = 3;

        isConversionPossible(s1, s2, x);
    }
}

package com.company.algorithm.array.rotation;

//Count rotations in sorted and rotated linked list
public class RotationProblem80 {

    private static class Node {
        int data;
        Node next;
    }

    private static int countRotation(Node head) {
        int count = 0;

        int min = head.data;

        while (head != null) {
            if (min > head.data)
                break;
            count++;

            head = head.next;
        }
        return count;
    }

    private static Node push(Node head, int data) {
        Node newNode = new Node();
        newNode.data = data;
        newNode.next = head;
        head = newNode;
        return head;
    }

    private static void printList(Node node) {
        while (node != null) {
            System.out.printf("%d", node.data);
            node = node.next;
        }
    }

    public static void main(String[] args) {
        Node head = null;
        head = push(head, 12);
        head = push(head, 11);
        head = push(head, 8);
        head = push(head, 5);
        head = push(head, 18);
        head = push(head, 15);

        printList(head);
        System.out.println();
        System.out.println("=".repeat(30));
        System.out.println(countRotation(head));
    }
}

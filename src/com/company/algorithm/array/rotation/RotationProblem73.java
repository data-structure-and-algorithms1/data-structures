package com.company.algorithm.array.rotation;

//Reversal algorithm for right rotation of an array
public class RotationProblem73 {

    private static void reversal(int[] arr, int start, int end) {
        while (start < end) {
            int temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }

    private static void rotate(int[] arr, int n, int k) {
        reversal(arr, 0, n - 1);
        reversal(arr, 0, k - 1);
        reversal(arr, k, n - 1);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        rotate(arr, arr.length, 3);
    }
}

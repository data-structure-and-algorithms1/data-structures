package com.company.algorithm.array.rotation;

//Check if it is possible to make array increasing or decreasing by rotating the array
public class RotationProblem55 {
//
//    private static int ifArrayAlreadyDecreasing(int[] arr, int n) {
//        for (int i = 0; i < n - 2; i++) {
//            if (!(arr[i] > arr[i + 1] && arr[i + 1] > arr[i + 2])) {
//                return 1;
//            }
//        }
//        return 0;
//    }
//
//    private static int ifArrayAlreadyIncreasing(int[] arr, int n) {
//        for (int i = 0; i < n - 2; i++) {
//            if (!(arr[i] < arr[i + 1] && arr[i + 1] < arr[i + 2])) {
//                return 1;
//            }
//        }
//        return 0;
//    }
//
//    private static int minimumValue(int[] arr, int n) {
//        int val1 = Integer.MAX_VALUE;
//        int mini = -1;
//        for (int i = 0; i < n; i++) {
//            if (arr[i] < val1) {
//                mini = i;
//                val1 = arr[i];
//            }
//        }
//        return mini;
//    }
//
//    private static int maximumValue(int[] arr, int n) {
//        int val2 = Integer.MIN_VALUE;
//        int maxi = 0;
//        for (int i = 0; i < n; i++) {
//            if (arr[i] > val2) {
//                maxi = i;
//                val2 = arr[i];
//            }
//        }
//        return maxi;
//    }
//
//    private static int canMakeArrayIncreasing(int[] arr, int maxi) {
//        for (int i = 0; i < maxi; i++) {
//            if (arr[i] > arr[i + 1]) {
//                return 0;
//            }
//        }
//        return 1;
//    }
//
//    private static int ifArrayIncreasingAgainOrNot(int[] arr, int n, int mini) {
//        for (int i = mini; i < n - 1; i++) {
//            if (arr[i] > arr[i + 1]) {
//                return 1;
//            }
//        }
//        return 0;
//    }
//
//    private static int canMakeArrayDecreasing(int[] arr, int mini) {
//        for (int i = 0; i < mini; i++) {
//            if (arr[i] < arr[i + 1]) {
//                return 0;
//            }
//        }
//        return 1;
//    }
//
//    private static int ifArrayDecreasingAgainOrNot(int[] arr, int n, int maxi) {
//        for (int i = maxi; i < n - 1; i++) {
//            if (arr[i] < arr[i + 1]) {
//                return 0;
//            }
//        }
//        return 1;
//    }
//
//    private static boolean isPossible(int[] a, int n) {
//        if (n <= 2) return true;
//
//        int flag = ifArrayAlreadyDecreasing(a, n);
//        if (flag == 0) return true;
//
//        flag = ifArrayAlreadyIncreasing(a, n);
//        if (flag == 0) return true;
//
//        int maxi = maximumValue(a, n);
//        int mini = minimumValue(a, n);
//
//        flag = canMakeArrayIncreasing(a, maxi);
//        if (flag == 1 && maxi + 1 == mini) {
//            flag = ifArrayIncreasingAgainOrNot(a, n, mini);
//            if (flag == 1) return true;
//        }
//
//        flag = canMakeArrayDecreasing(a, mini);
//        if (flag == 1 && maxi - 1 == mini) {
//            flag = ifArrayDecreasingAgainOrNot(a, n, maxi);
//            return flag == 1;
//        }
//
//        return false;
//    }

    // =======================================================================

    static boolean isPossibleV2(int a[], int n) {
        // If size of the array is less than 3
        if (n <= 2)
            return true;

        int flag = 0;

        // Check if the array is already decreasing
        for (int i = 0; i < n - 2; i++) {
            if (!(a[i] > a[i + 1] &&
                    a[i + 1] > a[i + 2])) {
                flag = 1;
                break;
            }
        }

        // If the array is already decreasing
        if (flag == 0)
            return true;

        flag = 0;

        // Check if the array is already increasing
        for (int i = 0; i < n - 2; i++) {
            if (!(a[i] < a[i + 1] &&
                    a[i + 1] < a[i + 2])) {
                flag = 1;
                break;
            }
        }

        // If the array is already increasing
        if (flag == 0)
            return true;

        // Find the indices of the minimum
        // && the maximum value
        int val1 = Integer.MAX_VALUE, mini = -1,
                val2 = Integer.MIN_VALUE, maxi = 0;
        for (int i = 0; i < n; i++) {
            if (a[i] < val1) {
                mini = i;
                val1 = a[i];
            }
            if (a[i] > val2) {
                maxi = i;
                val2 = a[i];
            }
        }

        flag = 1;

        // Check if we can make array increasing
        for (int i = 0; i < maxi; i++) {
            if (a[i] > a[i + 1]) {
                flag = 0;
                break;
            }
        }

        // If the array is increasing upto max index
        // && minimum element is right to maximum
        if (flag == 1 && maxi + 1 == mini) {
            // Check if array increasing again or not
            for (int i = mini; i < n - 1; i++) {
                if (a[i] > a[i + 1]) {
                    flag = 0;
                    break;
                }
            }
            if (flag == 1)
                return true;
        }

        flag = 1;

        // Check if we can make array decreasing
        for (int i = 0; i < mini; i++) {
            if (a[i] < a[i + 1]) {
                flag = 0;
                break;
            }
        }

        // If the array is decreasing upto min index
        // && minimum element is left to maximum
        if (flag == 1 && maxi - 1 == mini) {

            // Check if array decreasing again or not
            for (int i = maxi; i < n - 1; i++) {
                if (a[i] < a[i + 1]) {
                    flag = 0;
                    break;
                }
            }
            return flag == 1;
        }

        // If it is not possible to make the
        // array increasing or decreasing
        return false;
    }


    public static void main(String[] args) {
       // int[] arr = {1, 2, 4, 3, 5};
        int[] arr = {4, 5, 6, 2, 3};
        int n = arr.length;

        if (isPossibleV2(arr, n)) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}

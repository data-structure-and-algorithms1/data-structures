package com.company.algorithm.array.rotation;

//Swap characters in a String
public class RotationProblem58 {

    private static String swapCars(String s, int c, int b) {
        int n = s.length();

        c = c % n;
        if (c == 0) {
            return s;
        }
        int f = b / n;
        int r = b % n;

        String p1 = rotateLeft(s.substring(0, c), ((n % c) * f) % c);
        String p2 = rotateLeft(s.substring(c), ((c * f) % (n - c)));

        char[] a = (p1 + p2).toCharArray();

        for (int i = 0; i < r; i++) {
            char temp = a[i];
            a[i] = a[(i + c) % n];
            a[(i + c) % n] = temp;
        }

        return new String(a);
    }

    private static String rotateLeft(String s, int p) {
        return s.substring(p) + s.substring(0, p);
    }


    //Time Complexity: O(n)
    //Space Complexity: O(n)
    public static void main(String[] args) {
        String s = "ABCDEFGH";
        int b = 4;
        int c = 3;

        System.out.println(swapCars(s, c, b));
    }

}

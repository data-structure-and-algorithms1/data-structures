package com.company.algorithm.array.rotation;

//Maximum sum of i*arr[i] among all rotations of a given array
public class RotationProblem8 {

    // ---------------- VERSION 1 ----------------- O(N^2)
    private static int findMaximum(int[] arr) {
        int result = 0;
        int n = arr.length;
        for (int i = 0; i < n; i++) {
            int currSum = 0;
            for (int j = 0; j < n; j++) {
                int index = (i + j) % n;
                currSum += j * arr[index];
            }
            result = Math.max(result, currSum);
        }
        return result;
    }

    //----------------------------------------------------------

    // --------------------- VERSION 2 ----------------  O(N)
    private static int findMaximumV2(int[] arr) {
        int sum = 0;
        int currVal = 0;
        int n = arr.length;
        for (int i = 0; i < n; i++) {
            sum += arr[i];
            currVal += i * arr[i];
        }
        int maxVal = currVal;

        for (int i = 1; i < n; i++) {
            int nextVal = currVal - (sum - arr[i - 1]) + arr[i - 1] * (n - 1);
            currVal = nextVal;
            maxVal = Math.max(maxVal, nextVal);
        }
        return maxVal;
    }

    public static void main(String[] args) {
        int[] arr = {8, 3, 1, 2};
        //System.out.println(findMaximum(arr));
        System.out.println(findMaximumV2(arr));
    }
}

package com.company.algorithm.array.rotation;

//Check if all rows of a matrix are circular rotations of each other
public class RotationProblem94 {

    private static boolean isPermutedMatrix(int[][] mat, int n) {
        String strCat = "";
        for (int i = 0; i < n; i++) {
            strCat = strCat + " - " + mat[0][i];
        }

        strCat = strCat + strCat;

        for (int i = 1; i < n; i++) {
            String currStr = "";
            for (int j = 0; j < n; j++) {
                currStr = currStr + "-" + mat[i][j];
            }
            if (strCat.contentEquals(currStr)) {
                return false;
            }
        }
        return true;
    }


    public static void main(String[] args) {
        int n = 3;
        int[][] mat = {
                {1, 2, 3},
                {3, 2, 1},
                {1, 3, 2}
        };
        System.out.println(isPermutedMatrix(mat, n) ? "Yes" : "No");
    }
}

package com.company.algorithm.array.rotation;

//Check if a string can be obtained by rotating another string d places
public class RotationProblem59 {


    private static boolean checkIsObtained(String s1, String s2, int d) {
        if (s1.length() != s2.length()) return false;
        if (s1.equals(s2)) return true;

        if (rightRotate(s1, s2, d)) return true;
        return leftRotate(s1, s2, d);
    }

    private static boolean rightRotate(String s1, String s2, int d) {
        for (int i = 0; i < d; i++) {
            s2 = s2.substring(1) + s2.charAt(0);
            if (s1.equals(s2)) {
                return true;
            }
        }
        return false;
    }

    private static boolean leftRotate(String s1, String s2, int d) {
        for (int i = 0; i < d; i++) {
            s2 = s2.charAt(s2.length() - 1) + s2.substring(0, s2.length() - 1);
            if (s1.equals(s2)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        String s1 = "abcdefg";
        String s2 = "cdefgab";
        int d = 2;

        System.out.println(checkIsObtained(s1, s2, d) ? "YES" : "NO");
    }

}

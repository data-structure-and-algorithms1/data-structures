package com.company.algorithm.array.rotation;

//Mth element after K Right Rotations of an Array
public class RotationProblem40 {

    private static int findMthElement(int[] arr, int k, int n, int m) {
        k %= n;
        int index;
        if (k >= m)
            index = (n - k) + (m - 1);
        else
            index = (m - k - 1);

        return arr[index];
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int k = 3;
        int m = 2;
        int n = arr.length;
        System.out.println(findMthElement(arr, k, n, m));
    }
}

package com.company.algorithm.array.rotation;

//Rotate Matrix Elements
public class RotationProblem95 {

    private static void rotateMatrix(int m, int n, int[][] matrix) {
        int row = 0;
        int col = 0;
        int prev;
        int curr;

        // row - Staring row index
        //        m - ending row index
        //        col - starting column index
        //        n - ending column index
        //        i - iterator
        while (row < m && col < n) {
            if (row + 1 == m || col + 1 == n) {
                break;
            }

            prev = matrix[row + 1][col];

            // Move elements of first row
            // from the remaining rows
            // --- > right
            for (int i = col; i < n; i++) {
                curr = matrix[row][i];
                matrix[row][i] = prev;
                prev = curr;
            }
            row++;

            // Move elements of last column
            // from the remaining columns
            // up to down
            for (int i = row; i < m; i++) {
                curr = matrix[i][n - 1];
                matrix[i][n - 1] = prev;
                prev = curr;
            }
            n--;

            // Move elements of last row
            // from the remaining rows
            // right to left
            if (row < m) {
                for (int i = n - 1; i >= col; i--) {
                    curr = matrix[m - 1][i];
                    matrix[m - 1][i] = prev;
                    prev = curr;
                }
            }
            m--;

            // Move elements of first column
            // from the remaining rows
            // down to up
            // like sprint
            if (col < n) {
                for (int i = m - 1; i >= row; i--) {
                    curr = matrix[i][col];
                    matrix[i][col] = prev;
                    prev = curr;
                }
            }
            col++;
        }

        for (int[] ints : matrix) {
            for (int j = 0; j < matrix.length; j++)
                System.out.print(ints[j] + " ");
            System.out.print("\n");
        }
    }

    public static void main(String[] args) {
        int a[][] = {{1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}};
        rotateMatrix(a.length, a.length, a);
    }

}

package com.company.algorithm.array.rotation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

//Minimum circular rotations to obtain a given numeric string by avoiding a set of given strings
public class RotationProblem49 {

    private static String startString(int N) {
        StringBuilder start = new StringBuilder();
        for (int i = 0; i < N; i++) {
            start.append("0");
        }
        return start.toString();
    }

    private static int minCircularRotations(String target, ArrayList<String> blocked, int N) {
        String start = startString(N);

        HashSet<String> avoid = new HashSet<>(blocked);

        if (avoid.contains(start))
            return -1;

        if (avoid.contains(target))
            return -1;

        Queue<String> queue = new LinkedList<>();
        queue.add(start);

        int count = 0;

        while (!queue.isEmpty()) {
            count++;

            int size = queue.size();
            for (int i = 0; i < size; i++) {
                StringBuilder sb = new StringBuilder(queue.poll());
                for (int j = 0; j < N; j++) {
                    char ch = sb.charAt(j);

                    sb.setCharAt(j, (char) (sb.charAt(j) + 1));
                    if (sb.charAt(j) > '9')
                        sb.setCharAt(j, '0');

                    // If target is reached
                    if (sb.toString().equals(target))
                        return count;

                    // If the String formed
                    // is not one to be avoided
                    if (!avoid.contains(sb.toString()))
                        queue.add(sb.toString());

                    // Add it to the list of
                    // Strings to be avoided
                    // to prevent visiting
                    // already visited states
                    avoid.add(sb.toString());

                    // Decrease the current
                    // value by 1 and repeat
                    // the similar checkings
                    sb.setCharAt(j, (char) (ch - 1));
                    if (sb.charAt(j) < '0')
                        sb.setCharAt(j, '9');
                    if (sb.toString().equals(target))
                        return count;
                    if (!avoid.contains(sb.toString()))
                        queue.add(sb.toString());
                    avoid.add(sb.toString());

                    // Restore the original
                    // character
                    sb.setCharAt(j, ch);
                }
            }
        }
        return -1;
    }


    public static void main(String[] args) {
        int N = 4;
        String target = "7531";
        ArrayList<String> blocked = new ArrayList<>(Arrays.asList(
                "1543",
                "7434",
                "7300",
                "7321",
                "2427"
        ));

        System.out.println(minCircularRotations(target,blocked,N));
    }
}

package com.company.algorithm.array.rotation;

//Queries for rotation and Kth character of the given string in constant time
//(1, K): Left rotate the string by K characters.
//(2, K): Print the Kth character of the string.
public class RotationProblem57 {

    // ======================= VERSION 1 ===========================
    // for left rotation
    private static void rotationByQueries(String str, int[][] query) {

        for (int[] ints : query) {
            if (ints[0] == 1) {
                str = rotate(str, ints[1]);
            }
            if (ints[0] == 2) {
                System.out.println(str.charAt(ints[1]));
            }
        }
    }

    private static String rotate(String str, int k) {
        for (int i = 0; i < k; i++)
            str = str.substring(1) + str.charAt(0);
        return str;
    }

    // =========================================================

    // ======================== VERSION 2 =======================

    private static void performQueries(String str, int n, int[][] queries, int q) {
        // Pointer pointing to the current
        // starting character of the string
        int ptr = 0;

        for (int i = 0; i < q; i++) {
            if (queries[i][0] == 1) {
                ptr = (ptr + queries[i][1]) % n;
            }
            else {
                int k = queries[i][1];
                int index = (ptr + k - 1) % n;
                System.out.println(str.charAt(index));
            }
        }
    }


    public static void main(String[] args) {
        String str = "abcdefgh";
        int[][] query = {{1, 2}, {2, 2}, {1, 4}, {2, 7}};
        rotationByQueries(str, query);
        performQueries(str,str.length(),query, query.length);
    }

}

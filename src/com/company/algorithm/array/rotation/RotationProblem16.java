package com.company.algorithm.array.rotation;

//Find element at given index after a number of rotations
public class RotationProblem16 {

    private static int findElement(int[] arr, int rotation, int[][] ranges, int index) {
        for (int i = rotation - 1; i >= 0; i--) {
            int left = ranges[i][0];
            int right = ranges[i][1];

            if (left <= index && right >= index) {
                if (index == left)
                    index = right;
                else index--;
            }
        }
        return arr[index];
    }


    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int n = arr.length;
        int[][] ranges = {{0, 2}, {0, 3}};

        int index = 1;
        int rotation = 2; // number of rotation
        System.out.println(findElement(arr, rotation, ranges, index));

        /**
         * After rotation array will be
         * 4,3,1,2,5  and you want find index 1
         * and index 1 is 3
         */
    }
}

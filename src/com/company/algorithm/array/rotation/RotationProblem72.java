package com.company.algorithm.array.rotation;

//Rotate the matrix right by K times
public class RotationProblem72 {

    private static final int M = 3;
    private static final int N = 3;

    private static void rotateMatrix(int[][] matrix, int k) {
        int[] temp = new int[M];
        k = k % M;

        for (int i = 0; i < N; i++) {

            for (int j = 0; j < M - k; j++) {
                temp[j] = matrix[i][j];
            }

            for (int f = M - k; f < M; f++) {
                matrix[i][f - M + k] = matrix[i][f];
            }

            for (int t = k; t < M; t++) {
                matrix[i][t] = temp[t - k];
            }
        }
    }

    private static void rotateMatrix2V(int[][] matrix, int k) {
        int[] temp = new int[M];
        k = k % M;
        for (int i = 0; i < N; i++) {
            System.arraycopy(matrix[i], 0, temp, 0, M - k);
            if (M - (M - k) >= 0) System.arraycopy(matrix[i], M - k, matrix[i], M - k - 3 + k, M - (M - k));
            System.arraycopy(temp, 0, matrix[i], k, M - k);
        }
    }

    private static void displayMatrix(int[][] matrix){
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int[][] matrix = {{12, 23, 34},
                {45, 56, 67},
                {78, 89, 91}};
        int k = 2;

        // rotate matrix by k
        rotateMatrix(matrix, k);

        // display rotated matrix
        displayMatrix(matrix);
    }
}

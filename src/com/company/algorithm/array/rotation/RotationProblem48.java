package com.company.algorithm.array.rotation;

//Maximize count of corresponding same elements in given Arrays by Rotation
public class RotationProblem48 {

    private static void maximumEqual(int[] a, int[] b, int n) {
        int[] store = new int[(int) 1e5];

        for (int i = 0; i < n; i++) {
            store[b[i]] = i + 1;
        }

        int[] ans = new int[(int) 1e5];

        for (int i = 0; i < n; i++) {
            int d = Math.abs(store[a[i]] - (i + 1));
            if (store[a[i]] < i + 1) {
                d = n - d;
            }
            ans[d]++;
        }

        int finalAns = 0;
        for (int i = 0; i < 1e5; i++) {
            finalAns = Math.max(finalAns, ans[i]);
        }

        System.out.println(finalAns);
    }

    //Time Complexity: O(N)
    //Auxiliary Space: O(N)
    public static void main(String[] args) {
        int[] arr1 = {1, 7, 3, 9, 5, 6};
        int[] arr2 = {7, 3, 9, 5, 6, 1};
        maximumEqual(arr1, arr2, arr1.length);
    }
}

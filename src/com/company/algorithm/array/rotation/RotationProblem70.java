package com.company.algorithm.array.rotation;

//Check if strings are rotations of each other or not | Set 2
public class RotationProblem70 {

    // =================== VERSION 1 ==============================
    private static boolean checkIsRotate(String s1, String s2) {
        for (int i = 0; i < s2.length(); i++) {
            s2 = s2.substring(1) + s2.charAt(0);
            if (s2.equals(s1)) {
                return true;
            }
        }
        return false;
    }
    // ==============================================================

    // ======================= VERSION 2 ============================

    private static boolean isRotation(String s1, String s2) {
        int n = s1.length();
        int m = s2.length();

        int[] lps = new int[n];

        int len = 0;
        int i = 1;
        lps[0] = 0;

        while (i < n) {
            if (s1.charAt(i) == s2.charAt(len)) {
                lps[i] = ++len;
                i++;
            } else {
                if (len == 0) {
                    lps[i] = 0;
                    i++;
                } else {
                    len = lps[len - 1];
                }
            }
        }
        i = 0;

        for (int j = lps[n - 1]; j < m; j++) {
            if (s2.charAt(j) != s1.charAt(i++))
                return false;
        }
        return true;
    }
    // ======================================================


    public static void main(String[] args) {
        String s1 = "ABCDE";
        String s2 = "BCDEA";

        System.out.println(checkIsRotate(s1, s2));
        System.out.println(isRotation(s1, s2));
    }
}

package com.company.algorithm.array.rotation;

//Find the minimum element in a sorted and rotated array
public class RotationProblem11 {

    private static int findMin(int[] arr, int low, int high) {
        if (high < low) return -1;
        if (high == low) return low;

        int mid = low + (high - low) / 2;
        if (mid < high && arr[mid + 1] < arr[mid])
            return arr[mid + 1];

        if (mid > low && arr[mid] < arr[mid - 1])
            return arr[mid];

        if (arr[high] > arr[mid])
            return findMin(arr, low, mid - 1);
        return findMin(arr, mid + 1, high);
    }

    public static void main(String[] args) {
        int[] arr = {9, 1, 2, 3, 4, 5, 6, 7, 8};
        int n = arr.length;
        System.out.println(findMin(arr, 0, n - 1));
    }
}

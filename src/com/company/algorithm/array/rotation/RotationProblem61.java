package com.company.algorithm.array.rotation;

//Generate all rotations of a number
public class RotationProblem61 {

    // ===================== VERSION 1 ===========================
    private static void rotateNumber(int num) {
        int len = (int) Math.floor(Math.log10(num) + 1);
        int x = (int) Math.pow(10, len - 1);

        for (int i = 0; i < len - 1; i++) {
            int lastDigit = num % 10;
            num = num / 10;
            num += lastDigit * x;
            System.out.println(num);
        }
    }
    // ===============================================================

    // ======================  VERSION 2 ============================

    private static int numberOfDigits(int num) {
        int cnt = 0;
        while (num > 0) {
            cnt++;
            num /= 10;
        }
        return cnt;
    }

    private static void call(int number) {
        int digits = numberOfDigits(number);
        int powTen = (int) Math.pow(10, digits - 1);

        for (int i = 0; i < digits - 1; i++) {
            int firstDigit = number / powTen;

            // formual to calculate left shift previous number
            int left = ((number * 10) + firstDigit) - (firstDigit * powTen * 10);
            System.out.println(left);
            number = left;
        }
    }

    public static void main(String[] args) {
        int num = 123;
       // rotateNumber(num);
        call(num);
    }
}

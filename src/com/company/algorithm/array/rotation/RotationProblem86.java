package com.company.algorithm.array.rotation;

//Quickly find multiple left rotations of an array | Set 1
public class RotationProblem86 {

    private static void preProcess(int[] arr, int n, int[] temp) {
        for (int i = 0; i < n; i++) {
            temp[i] = temp[i + n] = arr[i];
        }
    }

    private static void leftRotate(int n, int k, int[] temp) {
        int start = k % n;

        for (int i = start; i < start + n; i++) {
            System.out.print(temp[i] + " ");
        }
        System.out.println("\n");
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        int n = arr.length;
        int[] temp = new int[2 * n];
        preProcess(arr, n, temp);

        int k = 2;
        leftRotate(n, k, temp);
        k = 3;
        leftRotate(n, k, temp);
    }

}

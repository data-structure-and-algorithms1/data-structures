package com.company.algorithm.array.rotation;

//Lexicographically smallest rotated sequence | Set 2
public class RotationProblem87 {

    private static boolean compareSeq(char[] c, int x, int y, int n) {
        for (int i = 0; i < n; i++) {
            if (c[x] < c[y])
                return false;
            else if (c[x] > c[y])
                return true;
            x = (x + 1) % n;
            y = (y + 1) % n;
        }
        return true;
    }

    private static int smallestSequence(char[] c, int n) {
        int index = 0;
        for (int i = 0; i < n; i++) {

            if (compareSeq(c, index, i, n)) {
                index = i;
            }
        }
        return index;
    }

    private static void printSmallestSequence(String str, int n) {
        char[] c = str.toCharArray();
        int startIndex = smallestSequence(c, n);
        for (int i = 0; i < n; i++) {
            System.out.print(c[(startIndex + i) % n]);
        }
    }

    public static void main(String[] args) {
        String s = "DCACBCAA";
        int n = s.length();
        printSmallestSequence(s, n);
    }

}

package com.company.algorithm.array.rotation;

import java.util.ArrayList;
import java.util.Collections;

//Generating numbers that are divisor of their right-rotations
public class RotationProblem65 {

    private static boolean rightRotationDivisor(int num) {
        int lastDigit = num % 10;
        int rightRotation = (int) (lastDigit * Math.pow(10, (int) (Math.log10(num))) + Math.floor(num / 10));
        return rightRotation % num == 0;
    }

    private static void generateNumbers(int num) {
        for (int i = (int) Math.pow(10, num - 1); i < Math.pow(10, num); i++) {
            if (rightRotationDivisor(i))
                System.out.println(i);
        }
    }

    // ============================= VERSION 2 ================================
    private static void generateNumbersV2(int num) {
        ArrayList<Integer> numbers = new ArrayList<>();
        int kMax;
        int x;

        for (int i = 0; i < 10; i++) {
            kMax = (int) ((Math.pow(10, num - 2) * (10 * i + 1)) / Math.pow(10, num - 1) + i);

            for (int j = 1; j <= kMax; j++) {
                x = (int) (i * (Math.pow(10, num - 1) - j) / (10 * j - 1));

                if ((int) (i * (Math.pow(10, num - 1) - j)) % (10 * j - 1) == 0) {
                    numbers.add(10 * x + j);
                }
            }
        }

        Collections.sort(numbers);
        for (Integer number : numbers) {
            System.out.println(number);
        }
    }

    public static void main(String[] args) {
        int num = 3;
        //generateNumbers(num);
        generateNumbersV2(num);

    }
}

package com.company.algorithm.array.rotation;

//Count of Array elements greater than all elements on its left and at least K elements on its right
// yazdiqlari kod duz deyil islemir
public class RotationProblem39 {
    static class Node {
        int key;
        Node left;
        Node right;
        int height;
        int size;

        public Node(int key) {
            this.key = key;
            this.left = this.right = null;
            this.size = this.height = 1;
        }
    }

    // helper class to pass Integer as reference
    static class RefInteger {
        Integer value;

        public RefInteger(Integer value) {
            this.value = value;
        }
    }

    // utility for get height
    static int height(Node node) {
        if (node == null)
            return 0;
        return node.height;
    }

    static int size(Node node) {
        if (node == null)
            return 0;
        return node.size;
    }

    static Node rightRotate(Node y) {
        Node x = y.left;
        Node T2 = x.right;

        x.right = y;
        y.left = T2;

        // update height
        y.height = Math.max(height(y.left), height(y.right)) + 1;
        x.height = Math.max(height(x.left), height(x.right)) + 1;

        // update size
        y.size = size(y.left) + size(y.right) + 1;
        x.size = size(x.left) + size(x.right) + 1;

        return x;
    }

    static Node leftRotate(Node x) {
        Node y = x.right;
        Node T2 = y.left;

        y.left = x;
        x.right = T2;

        // update height
        x.height = Math.max(height(x.left), height(x.right)) + 1;
        y.height = Math.max(height(y.left), height(y.right)) + 1;

        // update size
        x.size = size(x.left) + size(x.right) + 1;
        y.size = size(y.left) + size(y.right) + 1;

        return y;
    }

    private static int getBalance(Node n) {
        if (n == null)
            return 0;
        return height(n.left) - height(n.right);
    }

    private static Node insert(Node node, int key, RefInteger count) {
        if (node == null)
            return new Node(key);

        if (key < node.key) {
            node.left = insert(node.left, key, count);
        } else {
            node.right = insert(node.right, key, count);

            count.value = count.value + size(node.left) + 1;
        }

        node.height = Math.max(height(node.left), height(node.right)) + 1;
        node.size = Math.max(size(node.left), size(node.right)) + 1;

        int balance = getBalance(node);

        // left left
        if (balance > 1 && key < node.left.key)
            return rightRotate(node);

        // right right
        if (balance < -1 && key > node.right.key)
            return leftRotate(node);

        // left right
        if (balance > 1 && key > node.left.key) {
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }

        // right left
        if (balance < -1 && key < node.right.key) {
            node.right = rightRotate(node.right);
            return leftRotate(node);
        }
        return node;
    }

    private static void constructLowerArray(int[] arr, RefInteger[] countSmaller, int n) {
        int i, j;
        Node root = null;
        for (i = 0; i < n; i++) {
            countSmaller[i] = new RefInteger(0);
        }

        for (i = n - 1; i >= 0; i--) {
            root = insert(root, arr[i], countSmaller[i]);
        }
    }

    private static int countElements(int[] arr, int n, int k) {
        int count = 0;
        RefInteger[] countSmaller = new RefInteger[n];
        constructLowerArray(arr, countSmaller, n);

        int maxi = Integer.MIN_VALUE;
        for (int i = 0; i <= (n - k - 1); i++) {
            if (arr[i] > maxi && countSmaller[i].value >= k) {
                count++;
                maxi = arr[i];
            }
        }
        return count;

    }

    public static void main(String[] args) {
        int[] arr = {11, 2, 4, 7, 5, 9, 6, 3};
        int n = args.length;
        int k = 2;
        System.out.println(countElements(arr, n, k));
    }
}

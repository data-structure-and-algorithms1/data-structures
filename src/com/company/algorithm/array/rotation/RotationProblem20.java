package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Rotate all Matrix elements except the diagonal K times by 90 degrees in clockwise direction
public class RotationProblem20 {

    private static void performSwap(int[][] mat, int i, int j) {
        int n = mat.length;
        int lastRow = n - 1 - i;
        int lastColumn = n - 1 - j;

        int temp = mat[i][j];
        mat[i][j] = mat[lastColumn][i];
        mat[lastColumn][i] = mat[lastRow][lastColumn];
        mat[lastRow][lastColumn] = mat[j][lastRow];
        mat[j][lastRow] = temp;
    }

    private static void rotate(int[][] mat, int n, int k) {

        k = k % 4;
        while (k-- > 0) {
            for (int i = 0; i < n / 2; i++) {
                for (int j = i; j < n - i - 1; j++) {

                    // Check if the element
                    // at i, j is not a
                    // diagonal element
                    if (i != j && (i + j) != n - 1) {
                        performSwap(mat, i, j);
                    }
                }
            }
        }
    }

    //Time Complexity: O(N2)
    //Auxiliary Space: O(1)
    public static void main(String[] args) {
        int[][] mat = {
                {1, 2, 3, 4, 5},
                {6, 7, 8, 9, 10},
                {11, 12, 13, 14, 15},
                {16, 17, 18, 19, 20},
                {21, 22, 23, 24, 25},
        };
        int n = mat.length;
        int k = 5; //elements of the matrix K times in clockwise direction
        rotate(mat, n, k);
        System.out.println(Arrays.deepToString(mat));
    }
}

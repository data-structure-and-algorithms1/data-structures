package com.company.algorithm.array.rotation;

//Count rotations divisible by 4
public class RotationProblem90 {

    private static void countRotationDivBy4(String num) {
        int n = num.length();

        int count = 0;
        for (int i = 0; i < n; i++) {
            if (Integer.parseInt(num) % 4 == 0) {
                count++;
            }
            num = num.substring(1) + num.charAt(0);
        }
        System.out.println("COUNT: " + count);
    }

    // ================== VERSION 1 =======================

    private static int countRotations(String num) {
        int n = num.length();

        if (n == 1) {
            int oneDigit = num.charAt(0) - '0';
            if (oneDigit % 4 == 0) {
                return 1;
            }
            return 0;
        }

        // at least 2 digit
        int twoDigit, count = 0;
        for (int i = 0; i < n - 1; i++) {
            twoDigit = (num.charAt(i) - '0') * 10 +
                    (num.charAt(i + 1) - '0');
            if (twoDigit % 4 == 0)
                count++;
        }

        twoDigit = (num.charAt(n - 1) - '0') * 10 +
                (num.charAt(0) - '0');

        if (twoDigit % 4 == 0)
            count++;
        return count;
    }

    public static void main(String[] args) {
        String num = "43292816";
        // countRotationDivBy4(num);
        System.out.println(countRotations(num));
    }

}

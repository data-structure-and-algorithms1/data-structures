package com.company.algorithm.array.rotation;

//Given a sorted and rotated array, find if there is a pair with a given sum
public class RotationProblem96 {

    private static boolean sumOfPair(int[] arr, int n, int x) {
        int i;
        for (i = 0; i < n - 1; i++) {
            if (arr[i] > arr[i + 1])
                break;
        }

        int l = (i + 1) % n;
        int r = i;

        while (l != i) {

            if (arr[l] + arr[r] == x)
                return true;

            if (arr[l] + arr[r] < x)
                l = (l + 1) % n;

            else r = (n + r - 1) % n;
        }
        return false;
    }

    public static void main(String[] args) {
        int[] arr = {11, 15, 6, 8, 9, 10};
        int n = arr.length;
        int x = 16;

        System.out.println(sumOfPair(arr, n, x));
    }
}

package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Modify a matrix by rotating ith row exactly i times in clockwise direction
public class RotationProblem22 {

    // ----------------------- VERSION 1 --------------------------
    public static void rotate(int[][] matrix, int n) {
        for (int i = 1; i < n; i++) {
            rotateByK(matrix, i, n);
        }
    }

    private static void rotateByK(int[][] matrix, int k, int n) {
        int rotate = k;
        while (rotate != 0) {
            int temp = matrix[k][n - 1];
            System.arraycopy(matrix[k], 0, matrix[k], 1, n - 1);
            matrix[k][0] = temp;
            rotate--;
        }
    }
    // -----------------------------------------------------------------------

    // ------------------------- VERSION 2 -------------------------------
    private static void reverse(int[] arr, int start, int end) {
        while (start < end) {
            int temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }

    private static void rotateV2(int[][] matrix) {
        int i = 0;
        for (int[] row : matrix) {
            reverse(row, 0, row.length - 1);
            reverse(row, 0, i - 1);
            reverse(row, i, row.length - 1);
            i++;
        }
    }

    public static void main(String[] args) {
        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int n = matrix.length;
       // rotate(matrix, n);
        rotateV2(matrix);
        System.out.println(Arrays.deepToString(matrix));
    }
}

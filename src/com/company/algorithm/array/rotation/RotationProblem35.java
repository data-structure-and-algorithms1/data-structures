package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Minimize characters to be changed to make the left and right rotation of a string same
public class RotationProblem35 {

    // -------------------------- VERSION 1 ----------------------------------
    private static void maximum(int[] arr, int[][] query, int q) {
        int sum = 0;
        for (int i = 0; i < q; i++) {
            if (query[i][0] == 1) {
                rotateArray(arr, query[i][1]);
            }
            sum = sumOfNumbers(arr, query[i][1]);
        }
        System.out.println(Arrays.toString(arr));
        System.out.println(sum);
    }

    private static void rotateArray(int[] arr, int k) {
        for (int i = 0; i < k; i++) {
            int temp = arr[0];
            System.arraycopy(arr, 1, arr, 0, arr.length - 1);
            arr[arr.length - 1] = temp;
        }
    }

    private static int sumOfNumbers(int[] arr, int range) {
        int sum = 0;
        for (int i = 0; i < range; i++) {
            sum += arr[i];
        }
        return sum;
    }
    // -------------------------------------------------------------------------

    // ------------------------ VERSION 2 -----------------------------------------

    private static int maxSum(int[] arr, int n, int k) {
        int i, maxSum = 0, sum = 0;

        for (i = 0; i < k; i++) {
            sum += arr[i];
        }
        maxSum = sum;
        while (i < n) {
            sum = sum - arr[i - k] + arr[i];
            maxSum = Math.max(maxSum, sum);
            i++;
        }
        return maxSum;
    }

    private static int gcd(int n1, int n2) {
        if (n2 == 0)
            return n1;
        else
            return gcd(n2, n1 % n2);
    }

    private static int[] rotateArr(int[] arr, int n, int d) {
        int i = 0, j = 0;
        d = d % n;

        int noOfSets = gcd(d, n);
        for (i = 0; i < noOfSets; i++) {
            int temp = arr[i];
            j = i;
            while (true) {
                int k = j + d;
                if (k >= n) {
                    k = k - n;
                }
                if (k == i) {
                    break;
                }
                arr[j] = arr[k];
                j = k;
            }
            arr[j] = temp;
        }
        return arr;
    }

    private static void performQuery(int[] arr, int[][] query, int q) {
        int n = arr.length;
        int sum = 0;
        for (int i = 0; i < q; i++) {
            if (query[i][0] == 1) {
                rotateArr(arr, n, query[i][1]);
            } else {
                sum = maxSum(arr, n, query[i][1]);
            }
        }

        System.out.println(Arrays.toString(arr));
        System.out.println(sum);

    }

    public static void main(String[] args) {
//        int[] arr = {1, 2, 3, 4, 5};
//        int Q = 2;
//        int[][] query = {{1, 2}, {2, 3}};
//        int N = arr.length;

        //==========================================
        int[] arr = {1, 2, 3, 4, 5};
        int q = 5;
        int query[][] = {{1, 2}, {2, 3},
                {1, 3}, {1, 1},
                {2, 4}};
        // maximum(arr, query, q);
        performQuery(arr, query, q);
    }
}

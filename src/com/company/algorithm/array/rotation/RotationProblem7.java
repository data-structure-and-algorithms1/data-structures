package com.company.algorithm.array.rotation;

//Find maximum value of Sum( i*arr[i]) with only rotations on given array allowed
public class RotationProblem7 {

    // ----------------- VERSION 1 ------------------------- O(N^2)
    private static int findMaxSum(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            int currSum = 0;
            for (int j = 0; j < arr.length; j++) {
                currSum += arr[j] * j;
            }
            sum = Math.max(currSum, sum);
            rotateArray(arr);
        }
        return sum;
    }

    private static void rotateArray(int[] arr) {
        int temp = arr[0];
        int n = arr.length;
        System.arraycopy(arr, 1, arr, 0, n - 1);
        arr[n - 1] = temp;
    }
//------------------------------------------------------------------

    // ------------------ VERSION 2 --------------  O(N)
    private static int findMaxSumV2(int[] arr) {
        int sum = 0;
        int currVal = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
            currVal += i * arr[i];
        }
        int maxVal = currVal;

        for (int j = 1; j < arr.length; j++) {
            currVal += sum - arr.length * arr[arr.length - j];
            if (currVal > maxVal)
                maxVal = currVal;
        }
        return maxVal;
    }

    public static void main(String[] args) {
        int[] arr = {10, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        //System.out.println(findMaxSum(arr));
        System.out.println(findMaxSumV2(arr));
    }
}

package com.company.algorithm.array.rotation;

//Maximize sum of diagonal of a matrix by rotating all rows or all columns
public class RotationProblem32 {

    private static int maxDiagonalOfSum(int[][] mat) {
        int sum = 0;
        for (int i = 0; i < mat.length; i++) {
            int currSum = 0;

            for (int j = 0; j < mat.length; j++) {
                currSum += mat[j][(i + j) % mat.length];
            }
            sum = Math.max(sum, currSum);
        }
        for (int i = 0; i < mat.length; i++) {
            int currSum = 0;
            for (int j = 0; j < mat.length; j++) {
                currSum += mat[(i + j) % mat.length][j];
            }
            sum = Math.max(sum, currSum);
        }
        return sum;
    }

    // --------------------------------- VERSION 2 ----------------------------------

    private static int maxDiagonalOfSumV2(int[][] mat) {
        int sum = 0;
        for (int[] ints : mat) {
            int max = 0;
            for (int j = 0; j < mat.length; j++) {
                max = Math.max(ints[j], max);
            }
            sum += max;
        }
        return sum;
    }

    //Time Complexity: O(N^2)
    //Auxiliary Space: O(1)
    public static void main(String[] args) {
        //int mat[][] = {{1, 1, 2}, {2, 1, 2}, {1, 2, 2}}; //bunda duz isleyir
        int mat[][] = {{10, 2, 3}, {7, 1, 5}, {9, 2, 2}}; // ancaq bu versiyada hemin riyazi hesablama tam duz olmur
        System.out.println(maxDiagonalOfSum(mat));
        System.out.println(maxDiagonalOfSumV2(mat));
    }
}

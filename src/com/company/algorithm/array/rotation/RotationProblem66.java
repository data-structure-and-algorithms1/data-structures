package com.company.algorithm.array.rotation;

//Check if an array is sorted and rotated
public class RotationProblem66 {

    private static void checkIfSortRotated(int[] arr, int n) {
        int minEle = Integer.MAX_VALUE;
        int minIndex = -1;

        for (int i = 0; i < n; i++) {
            if (arr[i] < minEle) {
                minEle = arr[i];
                minIndex = i;
            }
        }
        boolean flag1 = true;

        for (int i = 1; i < minIndex; i++) {
            if (arr[i] < arr[i - 1]) {
                flag1 = false;
                break;
            }
        }

        boolean flag2 = true;

        for (int i = minIndex + 1; i < n; i++) {
            if (arr[i] < arr[i - 1]) {
                flag2 = false;
                break;
            }
        }

        if (minIndex == 0) {
            System.out.println("NO");
            return;
        }

        if (flag1 && flag2 && (arr[n - 1] < arr[0]))
            System.out.println("YES");
        else System.out.println("NO");
    }

    // ===================== VERSION 2 ====================

    private static boolean checkIfSortRotatedV2(int[] arr, int n) {
        int x = 0;
        int y = 0;

        for (int i = 0; i < n - 1; i++) {
            if (arr[i] < arr[i + 1])
                x++;
            else
                y++;
        }

        // If till now both x,y are greater
        // then 1 means array is not sorted.
        // If both any of x,y is zero means
        // array is not rotated.
        if (x == 1 || y == 1) {
            if (arr[n - 1] < arr[0])
                x++;
            else
                y++;
            return x == 1 || y == 1;
        }
        return false;
    }

    public static void main(String[] args) {
        int[] arr = {3, 4, 5, 1, 2};
        int n = arr.length;

        //checkIfSortRotated(arr, n);
        System.out.println(checkIfSortRotatedV2(arr, n) ? "YES" : "NO");
    }
}

package com.company.algorithm.array.rotation;

//Print left rotation of array in O(n) time and O(1) space
public class RotationProblem15 {

    private static void rotate(int[] arr, int k, int n) {
        int mod = k % n;
        for (int i = 0; i < n; i++) {
            System.out.print(arr[(i + mod) % n] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6};
        int n = arr.length;
        int k = 1;

        rotate(arr,k,n);
        rotate(arr,2,n);
        rotate(arr,3,n);

    }
}

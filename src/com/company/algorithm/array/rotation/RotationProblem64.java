package com.company.algorithm.array.rotation;

//Check if it is possible to sort the array after rotating it
public class RotationProblem64 {

    private static boolean isSorted(int[] arr, int n) {
        int cnt1 = 0;
        int cnt2 = 0;

        // is ascending
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] < arr[i + 1]) {
                cnt1++;
            }
        }
        // is descending
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] <= arr[i - 1]) {
                cnt2++;
            }
        }
        return cnt1 == n - 1 || cnt2 == n - 1;
    }

    private static boolean isPossible(int[] a, int n) {

        if (isSorted(a, n)) {
            return true;
        }

        int cnt = 0;
        for (int i = 0; i < n - 1; i++, cnt++) {
            if (a[i] > a[i + 1]) {
                break;
            }
        }
        cnt++;

        for (int k = cnt; k < n - 1; k++) {
            if (a[k] > a[k + 1]) {
                return false;
            }
        }
        return a[n - 1] <= a[0];

    }

    public static void main(String[] args) {
        int[] arr = {6, 8, 1, 2, 5};
        System.out.println(isPossible(arr, arr.length) ? "POSSIBLE" : "NOT POSSIBLE");
    }
}

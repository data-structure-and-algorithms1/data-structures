package com.company.algorithm.array.rotation;

//Rotate a matrix by 90 degree in clockwise direction without using any extra space v 2
public class RotationProblem67 {

    private static void rotate90Clockwise(int arr[][]) {
        int n = arr.length;
        for (int i = 0; i < n / 2; i++) {
            for (int j = 0; j < n - 1; j++) {
                int temp = arr[i][j];
                arr[i][j] = arr[n - 1 - j][i];
                arr[n - 1 - j][i] = arr[n - 1 - i][n - 1 - j];
                arr[n - 1 - i][n - 1 - j] = arr[j][n - 1 - i];
                arr[j][n - 1 - i] = temp;
            }
        }
    }

    private static void printMatrix(int arr[][]) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++)
                System.out.print(arr[i][j] + " ");
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int arr[][] = {{1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}};
        rotate90Clockwise(arr);
        printMatrix(arr);
    }
}

package com.company.algorithm.array.rotation;

//Generate all rotations of a number
public class RotationProblem62 {

    // ===================== VERSION 1 ===========================
    private static void rotateNumber(int num) {
        int len = (int) Math.floor(Math.log10(num) + 1);
        int x = (int) Math.pow(10, len - 1);

        int originalNum = num;
        boolean flag = false;
        for (int i = 0; i < len - 1; i++) {
            int lastDigit = num % 10;
            num = num / 10;
            num += lastDigit * x;
            flag = num >= originalNum;
        }

        System.out.println(flag ? "YES" : "NO");
    }
    // ===============================================================

    public static void main(String[] args) {
        int num = 2214;
        rotateNumber(num);
    }
}

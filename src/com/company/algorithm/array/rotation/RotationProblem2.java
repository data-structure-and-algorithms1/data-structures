package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Reversal algorithm for array rotation
public class RotationProblem2 {

    public static void leftRotate(int[] arr, int d, int n) {
        if (d == 0) return;

        d = d % n;
        reverseArray(arr, 0, d - 1);
        reverseArray(arr, d, n - 1);
        reverseArray(arr, 0, n - 1);
    }

    public static void reverseArray(int[] arr, int start, int end) {
        int temp;
        while (start < end) {
            temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }


    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        int d = 2; // nece defe roatte etsin
        int n = arr.length;
        leftRotate(arr, d, n);
        System.out.println(Arrays.toString(arr));
    }
}

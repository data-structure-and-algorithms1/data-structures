package com.company.algorithm.array.rotation;

//Rotate Doubly linked list by N nodes
public class RotationProblem71 {

    private static class Node {
        char data;
        Node prev;
        Node next;
    }

    static Node head = null;

    private static void rotate(int N) {
        if (N == 0) return;

        Node current = head;
        int count = 1;
        while (count < N && current != null) {
            current = current.next;
            count++;
        }

        if(current == null) return;

        Node NthNode = current;
        while (current.next != null)
            current = current.next;

        current.next = head;
        head = NthNode.next;
        head.prev = null;
        NthNode.next = null;
    }

    private static void push(char newData){
        Node newNode = new Node();
        newNode.data = newData;
        newNode.prev = null;
        newNode.next = head;
        if(head != null)
            head.prev = newNode;
        head = newNode;
    }

    static void printList(Node node)
    {
        while (node != null && node.next != null)
        {
            System.out.print(node.data + " ");
            node = node.next;
        }
        if(node != null)
            System.out.print(node.data);
    }

    public static void main(String[] args)
    {
        push( 'e');
        push( 'd');
        push('c');
        push('b');
        push( 'a');

        int N = 2;

        System.out.println("Given linked list ");
        printList(head);
        rotate( N);
        System.out.println();
        System.out.println("Rotated Linked list ");
        printList(head);
    }
}

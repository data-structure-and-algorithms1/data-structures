package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Sort a Rotated Sorted Array
public class RotationProblem79 {

    private static void reverse(int[] arr, int start, int end) {
        int temp;
        while (start < end) {
            temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }

    private static void findIndexBreakSort(int[] arr, int n) {


        for (int i = 0; i < n; i++) {
            if (arr[i] > arr[i + 1]) {
                reverse(arr, 0, i);
                reverse(arr, i + 1, n);
                reverse(arr, 0, n);
            }
        }
    }

    // ===================== VERSION 2 ====================

    private static int firstStartIndexOfArray(int[] arr, int low, int high) {
        if (low > high) return -1;
        if (low == high) return low;

        int mid = low + (high - low) / 2;

        if (arr[mid] > arr[mid + 1]) {
            return mid + 1;
        }

        if (arr[mid - 1] > arr[mid]) {
            return mid;
        }

        if (arr[low] > arr[mid]) {
            return firstStartIndexOfArray(arr, low, mid - 1);
        } else {
            return firstStartIndexOfArray(arr, mid + 1, high);
        }
    }

    private static void restoreSortedArray(int[]arr, int n){
        if(arr[0] < arr[n-1]) return;

        int start = firstStartIndexOfArray(arr,0,n-1);

        Arrays.sort(arr,0,start);
        Arrays.sort(arr,start,n);
        Arrays.sort(arr);
    }

    public static void main(String[] args) {
        int[] arr = {3, 4, 5, 1, 2};
        int n = arr.length;
        //findIndexBreakSort(arr, n - 1);
        restoreSortedArray(arr,n);
        System.out.println(Arrays.toString(arr));
    }
}

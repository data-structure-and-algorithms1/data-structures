package com.company.algorithm.array.rotation;

//Print array after it is right rotated K times
public class RotationProblem52 {

    private static void rotateArray(int[] arr, int k) {
        int n = arr.length;
        k = k % n;
        for (int i = 0; i < n; i++) {
            if (i < k) {
                System.out.print(arr[n + i - k] + " ");
            }
            else {
                System.out.print(arr[i-k] + " ");
            }
        }
    }


    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int k = 2;
        rotateArray(arr,k);
    }
}

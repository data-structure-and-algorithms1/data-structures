package com.company.algorithm.array.rotation;

//Queries on Left and Right Circular shift on array
public class RotationProblem75 {

    private static void performQuery(int[] arr, int n, int[][] query, int q) {

        for (int i = 0; i < q; i++) {
            if (query[i][0] == 1) {
                rotate(arr, query[i][1], n, 1);
            } else if (query[i][0] == 2) {
                rotate(arr, query[i][1], n, 2);
            } else if (query[i][0] == 3) {
                print(arr, query[i][1], query[i][2]);
            }
        }
    }

    private static void rotate(int[] arr, int k, int n, int rotateDir) {
        for (int i = 0; i < k; i++) {
            if (rotateDir == 1) {
                rotateLeft(arr, n);
            } else {
                rotateRight(arr, n);
            }
        }
    }

    private static void rotateLeft(int[] arr, int n) {
        int temp = arr[0];
        if (n - 1 >= 0) System.arraycopy(arr, 1, arr, 0, n - 1);
        arr[n - 1] = temp;
    }

    private static void rotateRight(int[] arr, int n) {
        int temp = arr[n - 1];
        System.arraycopy(arr, 0, arr, 1, n - 1);
        arr[0] = temp;
    }

    private static void print(int[] arr, int start, int end) {
        for (int i = start; i < end; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int n = 5;
        int[][] query = {{1, 3}, {3, 0, 2}, {2, 1}, {3, 1, 4}};
        int q = query.length;

        performQuery(arr, n, query, q);
    }

}

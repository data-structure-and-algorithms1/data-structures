package com.company.algorithm.array.rotation;

//Find the Mth element of the Array after K left rotations
public class RotationProblem41 {

    private static int findMthElement(int[] arr, int k, int n, int m) {
        k %= n;
        int index = (k + m - 1) % n;
        return arr[index];
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int k = 7;
        int m = 2;
        int n = arr.length;
        System.out.println(findMthElement(arr, k, n, m));
    }
}

package com.company.algorithm.array.rotation;

//Rotate digits of a given number by K
public class RotationProblem30 {

    // ------------------------ VERSION 1 -------------------------
    private static int numberOfDigit(int n) {
        int digit = 0;
        while (n > 0) {
            digit++;
            n /= 10;
        }
        return digit;
    }

    // left rotation
    private static void rotateByK(int n, int k) {
        int x = numberOfDigit(n);
        //k = ((k % x) + x) % x;

        int leftNo = n / (int) (Math.pow(10, x - k));
        n = n % (int) (Math.pow(10, x - k));
        int leftDigit = numberOfDigit(leftNo);
        n = (n * (int) (Math.pow(10, leftDigit))) + leftNo;
        System.out.println(n);
    }
    // -----------------------------------------------------------------

    // ----------------------- VERSION 2 ---------------------------------
    // right rotation
    private static void rotateByKV2(int n, int k) {
        int len = (int) (Math.floor(Math.log10(n)) + 1);
        int x = (int) Math.pow(10, len - 1);

        for (int i = 0; i < k; i++) {
                int lastDigit = n % 10;
                n = n / 10;
                n += lastDigit * x;
        }
        System.out.println(n);
    }

    public static void main(String[] args) {
        int n = 12345;
        int k = 1;
        rotateByK(n, k);
        rotateByKV2(n, k);
    }
}

package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Split the array and add the first part to the end | Set 2
public class RotationProblem69 {

    private static void splitArrayAndRotate(int[] arr, int n, int k) {

        int[] split = new int[k];
        System.arraycopy(arr, 0, split, 0, k);
        if (n - k >= 0) System.arraycopy(arr, 0 + k, arr, 0, n - k);
        //for (int i = 0; i < n - k; i++) {
        //     arr[i] = arr[i + k];
        //    }  // yuxardakinin loop versiyasi
        for (int i = n - k, j = 0; i < n; i++, j++) {
            arr[i] = split[j];
        }

        System.out.println(Arrays.toString(arr));
    }

    // ======================== VERSION 2 ===========================
    private static void splitArrayAndRotateV2(int[] arr, int n, int k) {

        int[] split = new int[k];

        for (int i = 0; i < n - k; i++) {
            if (i < split.length)
                split[i] = arr[i];
            arr[i] = arr[i + k];
        }

        for (int i = n - k, j = 0; i < n; i++, j++) {
            arr[i] = split[j];
        }

        System.out.println(Arrays.toString(arr));
    }

    //============================== VERSION 3 ===================================
    private static void splitArrayAndRotateV3(int[] arr, int n, int k) {
        if(k != 0 || k != n) {
            reverseArray(arr, 0, n - 1);
            reverseArray(arr, 0, n - k - 1);
            reverseArray(arr, n - k, n - 1);
        }
        System.out.println(Arrays.toString(arr));
    }

    private static void reverseArray(int[] arr, int start, int end) {
        while (start < end) {
            int temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }
    //==============================================================

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        int k = 3;
        //splitArrayAndRotate(arr, arr.length, k);
        //splitArrayAndRotateV2(arr, arr.length, k);
        splitArrayAndRotateV3(arr, arr.length, k);
    }

}

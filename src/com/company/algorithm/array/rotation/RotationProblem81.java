package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Find element at given index after a number of rotations
public class RotationProblem81 {

    private static void rotateByRanges(int[] arr, int[][] ranges, int n) {

        for (int[] range : ranges) {
            rotate(arr, range[0], range[1]);
        }
    }

    private static void rotate(int[] arr, int first, int second) {
        int temp = arr[second];
        if (first < second) {
            if (second - first >= 0) System.arraycopy(arr, first, arr, first + 1, second - first);
        }
        else {
            if (first - second >= 0) System.arraycopy(arr, second + 1, arr, second, first - second);
        }
        arr[first] = temp;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[][] ranges = {{0, 2}, {0, 3}};
        int n = arr.length;
        int index = 1;

        rotateByRanges(arr, ranges, n);
        System.out.println(Arrays.toString(arr));
        System.out.println(arr[index]);

    }
}

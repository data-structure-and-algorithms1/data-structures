package com.company.algorithm.array.rotation;

//Count number of rotated strings which have more number of vowels in the first half than second half
public class RotationProblem56 {

    private static int cntRotations(String s, int n) {
        String str = s + s;
        int[] pre = new int[2 * n];

        for (int i = 0; i < 2 * n; i++) {
            if (i != 0) {
                pre[i] += pre[i - 1];
            }
            switch (str.charAt(i)) {
               // case 'a', 'e', 'i', 'o', 'u' -> pre[i]++;
            }
        }

        int res = 0;

        for (int i = n - 1; i < n * 2 - 1; i++) {
            int r = i;
            int l = i - n;

            int x1 = pre[r];
            if (l >= 0) {
                x1 -= pre[l];
            }
            r = i - n / 2;

            int left = pre[r];
            if (l >= 0) {
                left -= pre[l];
            }

            int right = x1 - left;
            if (left > right) {
                res++;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        String str = "aebci";
        int n = str.length();
        System.out.println(cntRotations(str,n));
    }

}

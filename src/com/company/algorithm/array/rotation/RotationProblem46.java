package com.company.algorithm.array.rotation;

//Count of rotations required to generate a sorted array
public class RotationProblem46 {

    //===================== VERSION 1 ==========================
    //Time Complexity: O(N)
    //Auxiliary Space: O(1)
    private static int countGenerateSort(int[] arr, int n) {
        for (int i = 1; i < n; i++) {
            if (arr[i] < arr[i - 1]) {
                return i;
            }
        }
        return 0;
    }
    // ==============================================================

    //================== VERSION 2 ==============================
    private static int countGenerateSortV2(int[] arr, int low, int high) {
        if (low > high) return 0;

        int mid = low + (high - low) / 2;

        if (mid < high && arr[mid] > arr[mid + 1]) {
            return mid + 1;
        }

        if (mid > low && arr[mid] < arr[mid - 1]) {
            return mid;
        }

        if (arr[mid] > arr[low]) {
            return countGenerateSortV2(arr, mid + 1, high);
        }

        if (arr[mid] < arr[high]) {
            return countGenerateSortV2(arr, low, mid - 1);
        } else {
            int rightIndex = countGenerateSortV2(arr, mid + 1, high);
            int leftIndex = countGenerateSortV2(arr, low, mid - 1);

            if (rightIndex == 0) {
                return leftIndex;
            }
            return rightIndex;
        }
    }

    public static void main(String[] args) {
        int[] arr = {6, 1, 2, 3, 4, 5};
        // System.out.println(countGenerateSort(arr, arr.length));
        System.out.println(countGenerateSortV2(arr, 0, arr.length - 1));
    }
}

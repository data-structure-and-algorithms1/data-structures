package com.company.algorithm.array.rotation;

//Count rotations of N which are Odd and Even
public class RotationProblem60 {

    private static void countOddAndEvenRotation(int num) {
        int oddCount = 0;
        int evenCount = 0;

        while (num != 0) {
            int digit = num % 10;
            if (digit % 2 == 0)
                evenCount++;
            else
                oddCount++;

            num = num / 10;
        }

        System.out.println("ODD COUNT: " + oddCount);
        System.out.println("EVEN COUNT: " + evenCount);
    }

    public static void main(String[] args) {
        int num = 183621;
        countOddAndEvenRotation(num);
    }
}

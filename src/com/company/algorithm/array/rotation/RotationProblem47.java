package com.company.algorithm.array.rotation;

//Find array sum using Bitwise OR after splitting given array in two halves after K circular shifts
public class RotationProblem47 {

    static int MAX = 100005;
    static int[] seg = new int[4 * MAX];

    private static void build(int node, int left, int right, int[] arr) {
        if (left == right)
            seg[node] = arr[left];
        else {
            int mid = (left + right) / 2;
            build(2 * node, left, mid, arr);
            build(2 * node + 1, mid + 1, right, arr);

            seg[node] = seg[2 * node] | seg[2 * node + 1];
        }
    }

    private static int query(int node, int left, int rigth, int start, int end, int[] arr) {
        if (left > end || rigth < start)
            return 0;

        if (start <= left && rigth <= end)
            return seg[node];

        int mid = (left + rigth) / 2;
        return ((query(2 * node, left, mid, start, end, arr)) |
                (query(2 * node + 1, mid + 1, rigth, start, end, arr)));
    }

    private static void orSum(int[] arr, int n, int q, int[] k) {
        build(1, 0, n - 1, arr);

        for (int i = 0; i < q; i++) {
            int j = k[i] % (n / 2);

            int sec = query(1, 0, n - 1, n / 2 - j, n - j - 1, arr);

            int first = (query(1, 0, n - 1, 0, n / 2 - 1 - j, arr) |
                    (query(1, 0, n - 1, n - j, n - 1, arr)));

            int temp = sec + first;
            System.out.println(temp);
        }
    }


    public static void main(String[] args) {
        int a[] = { 7, 44, 19, 86, 65, 39, 75, 101 };
        int n = a.length;
        int q = 2;

        int k[] = { 4, 2 };
        orSum(a, n, q, k);
    }
}

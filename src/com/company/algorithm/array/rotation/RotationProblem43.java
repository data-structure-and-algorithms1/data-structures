package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Rotate all odd numbers right and all even numbers left in an Array of 1 to N
public class RotationProblem43 {

    // ====================== VERSION 1 ============================
    private static void rotate(int[] arr, int n) {
        leftRotate(arr, n);
        rightRotate(arr, n);
    }

    private static void rightRotate(int[] arr, int n) {
        int start = arr[n - 2];
        for (int i = n - 4; i >= 0; i -= 2) {
            arr[i + 2] = arr[i];
        }
        arr[0] = start;
    }

    private static void leftRotate(int[] arr, int n) {
        int last = arr[1];
        for (int i = 3; i < n; i += 2) {
            arr[i - 2] = arr[i];
        }
        arr[n - 1] = last;
    }
    // ======================================================

    // ========================== VERSION 2 ==========================
    private static void rotateV2(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            if (arr[i] % 2 == 0) {
                rightRotateV2(arr, n);
            } else leftRotateV2(arr, n);
        }
    }

    private static void rightRotateV2(int[] arr, int n) {
        int start = arr[n - 1];
        for (int i = n - 1; i > 0; i--) {
            arr[i] = arr[i - 1];
        }
        arr[0] = start;
    }

    private static void leftRotateV2(int[] arr, int n) {
        int last = arr[0];
        for (int i = 0; i < n - 1; i++) {
            arr[i] = arr[i + 1];
        }
        arr[n - 1] = last;
    }
    //=============================================================

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        int n = arr.length;
        rotate(arr, n);
        System.out.println(Arrays.toString(arr));

        int[] arr2 = {5, 2, 6, 4, 3, 8, 7, 1};
        rotateV2(arr2, n);
        System.out.println(Arrays.toString(arr2));
    }
}

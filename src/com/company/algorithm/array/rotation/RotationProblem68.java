package com.company.algorithm.array.rotation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//Elements that occurred only once in the array
public class RotationProblem68 {

    //Time Complexity: O(Nlogn)
    //Space Complexity: O(1)
    private static void occurredOnce(int[] arr, int n) {

        Arrays.sort(arr);

        if (arr[0] != arr[1])
            System.out.println(arr[0]);

        for (int i = 1; i < n - 1; i++) {
            if (arr[i] != arr[i - 1] && arr[i] != arr[i + 1]) {
                System.out.println(arr[i]);
            }
        }

        if (arr[n - 1] != arr[n - 2])
            System.out.println(arr[n - 1]);
    }

    // ============================ VERSION 2 ===========================

    //Time Complexity: O(N)
    //Space Complexity: O(N)
    private static void occurredOnceV2(int[] arr, int n) {
        HashMap<Integer, Integer> mp = new HashMap<>();

        for (int i = 0; i < n; i++) {
            if (mp.containsKey(arr[i]))
                mp.put(arr[i], 1 + mp.get(arr[i]));
            else mp.put(arr[i], 1);
        }

        for (Map.Entry entry : mp.entrySet()) {
            if (Integer.parseInt(String.valueOf(entry.getValue())) == 1)
                System.out.println(entry.getKey() + " ");
        }
    }

    public static void main(String[] args) {
        int arr[] = {7, 7, 8, 8, 9, 1, 1, 4, 2, 2};
        //occurredOnce(arr, arr.length);
        occurredOnceV2(arr, arr.length);
    }
}

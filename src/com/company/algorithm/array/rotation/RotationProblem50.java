package com.company.algorithm.array.rotation;

//Range sum queries for anticlockwise rotations of Array by K indices
public class RotationProblem50 {

    private static void rotatedSumQuery(int[] arr, int n, int[][] query, int Q) {
        int[] prefix = new int[n * 2];
        for (int i = 0; i < n; i++) {
            prefix[i] = arr[i];
            prefix[n + i] = arr[i];
        }

        for (int i = 1; i < n * 2; i++) {
            prefix[i] += prefix[i - 1];
        }

        int start = 0;

        for (int q = 0; q < Q; q++) {
            if (query[q][0] == 1) {
                int k = query[q][1];
                start = (start + k) % n;
            } else if (query[q][0] == 2) {
                int left, right;
                left = query[q][1];
                right = query[q][2];
                if (start + left == 0) {
                    System.out.println(prefix[start + right]);
                } else {
                    System.out.println(prefix[start + right] - prefix[start + left - 1]);
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6};
        int[][] query = {{2, 1, 3}, {1, 3}, {2, 0, 3}, {1, 4}, {2, 3, 5}};
        int Q = 5;
        int n = arr.length;
        rotatedSumQuery(arr, n, query, Q);
    }
}

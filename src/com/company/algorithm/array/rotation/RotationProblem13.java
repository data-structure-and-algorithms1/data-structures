package com.company.algorithm.array.rotation;

//Find a rotation with maximum hamming distance
public class RotationProblem13 {

    private static int[] doubleArray(int[] arr, int n) {
        int[] tempArr = new int[n * 2];
        for (int i = 0, j = 0; i < tempArr.length; i++, j++) {
            if (j == n) j = 0;
            tempArr[i] = arr[j];
        }
        return tempArr;
    }

    private static int hammingDistance(int[] arr, int n) {
        int[] tempArr = doubleArray(arr, n);
        int maxHam = 0;
        for (int i = 1; i < n; i++) {
            int currHam = 0;
            for (int j = i, k = 0; j < (i + n); j++, k++) {
                if (tempArr[j] != arr[k])
                    currHam++;
            }
            if (currHam == n) {
                return n;
            }
            maxHam = Math.max(maxHam, currHam);
        }
        return maxHam;
    }

    public static void main(String[] args) {
        //int[] arr = {1, 4, 1};
        int[] arr = {1, 2, 3, 1, 4};
        int n = arr.length;
        System.out.println(hammingDistance(arr, n));
    }
}

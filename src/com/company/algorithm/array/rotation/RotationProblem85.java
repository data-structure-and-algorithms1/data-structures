package com.company.algorithm.array.rotation;

//Minimum rotations required to get the same string
public class RotationProblem85 {

    private static int rotateAndCheck(String s) {
        int n = s.length();
        String temp = s;
        int cnt = 0;
        for (int i = 0; i < n; i++) {
            cnt++;
            s = s.substring(1) + s.charAt(0);
            if (s.equals(temp)) {
                return cnt;
            }
        }
        return cnt;
    }

    // ======================== VERSION 2 ======================
    private static int rotateAndCheckV2(String s) {
        int n = s.length();
        String temp = s.repeat(2);
        for (int i = 1; i <= n; i++) {
            String subString = temp.substring(i, i + n);

            if (s.equals(subString)) {
                return i;
            }
        }
        return n;
    }


    public static void main(String[] args) {
        String s = "aaaa";
        System.out.println(rotateAndCheck(s));
        System.out.println(rotateAndCheckV2(s));
    }
}

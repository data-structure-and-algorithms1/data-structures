package com.company.algorithm.array.rotation;

import java.util.Arrays;

//Search an element in a sorted and rotated array
//Time Complexity: O(log n).
//Binary Search requires log n comparisons to find the element. So time complexity is O(log n).
//Space Complexity: O(1).
public class RotationProblem5 {

    // --------------------- VERSION 1 ----------------------------------
    private static int pivotedBinarySearch(int[] arr, int n, int key) {
        int pivot = findPivot(arr, 0, n - 1);
        if (pivot == -1)
            return binarySearch(arr, 0, n - 1, key);

        if (arr[pivot] == key)
            return pivot;
        if (arr[0] <= key)
            return binarySearch(arr, 0, pivot - 1, key);
        return binarySearch(arr, pivot + 1, n - 1, key);
    }

    private static int findPivot(int[] arr, int low, int high) {
        if (high < low) return -1;
        if (high == low) return low;

        int mid = (low + high) / 2;
        if (mid < high && arr[mid] > arr[mid + 1])
            return mid;
        if (mid > low && arr[mid] < arr[mid - 1])
            return (mid - 1);
        if (arr[low] >= arr[mid])
            return findPivot(arr, low, mid - 1);
        return findPivot(arr, mid + 1, high);
    }

    private static int binarySearch(int[] arr, int low, int high, int key) {
        if (high < low) return -1;
        int mid = (low + high) / 2;
        if (key == arr[mid]) return mid;
        if (key > arr[mid])
            return binarySearch(arr, (mid + 1), high, key);
        return binarySearch(arr, low, (mid - 1), key);
    }
    // -----------------------------------------------------------------------

    // ---------------------------  VERSION 2 ------------------------------
    private static int search(int[] arr, int l, int h, int key) {
        if (l > h) return -1;
        int mid = (l + h) / 2;
        if (arr[mid] == key)
            return mid;
        if (arr[l] <= arr[mid]) {
            if (key >= arr[l] && key <= arr[mid])
                return search(arr, l, mid - 1, key);
            return search(arr, mid + 1, h, key);
        }

        if (key >= arr[mid] && key <= arr[h])
            return search(arr, mid + 1, h, key);
        return search(arr, l, mid - 1, key);
    }

    public static void main(String[] args) {
        // int[] arr = {1, 2, 3, 4, 5, 6, 7};
        int[] arr = {4, 5, 6, 7, 1, 2, 3};
        int n = arr.length;
        int key = 5;
        System.out.println("INDEX: " + search(arr, 0, n - 1, key));
        System.out.println("INDEX: " + pivotedBinarySearch(arr, n, key));
        System.out.println(Arrays.toString(arr));
    }
}

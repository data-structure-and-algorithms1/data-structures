package com.company.algorithm.array.rangeQueries;

//Number of indexes with equal elements in given range
public class RangeQueries20 {

    //Time Complexity : O(R – L) for every query
    private static int resultQuery(int[] arr, int l, int r) {
        int count = 0;
        for (int i = l; i < r; i++) {
            if (arr[i] == arr[i + 1]) {
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 2, 2, 3, 3, 4, 4, 4};
        int L = 1;
        int R = 8;

        System.out.println(resultQuery(arr,L,R));
    }

}

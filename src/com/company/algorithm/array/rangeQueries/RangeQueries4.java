package com.company.algorithm.array.rangeQueries;

//Range sum query using Sparse Table
public class RangeQueries4 {
    static int MAX = 100000;
    static int k = 16;

    static int[][] lookup = new int[MAX][k + 1];

    private static void buildSparseTable(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            lookup[i][0] = arr[i];
        }

        for (int i = 1; i <= k; i++) {
            for (int j = 0; j <= n - (1 << i); j++) {
                lookup[j][i] = lookup[j][i - 1] +
                        lookup[j + (1 << (i - 1))][i - 1];
            }
        }
    }

    private static long query(int l, int r) {
        long res = 0;
        for (int i = k; i >= 0; i--) {
            if (l + (1 << i) - 1 <= r) {
                res = res + lookup[l][i];
                l += 1 << i;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        int[] arr = {7, 2, 3, 0, 5, 10, 3, 12, 18};
        int n = arr.length;

        buildSparseTable(arr, n);
        System.out.println(query(0, 4));
        System.out.println(query(4, 7));
        System.out.println(query(7, 8));
    }
}

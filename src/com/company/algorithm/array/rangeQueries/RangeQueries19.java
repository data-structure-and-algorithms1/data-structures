package com.company.algorithm.array.rangeQueries;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

//Maximum Occurrence in a Given Range
public class RangeQueries19 {

    private static void findMax(int[]arr, int start, int end){
        Map<Integer,Integer> mp = new HashMap<>();

        for (int i = start; i <= end; i++)
            mp.put(arr[i], mp.get(arr[i]) == null ? 1 : mp.get(arr[i]) + 1);

        int max = Integer.MIN_VALUE;
        int key = 0;
        for (Map.Entry<Integer, Integer> entry : mp.entrySet()) {
           if(entry.getValue() > max) {
               max = entry.getValue();
               key = entry.getKey();
           }
        }
        System.out.println(key + " occurred the most number of times with a frequency of " +
                max + " within given range");
    }

    public static void main(String[] args) {
        int[] arr = {-5, -5, 2, 2, 2, 2, 3, 7, 7, 7};

        findMax(arr,0,9);
        findMax(arr,4,9);
    }
}

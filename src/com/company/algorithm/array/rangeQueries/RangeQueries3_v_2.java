package com.company.algorithm.array.rangeQueries;

//Sparse Table
public class RangeQueries3_v_2 {
    static int MAX = 10;

    static int[][] lookup = new int[MAX][MAX];

    private static void buildSparseTable(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            lookup[i][0] = arr[i];
        }

        for (int i = 1; (1 << i) <= n; i++) {

            for (int j = 0; (j + (1 << i) - 1) < n; j++) {
                //lookup[j][i] = Math.min(lookup[j][i - 1], lookup[j + (1 << (i - 1))][i - 1]);  qisa versiya
                if (lookup[j][i - 1] < lookup[j + (1 << (i - 1))][i - 1]) {
                    lookup[j][i] = lookup[j][i - 1];
                } else
                    lookup[j][i] = lookup[j + (1 << (i - 1))][i - 1];
            }
        }
    }

    private static int query(int l, int r) {
        int j = (int) Math.log(r - l + 1);

        if (lookup[l][j] <= lookup[r - (1 << j) + 1][j])
            return lookup[l][j];
        else
            return lookup[r - (1 << j) + 1][j];
    }

    public static void main(String[] args) {
        int[] arr = {7, 2, 3, 0, 5, 10, 3, 12, 18};
        int n = arr.length;

        buildSparseTable(arr, n);
        System.out.println(query(0, 4));
        System.out.println(query(4, 7));
        System.out.println(query(7, 8));
    }
}

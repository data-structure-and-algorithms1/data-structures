package com.company.algorithm.array.rangeQueries;


//Products of ranges in an array
public class RangeQueries33 {

    private static int calculateProduct(int[] arr, int L, int R, int P) {
        L = L - 1;

        int ans = 1;
        for (int i = L; i < R; i++) {
            ans = ans * arr[i];
            ans = ans % P;
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6};
        int P = 229;
        int L = 2, R = 5;

        System.out.println(calculateProduct(arr,L,R,P));
    }
}

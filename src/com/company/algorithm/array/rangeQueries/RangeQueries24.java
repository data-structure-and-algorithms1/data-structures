package com.company.algorithm.array.rangeQueries;

//Range Query on array whose each element is XOR of index value and previous element
public class RangeQueries24 {

    private static int fun(int x) {
        int y = (x / 4) * 4;
        int ans = 0;
        for (int i = y; i <= x; i++) {
            ans ^= i;
        }
        return ans;
    }

    private static int query(int x) {
        if (x == 0)
            return 0;

        int k = (x + 1) / 2;
        return (x % 2 != 0) ? 2 * fun(k) :
                ((fun(k - 1) * 2) ^ (k & 1));
    }

    private static void allQueries(int q, int[] l, int[] r) {
        for (int i = 0; i < q; i++) {
            System.out.println((query(r[i]) ^ query(l[i] - 1)));
        }
    }

    public static void main(String[] args) {
        int q = 3;
        int[] l = {2, 2, 5};
        int[] r = {4, 8, 9};
        allQueries(q, l, r);
    }
}

package com.company.algorithm.array.rangeQueries;

//Merge Sort Tree for Range Order Statistics
public class RangeQueries21 {

    private static void findKthSmallest(int[] arr, int start, int end, int k) {
        int min = Integer.MAX_VALUE;
        for (int i = start - 1; i < end; i++) {
            if (arr[i] < min && k >= arr[i]) {
                min = arr[i];
            }
        }
        System.out.println(min);
    }

    public static void main(String[] args) {
        int[] arr = {3, 2, 5, 1, 8, 9, 4, 10, 15, 13};
        int start = 2;
        int end = 5;
        int k = 2;

        findKthSmallest(arr, start, end, k);

        start = 5;
        end = arr.length;
        k = 12;
        findKthSmallest(arr, start, end, k);
    }
}

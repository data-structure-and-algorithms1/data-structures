package com.company.algorithm.array.rangeQueries;

//Difference Array | Range update query in O(1)
public class RangeQueries23_v_2 {

    private static void initializeDiffArray(int[] arr, int[] diff) {
        int n = arr.length;
        diff[0] = arr[0];
        diff[n] = 0;
        for (int i = 1; i < n; i++) {
            diff[i] = arr[i] - arr[i - 1];
        }
    }

    private static void update(int[] D, int l, int r, int x) {
        D[l] += x;
        D[r + 1] -= x;
    }

    private static int printArray(int[] arr, int[] diff) {
        for (int i = 0; i < arr.length; i++) {
            if (i == 0)
                arr[i] = diff[i];

            else
                arr[i] = diff[i] + arr[i - 1];

            System.out.print(arr[i] + " ");
        }
        System.out.println();
        return 0;
    }

    public static void main(String[] args) {
        int[] arr = {10, 5, 20, 40};
        int n = arr.length;

        int [] diff = new int[n+1];
        initializeDiffArray(arr,diff);

        update(diff,0,1,10);
        printArray(arr,diff);

        update(diff,1,3,20);
        update(diff,2,2,30);
        printArray(arr,diff);
    }
}

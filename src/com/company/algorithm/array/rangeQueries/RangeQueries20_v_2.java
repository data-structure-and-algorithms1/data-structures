package com.company.algorithm.array.rangeQueries;

//Number of indexes with equal elements in given range
public class RangeQueries20_v_2 {
    private static int prefixArr[] = new int[1000];

    // O(1) for every query and O(n) for pre-computing.
    private static void countIndex(int[] a, int n) {
        for (int i = 0; i < n; i++) {
            if (i + 1 < n && a[i] == a[i + 1])
                prefixArr[i] = 1;

            if (i != 0) {
                prefixArr[i] += prefixArr[i - 1];
            }
        }
    }

    private static int resultQuery(int l, int r) {
        if (l == 0)
            return prefixArr[r - 1];
        else
            return prefixArr[r - 1] - prefixArr[l - 1];
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 2, 2, 3, 3, 4, 4, 4};

        countIndex(arr, arr.length);

        int L = 1;
        int R = 8;
        System.out.println(resultQuery(L, R));

        L = 0;
        R = 4;
        System.out.println(resultQuery(L, R));
    }
}

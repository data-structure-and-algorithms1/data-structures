package com.company.algorithm.array.rangeQueries;

//Range sum queries without updates
public class RangeQueries26 {

    private static void sumQueries(int[] arr, int start, int end) {
        int sum = 0;
        for (int i = start; i <= end; i++) {
            sum += arr[i];
        }
        System.out.println(sum);
    }
    //=====================================================

    //================== VERSION 2=======================
    private static void preCompute(int[] arr, int n, int[] pre) {
        pre[0] = arr[0];
        for (int i = 1; i < n; i++) {
            pre[i] = arr[i] + pre[i - 1];
        }
    }

    private static int rangeSum(int i, int j, int[] pre) {
        if (i == 0)
            return pre[j];
        return pre[j] - pre[i - 1];
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int n = arr.length;

        int [] pre = new int[n];
        preCompute(arr,n,pre);
        System.out.println(rangeSum(1,3,pre));
        System.out.println(rangeSum(2,4,pre));

//        sumQueries(arr, 1, 3);
//        sumQueries(arr, 2, 4);
    }
}

package com.company.algorithm.array.rangeQueries;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

//Array range queries for elements with frequency same as value
public class RangeQueries18 {

    private static int solveQuery(int start, int end, int arr[]) {
        Map<Integer, Integer> mp = new HashMap<>();

        for (int i = start; i <= end; i++) {
            mp.put(arr[i], mp.get(arr[i]) == null ? 1 : mp.get(arr[i]) + 1);
        }
        int count = 0;
        for (Map.Entry<Integer, Integer> entry : mp.entrySet()) {
            if (Objects.equals(entry.getKey(), entry.getValue()))
                count++;
        }
        return count;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 2, 3, 3, 3};
        int n = arr.length;

        int[][] queries = {
                {0, 1},
                {1, 1},
                {0, 2},
                {1, 3},
                {3, 5},
                {0, 5}
        };
        int q = queries.length;

        for (int i = 0; i < q; i++) {
            int start = queries[i][0];
            int end = queries[i][1];
            System.out.println("Query " + (i + 1) + " = " + solveQuery(start, end, arr));
        }
    }
}

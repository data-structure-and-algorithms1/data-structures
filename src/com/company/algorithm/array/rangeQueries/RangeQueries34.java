package com.company.algorithm.array.rangeQueries;


import java.util.Arrays;

//Count Primes in Ranges
public class RangeQueries34 {

    private static final int MAX = 10000;
    private static final int[] prefix = new int[MAX + 1];

    private static void buildPrefix() {
        boolean[] prime = new boolean[MAX+1];
        Arrays.fill(prime, true);

        for (int p = 2; p * p <= MAX; p++) {
            if (prime[p]) {
                for (int i = p * 2; i <= MAX; i += p) {
                    prime[i] = false;
                }
            }
        }

        prefix[0] = prefix[1] = 0;
        for (int p = 2; p <= MAX; p++) {
            prefix[p] = prefix[p - 1];
            if (prime[p])
                prefix[p]++;
        }
    }

    private static int query(int L, int R) {
        return prefix[R] - prefix[L - 1];
    }

    public static void main(String[] args) {
        buildPrefix();
        int L = 5, R = 10;
        System.out.println(query(L, R));

        L = 1;
        R = 10;
        System.out.println(query(L, R));
    }
}

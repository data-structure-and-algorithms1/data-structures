package com.company.algorithm.array.rangeQueries;

//Array range queries for searching an element
public class RangeQueries17 {

    private static boolean elementIsExist(int[] arr, int start, int end, int element) {
        for (int i = start - 1; i < end; i++)
            if (arr[i] == element)
                return true;
        return false;
    }

    public static void main(String[] args) {
        int[] arr = {1, 1, 5, 4, 5};
        int n = arr.length;

        System.out.println(elementIsExist(arr, 1, 3, 2) ? "YES" : "NO");
        System.out.println(elementIsExist(arr, 2, 5, 1) ? "YES" : "NO");
        System.out.println(elementIsExist(arr, 3, 5, 5) ? "YES" : "NO");
    }
}

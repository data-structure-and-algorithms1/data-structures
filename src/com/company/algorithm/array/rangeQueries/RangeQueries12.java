package com.company.algorithm.array.rangeQueries;

//Queries for counts of array elements with values in given range
public class RangeQueries12 {

    private static int countOfRange(int[] arr, int n, int x, int y) {
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (arr[i] >= x && arr[i] <= y)
                count++;
        }
        return count;
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 3, 9, 10, 4};

        int i1 = 1, j1 = 4;
        System.out.println(countOfRange(arr,arr.length,i1,j1));

        int i2 = 9, j2 = 12;
        System.out.println(countOfRange(arr,arr.length,i2,j2));
    }
}

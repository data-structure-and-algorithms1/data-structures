package com.company.algorithm.array.rangeQueries;

//Mean of range in array
public class RangeQueries30 {

    private static void findMeanOfRange(int[]arr, int start, int end){
        int sum = 0;
        int count = 0;
        for (int i = start; i <= end; i++) {
            sum += arr[i];
            count++;
        }
        System.out.println(Math.floor(sum/count));
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        findMeanOfRange(arr,0,2);
        findMeanOfRange(arr,1,3);
        findMeanOfRange(arr,0,4);
    }
}

package com.company.algorithm.array.rangeQueries;

//GCDs of given index ranges in an array
public class RangeQueries9 {

    private static int[] segmentTree; // segment tree array

    private static int[] constructSegmentTree(int[] arr) {
        int height = (int) Math.ceil(Math.log(arr.length) / Math.log(2));
        int size = 2 * (int) Math.pow(2, height) - 1;
        segmentTree = new int[size];
        constructST(arr, 0, arr.length - 1, 0);
        return segmentTree;
    }

    private static int constructST(int[] arr,
                                   int segmentStartIndex,
                                   int segmentEndIndex,
                                   int segmentCurrentIndex) {

        if (segmentStartIndex == segmentEndIndex) {
            segmentTree[segmentCurrentIndex] = arr[segmentStartIndex];
            return segmentTree[segmentCurrentIndex];
        }

        int mid = segmentStartIndex + (segmentEndIndex - segmentStartIndex) / 2;
        segmentTree[segmentCurrentIndex] =
                gcd(
                        constructST(arr,
                                segmentStartIndex,
                                mid,
                                segmentCurrentIndex * 2 + 1),
                        constructST(arr,
                                mid + 1,
                                segmentEndIndex,
                                segmentCurrentIndex * 2 + 2)
                );

        return segmentTree[segmentCurrentIndex];
    }

    private static int gcd(int a, int b) {
        return a == 0 ? b : gcd(b % a, a);
    }

    private static int findRangeGcd(int segmentStartIndex,
                                    int segmentEndIndex,
                                    int[] arr) {
        int n = arr.length;
        if (segmentStartIndex < 0 || segmentEndIndex > n - 1 || segmentStartIndex > segmentEndIndex)
            throw new IllegalArgumentException("Invalid Argument");

        return findGcd(0,
                n - 1,
                segmentStartIndex,
                segmentEndIndex,
                0);
    }

    private static int findGcd(int segmentStartIndex,
                               int segmentEndIndex,
                               int queryStartRange,
                               int queryEndRange,
                               int segmentCurrentIndex) {
        if (segmentStartIndex > queryEndRange || segmentEndIndex < queryStartRange)
            return 0;
        if (queryStartRange <= segmentStartIndex && queryEndRange >= segmentEndIndex)
            return segmentTree[segmentCurrentIndex];

        int mid = segmentStartIndex + (segmentEndIndex - segmentStartIndex) / 2;

        return gcd(
                findGcd(segmentStartIndex,
                        mid,
                        queryStartRange,
                        queryEndRange,
                        segmentCurrentIndex * 2 + 1),
                findGcd(mid + 1,
                        segmentEndIndex,
                        queryStartRange,
                        queryEndRange,
                        segmentCurrentIndex * 2 + 2));
    }

    // O(n*log(min(a,b));
    public static void main(String[] args) {
        int[] arr = {2, 3, 6, 9, 5};
        constructSegmentTree(arr);
        int l = 1;
        int r = 3;
        System.out.println(findRangeGcd(l, r, arr));
    }
}

package com.company.algorithm.array.rangeQueries;

//Sparse Table
public class RangeQueries3 {

    // ====================== VERSION 1 ===========================
    private static int minimumOfRange(int[] arr, int n, int start, int end) {
        int min = Integer.MAX_VALUE;
        for (int i = start; i <= end; i++) {
           if(arr[i] < min){
               min = arr[i];
           }
        }
        return min;
    }

    private static void getminimumByRanges(int[] arr, int n, int[][] queries, int qSize) {
        int start;
        int end;
        for (int i = 0; i < qSize; i++) {
            start = queries[i][0];
            end = queries[i][1];
            int min = minimumOfRange(arr, n, start, end);
            System.out.println("SUM OF RANGE " + start + " - " + end + " is " + min);
        }
    }
    // =======================================================================

    public static void main(String[] args) {
        int[] arr = {7, 2, 3, 0, 5, 10, 3, 12, 18};
        int[][] queries = {{0, 4}, {4, 7}, {7, 8}};
        int n = arr.length;
        int qSize = queries.length;
        getminimumByRanges(arr, n, queries, qSize);
    }
}

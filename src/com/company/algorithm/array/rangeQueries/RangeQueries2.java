package com.company.algorithm.array.rangeQueries;

//Sqrt (or Square Root) Decomposition Technique | Set 1 (Introduction)
public class RangeQueries2 {

    static int MAXN = 10;
    static int SQRSIZE = 10;
    static int[] arr = new int[MAXN]; // original array
    static int[] block = new int[SQRSIZE]; // decomposed array
    static int blockSize;  // block size;

    private static void update(int idx, int val) {
        int blockNumber = idx / blockSize;
        block[blockNumber] += val - arr[idx];
        arr[idx] = val;
    }

    private static int query(int l, int r) {
        int sum = 0;
        while (l < r && l % blockSize != 0 && l != 0) {
            sum += arr[l];
            l++;
        }
        while (l + blockSize <= r) {
            sum += block[l / blockSize];
            l += blockSize;
        }
        while (l <= r) {
            sum += arr[l];
            l++;
        }
        return sum;
    }

    private static void preprocess(int[] input, int n) {
        int blockIdx = -1;
        blockSize = (int) Math.sqrt(n);

        for (int i = 0; i < n; i++) {
            arr[i] = input[i];
            if (i % blockSize == 0) {
                blockIdx++;
            }
            block[blockIdx] += arr[i];
        }
    }

    public static void main(String[] args) {
        int[] input = {1, 5, 2, 4, 6, 1, 3, 5, 7, 10};
        int n = input.length;

        preprocess(input, n);
        System.out.println("query(3,8) " + query(3, 8));
        System.out.println("query(1,6) " + query(1, 6));

        update(8, 0);
        System.out.println("query(8,8) " + query(8, 8));
    }
}

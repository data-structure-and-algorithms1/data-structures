package com.company.algorithm.array.rangeQueries;

//Count elements which divide all numbers in range L-R
public class RangeQueries15 {

    private static void findAllDivides(int[] arr, int n, int l, int r) {
        int count = 0;
        l = l - 1;

        for (int i = l; i < r; i++) {
            int divisors = 0;

            for (int j = l; j < r; j++) {
                if (arr[j] % arr[i] == 0) {
                    divisors++;
                } else break;
            }
            if (divisors == (r - l))
                count++;
        }
        System.out.println(count);
    }

    public static void main(String[] args) {
        int[] arr = {3, 4, 2, 2, 4, 6};
        findAllDivides(arr, arr.length, 1, 4);
        findAllDivides(arr, arr.length, 2, 4);
    }
}

package com.company.algorithm.array.rangeQueries;

//Array range queries for searching an element
public class RangeQueries17_v_2 {

    private static class Query {
        int start;
        int end;
        int value;

        public Query(int start, int end, int value) {
            this.start = start;
            this.end = end;
            this.value = value;
        }
    }

    private static final int maxn = 100;
    private static final int[] root = new int[maxn];

    private static int find(int indexElement) {
        if (indexElement == root[indexElement])
            return indexElement;
        else
            return root[indexElement] = find(root[indexElement]);
    }

    public static void main(String[] args) {
        int[] arr = {1, 1, 5, 4, 5};
        Query[] q = {
                new Query(0, 2, 2),
                new Query(1, 4, 1),
                new Query(2, 4, 5)
        };

        for (Query query : q) {
            int flag = 0;
            int l = query.start;
            int r = query.end;
            int val = query.value;
            int p = r;
            while (p >= l) {

                if (arr[p] == val) {
                    flag = 1;
                    break;
                }
                p = find(p) - 1;
            }
            if (flag != 0)
                System.out.println(val + " exists between [" + l + ", " + r + "]");
            else
                System.out.println(val + " does not exist between [" + l + ", " + r + "]");
        }
    }
}

package com.company.algorithm.array.rangeQueries;

import java.util.Arrays;

//Difference Array | Range update query in O(1)
public class RangeQueries23 {

    private static void updateArr(int[] arr, int start, int end, int value) {
        for (int i = start; i <=end; i++) {
             arr[i] += value;
        }
    }

    public static void main(String[] args) {
        int[] arr = {10, 5, 20, 40};
        System.out.println(Arrays.toString(arr));

        updateArr(arr,0,1,10);
        System.out.println(Arrays.toString(arr));

        updateArr(arr,1,3,20);
        System.out.println(Arrays.toString(arr));

        updateArr(arr,2,2,30);
        System.out.println(Arrays.toString(arr));
    }
}

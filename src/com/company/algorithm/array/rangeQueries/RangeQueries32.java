package com.company.algorithm.array.rangeQueries;


//Queries on probability of even or odd number in given ranges
public class RangeQueries32 {

    private static int gcd(int a, int b) {
        if (a == 0 || b == 0)
            return 0;

        if (a == b)
            return a;

        if (a > b)
            return gcd(a - b, b);
        return gcd(a, b - a);
    }

    private static void solveQuery(int[] arr, int n, int Q, int[][] query) {
        int[] even = new int[n + 1];
        int[] odd = new int[n + 1];
        even[0] = odd[0] = 0;

        for (int i = 0; i < n; i++) {
            if ((arr[i] & 1) > 0) {
                odd[i + 1] = odd[i] + 1;
                even[i + 1] = even[i];
            } else {
                even[i + 1] = even[i] + 1;
                odd[i + 1] = odd[i];
            }
        }

        for (int i = 0; i < Q; i++) {
            int r = query[i][2];
            int l = query[i][1];
            int k = query[i][0];

            int q = r - l + 1;
            int p;

            if (k > 0)
                p = odd[r] - odd[l - 1];
            else
                p = even[r] - even[l - 1];

            if (p <= 0)
                System.out.println("0");
            else if (p == q)
                System.out.println("1");
            else {
                int g = gcd(p, q);
                System.out.println(p / g + " " + q / g);
            }
        }
    }

    public static void main(String[] args) {
        int []arr = { 6, 5, 2, 1, 7 };
        int n = arr.length;
        int Q = 2;
        int [][]query = { { 0, 2, 2 },
                { 1, 2, 5 } };

        solveQuery(arr, n, Q, query);
    }
}

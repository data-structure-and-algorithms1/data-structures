package com.company.algorithm.array.rangeQueries;

import java.util.Scanner;

//Array Queries for multiply, replacements and product
public class RangeQueries29 {

    private static Scanner sc = new Scanner(System.in);
    private static int[] twos = new int[1000];
    private static int[] fives = new int[1000];
    private static int sum = 0;

    private static int returnTwos(int val) {
        int count = 0;
        while (val % 2 == 0 && val != 0) {
            val = val / 2;
            count++;
        }
        return count;
    }

    private static int returnFives(int val) {
        int count = 0;
        while (val % 5 == 0 && val != 0) {
            val = val / 5;
            count++;
        }
        return count;
    }

    private static void solveQueries(int[] arr, int n) {
        int type = sc.nextInt();

        if (type == 1) {
            int ql = sc.nextInt();
            int qr = sc.nextInt();
            int x = sc.nextInt();

            int temp = returnTwos(x);
            int temp1 = returnFives(x);

            for (int i = ql - 1; i < qr; i++) {
                arr[i] = arr[i] * x;
                twos[i] += temp;
                fives[i] += temp1;
            }
        }

        if (type == 2) {
            int ql = sc.nextInt();
            int qr = sc.nextInt();
            int y = sc.nextInt();

            int temp = returnTwos(y);
            int temp1 = returnFives(y);

            for (int i = ql - 1; i < qr; i++) {
                arr[i] = (i - ql + 2) * y;

                twos[i] = returnTwos(i - ql + 2) + temp;
                fives[i] = returnFives(i - ql + 2) + temp1;
            }
        }

        if (type == 3) {
            int ql = sc.nextInt();
            int qr = sc.nextInt();
            int sumtwos = 0;
            int sumfives = 0;

            for (int i = ql - 1; i < qr; i++) {
                sumtwos += twos[i];
                sumfives += fives[i];
            }

            sum += Math.min(sumtwos, sumfives);
        }
    }

    public static void main(String[] args) {
        int n = sc.nextInt();
        int m = sc.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
            twos[i] = returnTwos(arr[i]);
            fives[i] = returnFives(arr[i]);
        }

        while (m-- != 0) {
            solveQueries(arr, n);
        }
        System.out.println(sum);
    }
}

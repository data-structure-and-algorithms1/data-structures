package com.company.algorithm.array.rangeQueries;

//Queries for decimal values of subarrays of a binary array
public class RangeQueries13 {

    // ====================== VERSION 1 ===========================
    private static void findDecimal(int[] bits, int left, int right, int n) {
        int pow = 0;
        int sum = 0;
        for (int i = left; i <= right; i++) {
            if (bits[i] != 0) {
                sum += Math.pow(2, pow);
            }
            pow++;
        }
        System.out.println(sum);
    }

//    public static void main(String[] args) {
//        int[] bits = {1, 0, 1, 0, 1, 1};
//        findDecimal(bits, 2, 4, bits.length);
//        findDecimal(bits, 4, 5, bits.length);
//    }

    //============================ VERSION 2 =============================
    private static void precompute(int[] arr, int n, int[] pre) {
        pre[n - 1] = (int) (arr[n - 1] * Math.pow(2, 0));
        for (int i = n - 2; i >= 0; i--) {
            pre[i] = pre[i + 1] + arr[i] * (1 << (n - 1 - i));
        }
    }

    private static int decimalOfSubarr(int l, int r, int n, int[] pre) {
        if (r != n - 1)
            return (pre[l] - pre[r + 1]) / (1 << (n - 1 - r));
        return pre[l] / (1 << (n - 1 - r));
    }

    public static void main(String[] args) {
        int[] bits = {1, 0, 1, 0, 1, 1};
        int n = bits.length;

        int[] pre = new int[n];
        precompute(bits, n, pre);

        System.out.println(decimalOfSubarr(2, 4, n, pre));
        System.out.println(decimalOfSubarr(4, 5, n, pre));
    }
}

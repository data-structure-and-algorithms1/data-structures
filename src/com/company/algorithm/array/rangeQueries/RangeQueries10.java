package com.company.algorithm.array.rangeQueries;

//Queries for GCD of all numbers of an array except elements in a given range
public class RangeQueries10 {

    private static int gcd(int a, int b) {
        return a == 0 ? b : gcd(b % a, a);
    }

    private static void fillPrefixSuffix(int[] prefix, int[] arr, int[] suffix, int n) {
        prefix[0] = arr[0];
        for (int i = 1; i < n; i++) {
            prefix[i] = gcd(prefix[i - 1], arr[i]);
        }

        suffix[n - 1] = arr[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            suffix[i] = gcd(suffix[i + 1], arr[i]);
        }
    }

    private static int gcdOutsideRange(int left, int right, int[] prefix, int[] suffix, int n) {
        if (left == 0)
            return suffix[right + 1];

        if (right == n - 1)
            return prefix[left - 1];
        return gcd(prefix[left - 1], suffix[right + 1]);
    }

    public static void main(String[] args) {
        int[] arr = {2, 6, 9};
        int n = arr.length;
        int[] prefix = new int[n];
        int[] suffix = new int[n];
        fillPrefixSuffix(prefix, arr, suffix, n);

        int l = 0, r = 0;
        System.out.println(gcdOutsideRange(l, r, prefix, suffix, n));

        l = 1;
        r = 1;
        System.out.println(gcdOutsideRange(l, r, prefix, suffix, n));

        l = 1;
        r = 2;
        System.out.println(gcdOutsideRange(l, r, prefix, suffix, n));
    }
}

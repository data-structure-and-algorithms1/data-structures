package com.company.algorithm.array.rangeQueries;

//Check in binary array the number represented by a subarray is odd or even
public class RangeQueries27 {

    private static void findOddOrEven(int[] arr, int start, int end) {
        String n = "";
        for (int i = start; i <= end; i++) {
            n = n + arr[i];
        }
        if (Integer.parseInt(n) % 2 == 0) {
            System.out.println("Even");
        } else {
            System.out.println("ODD");
        }
    }

    //============================= VERSION 2 ===========================
    private static void findOddOrEvenV2(int[] arr, int start, int end) {
        if (arr[end] == 1) {
            System.out.println("ODD");
        } else {
            System.out.println("Even");
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 1, 0, 1};
        findOddOrEven(arr, 1, 3);
        findOddOrEvenV2(arr, 1, 3);
    }
}

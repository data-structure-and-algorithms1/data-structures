package com.company.algorithm.array.rangeQueries;

//Range LCM Queries
public class RangeQueries8 {
    static final int MAX = 1000;
    static int[] tree = new int[4 * MAX];
    static int[] arr = new int[MAX];

    private static int gcd(int a, int b) {
        int result = a == 0 ? b : gcd(b % a, a);
        return result;
    }

    private static int lcm(int a, int b) {
        int result = a * b / gcd(a, b);
        return result;
    }

    private static void build(int node, int start, int end) {
        if (start == end) {
            tree[node] = arr[start];
            return;
        }
        int mid = (start + end) / 2;
        build(2 * node, start, mid);
        build(2 * node + 1, mid + 1, end);

        int leftLcm = tree[2 * node];
        int rightLcm = tree[2 * node + 1];

        tree[node] = lcm(leftLcm, rightLcm);
    }

    private static int query(int node, int start, int end, int l, int r) {
        if (end < l || start > r) {
            return 1;
        }

        if (l <= start && r >= end) {
            return tree[node];
        }

        int mid = (start + end) / 2;
        int leftLcm = query(2 * node, start, mid, l, r);
        int rightLcm = query(2 * node + 1, mid + 1, end, l, r);
        return lcm(leftLcm, rightLcm);
    }

    public static void main(String[] args) {
        arr[0] = 5;
        arr[1] = 7;
        arr[2] = 5;
        arr[3] = 2;
        arr[4] = 10;
        arr[5] = 12;
        arr[6] = 11;
        arr[7] = 17;
        arr[8] = 14;
        arr[9] = 1;
        arr[10] = 44;

        build(1, 0, 10);
        System.out.println(query(1, 0, 10, 2, 5));

        System.out.println(query(1, 0, 10, 5, 10));

        System.out.println(query(1, 0, 10, 0, 10));
    }
}

package com.company.algorithm.array.rangeQueries;

import java.util.Arrays;

//Print modified array after executing the commands of addition and subtraction
public class RangeQueries31 {

    private static void printModifiedArrAdd(int[] arr, int start, int end, int element) {
        for (int i = start - 1; i < end; i++) {
            arr[i] += element;
        }
    }

    private static void printModifiedArrSubtract(int[] arr, int start, int end, int element) {
        for (int i = start - 1; i < end; i++) {
            arr[i] -= element;
        }
    }

    private static void selectOperation(int[] arr, int operation, int start, int end, int element) {
        if (operation == 0) {
            printModifiedArrAdd(arr, start, end, element);
        } else {
            printModifiedArrSubtract(arr, start, end, element);
        }
    }

    public static void main(String[] args) {
        int n = 5;
        int[] arr = new int[n];

        selectOperation(arr,0,1,3,2);
        selectOperation(arr,1,3,5,3);
        selectOperation(arr,0,2,5,1);

        System.out.println(Arrays.toString(arr));

    }

}

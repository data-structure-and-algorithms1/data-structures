package com.company.algorithm.array.rangeQueries;

//Number whose sum of XOR with given array range is maximum
public class RangeQueries16 {

    private static final int MAX = Integer.MAX_VALUE;
    private static int[][] one = new int[100001][32];

    private static void makePrefix(int[] arr, int n) {
        for (int i = 1; i <= n; i++) {
            int a = arr[i - 1];
            for (int j = 0; j < 32; j++) {
                int x = (int) Math.pow(2, j);

                if ((a & x) != 0)
                    one[i][j] = 1 + one[i - 1][j];
                else
                    one[i][j] = one[i - 1][j];
            }
        }
    }

    private static int solve(int l, int r) {
        int totalBits = r - l + 1;
        int X = MAX;
        for (int i = 0; i < 31; i++) {
            int x = one[r][i] - one[l - 1][i];

            if (x >= totalBits - x) {
                int ithBit = (int) Math.pow(2, i);

                X = X ^ ithBit;
            }
        }
        return X;
    }

    public static void main(String[] args) {
        int n = 5, q = 3;
        int A[] = {210, 11, 48, 22, 133};
        int L[] = {1, 4, 2}, R[] = {3, 14, 4};

        makePrefix(A, n);

        for (int i = 0; i < q; i++) {
            System.out.println(solve(L[i], R[i]));
        }

    }
}

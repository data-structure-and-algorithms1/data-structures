package com.company.algorithm.array.rangeQueries;

//MO’s Algorithm (Query Square Root Decomposition) | Set 1 (Introduction)
public class RangeQueries1 {

    // ====================== VERSION 1 ===========================
    private static int sumOfRange(int[] arr, int n, int start, int end) {
        int sum = 0;
        for (int i = start; i <= end; i++) {
            sum += arr[i];
        }
        return sum;
    }

    private static void getSumByRanges(int[] arr, int n, int[][] queries, int qSize) {
        int start;
        int end;
        for (int i = 0; i < qSize; i++) {
            start = queries[i][0];
            end = queries[i][1];
            int sum = sumOfRange(arr, n, start, end);
            System.out.println("SUM OF RANGE " + start + " - " + end + " is " + sum);
        }
    }
    // =======================================================================

    public static void main(String[] args) {
        int[] arr = {1, 1, 2, 1, 3, 4, 5, 2, 8};
        int[][] queries = {{0, 4}, {1, 3}, {2, 4}};
        int n = arr.length;
        int qSize = queries.length;
        getSumByRanges(arr, n, queries, qSize);
    }
}

package com.company.algorithm.array.rangeQueries;

//Find whether a subarray is in form of a mountain or not
public class RangeQueries25 {

    private static void preprocess(int[] arr, int N, int[] left, int[] right) {
        left[0] = 0;
        int lastIncr = 0;

        for (int i = 1; i < N; i++) {
            if (arr[i] > arr[i - 1])
                lastIncr = i;

            left[i] = lastIncr;
        }

        right[N - 1] = N - 1;
        int firstDecr = N - 1;
        for (int i = N - 2; i >= 0; i--) {
            if (arr[i] > arr[i + 1])
                firstDecr = i;
            right[i] = firstDecr;
        }
    }

    private static boolean isSubarrMountainForm(int[] arr, int[] left, int[] right, int L, int R) {
        return right[L] >= left[R];
    }

    public static void main(String[] args) {
        int[] arr = {2, 3, 2, 4, 4, 6, 3, 2};
        int N = arr.length;
        int[] left = new int[N];
        int[] right = new int[N];
        preprocess(arr,N,left,right);

        int L = 0;
        int R = 2;

        if(isSubarrMountainForm(arr,left,right,L,R)){
            System.out.println("YES");
        }
        else {
            System.out.println("NO");
        }
    }
}

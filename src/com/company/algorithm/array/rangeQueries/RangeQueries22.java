package com.company.algorithm.array.rangeQueries;

import java.util.LinkedHashSet;

//Total numbers with no repeated digits in a range
public class RangeQueries22 {

    private static int repeated_digit(int n) {
        LinkedHashSet<Integer> s = new LinkedHashSet<>();
        while (n != 0) {
            int d = n % 10;

            if (s.contains(d)) {
                return 0;
            }
            s.add(d);
            n = n / 10;
        }
        return 1;
    }

    private static int calculate(int L, int R) {
        int answer = 0;

        for (int i = L; i <= R; i++) {
            answer = answer + repeated_digit(i);
        }
        return answer;
    }

    public static void main(String[] args) {
        int L = 10;
        int R = 12;

        System.out.println(calculate(L,R));
    }
}

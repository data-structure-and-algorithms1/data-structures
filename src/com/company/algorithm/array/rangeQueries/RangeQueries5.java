package com.company.algorithm.array.rangeQueries;

//Range Minimum Query (Square Root Decomposition and Sparse Table)
public class RangeQueries5 {
    static int MAX = 500;

    static int[][] lookup = new int[MAX][MAX];

    static class Query {
        int L;
        int R;

        public Query(int l, int r) {
            L = l;
            R = r;
        }
    }

    private static void preprocess(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            lookup[i][i] = i;
        }

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[lookup[i][j - 1]] < arr[j])
                    lookup[i][j] = lookup[i][j - 1];
                else
                    lookup[i][j] = j;
            }
        }
    }

    private static void query(int[] arr, int n, Query q[], int m) {
        preprocess(arr, n);

        for (int i = 0; i < m; i++) {
            int L = q[i].L, R = q[i].R;

            System.out.println("[" + L + ", " + R + "] is " + arr[lookup[L][R]]);
        }
    }

    public static void main(String[] args) {
        int[] arr = {7, 2, 3, 0, 5, 10, 3, 12, 18};
        int n = arr.length;
        Query q[] = {new Query(0, 4),
                new Query(4, 7),
                new Query(7, 8)};
        int m = q.length;
        query(arr, n, q, m);
    }
}

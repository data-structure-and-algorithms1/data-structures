package com.company.algorithm.array.rangeQueries;

//Number of elements less than or equal to a given number in a given subarray | Set 2 (Including Updates)
public class RangeQueries11 {

    static final int MAX = 10001;

    private static void update(int idx, int blk, int val, int[][] bit) {
        for (; idx < MAX; idx += (idx & -idx)) {
            bit[blk][idx] += val;
        }
    }

    private static int query(int l, int r, int k, int[] arr, int blk_size, int[][] bit) {
        int sum = 0;
        while (l < r && l % blk_size != 0 && l != 0) {
            if (arr[l] <= k)
                sum++;
            l++;
        }
        while (l + blk_size <= r) {
            int idx = k;
            for (; idx > 0; idx -= idx & -idx) {
                sum += bit[l / blk_size][idx];
            }
            l += blk_size;
        }

        while (l <= r) {
            if (arr[l] <= k)
                sum++;
            l++;
        }
        return sum;
    }

    private static void preprocess(int[] arr, int blk_size, int n, int[][] bit) {
        for (int i = 0; i < n; i++) {
            update(arr[i], i / blk_size, 1, bit);
        }
    }

    private static void preprocessUpdate(int i, int v, int blk_size, int[] arr, int[][] bit) {
        update(arr[i], i / blk_size, -1, bit);
        update(v, i / blk_size, 1, bit);
        arr[i] = v;
    }

    public static void main(String[] args) {
        int[] arr = {5, 1, 2, 3, 4};
        int blk_size = (int) Math.sqrt(arr.length);

        int[][] bit = new int[blk_size + 1][MAX];
        preprocess(arr, blk_size, arr.length, bit);
        System.out.println(query(1, 3, 1, arr, blk_size, bit));

        preprocessUpdate(3, 10, blk_size, arr, bit);
        System.out.println(query(3, 3, 4, arr, blk_size, bit));

        preprocessUpdate(2, 1, blk_size, arr, bit);
        preprocessUpdate(0, 2, blk_size, arr, bit);
        System.out.println(query(0, 4, 5, arr, blk_size, bit));
    }
}

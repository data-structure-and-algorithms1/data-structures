package com.company.algorithm.array.rangeQueries;

import java.util.Arrays;

//Constant time range add operation on an array
public class RangeQueries7 {

    private static void updateArray(int[] arr, int start, int end, int element) {
        for (int i = start; i <=end; i++) {
            arr[i] += element;
        }
    }

    public static void main(String[] args) {
        int n = 6;
        int[] arr = new int[n];
        System.out.println(Arrays.toString(arr));

        updateArray(arr, 0, 2, 100);
        System.out.println(Arrays.toString(arr));

        updateArray(arr, 1, 5, 100);
        System.out.println(Arrays.toString(arr));

        updateArray(arr, 3, 5, 100);
        System.out.println(Arrays.toString(arr));

        updateArray(arr, 2, 4, 100);
        System.out.println(Arrays.toString(arr));
    }
}

package com.company.algorithm.array.rangeQueries;

import java.util.Arrays;

//Print modified array after executing the commands of addition and subtraction
public class RangeQueries31_v_2 {

    private static void updateQuery(int[] arr, int q, int l, int r, int k) {
        if (q == 0) {
            arr[l - 1] += k;
            arr[r] -= k;
        } else {
            arr[l - 1] -= k;
            arr[r] += k;
        }
    }

    static void generateArray(int arr[], int n) {
        for (int i = 1; i < n; ++i)
            arr[i] += arr[i - 1];

    }

    public static void main(String[] args) {
        int n = 5;
        int[] arr = new int[n + 1];

        updateQuery(arr, 0, 1, 3, 2);
        updateQuery(arr, 1, 3, 5, 3);
        updateQuery(arr, 0, 2, 5, 1);

        generateArray(arr, n);
        System.out.println(Arrays.toString(arr));

    }
}

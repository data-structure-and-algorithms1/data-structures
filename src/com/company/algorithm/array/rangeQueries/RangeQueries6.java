package com.company.algorithm.array.rangeQueries;

//Range Queries for Frequencies of array elements
public class RangeQueries6 {

    private static void findElementByFrequency(int[] arr, int left, int right, int element, int n) {
        int freq = 0;
        for (int i = left - 1; i < right - 1; i++) {
             if(arr[i] == element){
                 freq++;
             }
        }
        System.out.println("Element " + element + " appears " + freq + " times");
    }

    public static void main(String[] args) {
        int[] arr = {2, 8, 6, 9, 8, 6, 8, 2, 11};
        int n = arr.length;
        int left = 2;
        int right = 8;
        int element = 8;
        findElementByFrequency(arr,left,right,element,n);
    }
}

package com.company.algorithm.array.order_statistic;

import java.util.Arrays;

//Minimum number of elements to add to make median equals x
public class OrderStatistic44 {

    private static int minNumber(int[] arr, int n, int x) {
        Arrays.sort(arr);
        int i;
        for (i = 0; arr[n / 2] != x; i++) {
            arr[n++] = x;
            Arrays.sort(arr);
        }
        return i;
    }

    //======================= VERSION 2 ============================
    private static int minNumberV2(int[] arr, int n, int x) {
        int l = 0, h = 0, e = 0;
        for (int i = 0; i < n; i++) {
            if (arr[i] == x)
                e++;
            else if (arr[i] > x)
                h++;
            else if (arr[i] < x)
                l++;
        }
        int ans = 0;
        if (l > h)
            ans = l - h;
        else if (l < h)
            ans = h - l - 1;

        return ans + 1 - e;
    }

    public static void main(String[] args) {
        int x = 10;
        int[] arr = {10, 20, 30, 40};
        int n = arr.length;
        //System.out.println(minNumber(arr, n - 1, x));
        System.out.println(minNumberV2(arr, n, x));
    }
}

package com.company.algorithm.array.order_statistic;

//Maximum Sum Path in Two Arrays
public class OrderStatistic35 {

    private static void printCloset(int[] arr1, int[] arr2, int m, int n, int x) {
        int diff = Integer.MAX_VALUE;

        int resultLeft = 0;
        int resultRight = 0;

        int l = 0, r = n - 1;
        while (l < m && r >= 0) {
            if (Math.abs(arr1[l] + arr2[r] - x) < diff) {
                resultLeft = l;
                resultRight = r;
                diff = Math.abs(arr1[l] + arr2[r] - x);
            }

            if (arr1[l] + arr2[r] > x)
                r--;
            else l++;
        }
        System.out.print("The closest pair is [" + arr1[resultLeft] +
                ", " + arr2[resultRight] + "]");
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 4, 5, 7};
        int[] arr2 = {10, 20, 30, 40};
        int x = 32;
        int m = arr1.length;
        int n = arr2.length;
        printCloset(arr1, arr2, m, n, x);
    }
}

package com.company.algorithm.array.order_statistic;

//Sliding Window Maximum (Maximum of all subarrays of size k)
public class OrderStatistic26 {

    // MY VERSION
    private static void findMaxiumSubArrays(int[] arr, int k, int n) {

        for (int i = 0; i <= n - k; i++) {
            int[] subArr = createSubArr(arr, k, n, i);
            int max = subArr[0];
            for (int j = 1; j < subArr.length; j++) {
                max = Math.max(max, subArr[j]);
            }
            System.out.println(max);
        }
    }

    public static int[] createSubArr(int[] arr, int k, int n, int start) {

        int[] subArr = new int[k];
        for (int i = start, j = 0; i < n; i++, j++) {
            subArr[j] = arr[i];
            if (j == k - 1) {
                return subArr;
            }
        }
        return subArr;
    }

    //=========================== VERSION 2 =============================

    private static void findMaxiumSubArraysV2(int[] arr, int k, int n) {

        for (int i = 0; i <= n - k; i++) {
            int max = arr[i];
            for (int j = 1; j < k; j++) {
                if (arr[i + j] > max)
                    max = arr[i + j];
            }
            System.out.println(max);
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 1, 4, 5, 2, 3, 6};
        int k = 3;
        int n = arr.length;
        //findMaxiumSubArrays(arr, k, n);
        findMaxiumSubArraysV2(arr, k, n);
    }
}

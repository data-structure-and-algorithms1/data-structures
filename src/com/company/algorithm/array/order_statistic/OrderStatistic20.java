package com.company.algorithm.array.order_statistic;

//Find the smallest and second smallest elements in an array
public class OrderStatistic20 {

    private static void findElements(int[] arr, int n) {
        int firstLarge = Integer.MIN_VALUE;
        int secondLarge = Integer.MIN_VALUE;

        for (int i = 0; i < n; i++) {
            firstLarge = Math.max(firstLarge, arr[i]);
        }

        for (int i = 0; i < n; i++) {
            if (firstLarge != arr[i]) {
                secondLarge = Math.max(secondLarge, arr[i]);
            }
        }

        System.out.println("FIRST: " + firstLarge);
        System.out.println("SECOND: " + secondLarge);
    }

    public static void main(String[] args) {
        int[] arr = {5, 3, 23, 17, 20, 6, 9};
        findElements(arr, arr.length);
    }
}

package com.company.algorithm.array.order_statistic;

import java.util.Arrays;

//k-th smallest absolute difference of two elements in an array
public class OrderStatistic17 {

    private static int countPairs(int[] arr, int n, int mid) {
        int res = 0;
        for (int i = 0; i < n; i++) {
            int upperBound = upperBound(arr, n, arr[i] + mid);
            res += (upperBound - i);
        }
        return res;
    }

    private static int upperBound(int[] arr, int n, int value) {
        int low = 0;
        int high = n;

        while (low < high) {
            final int mid = (low + high) / 2;
            if (value >= arr[mid])
                low = mid + 1;
            else
                high = mid;
        }
        return low;
    }

    private static int findSmallestDifference(int[] arr, int n, int k) {
        Arrays.sort(arr);

        // minimum absolute difference
        int low = arr[1] - arr[0];
        for (int i = 1; i <= n - 2; i++) {
            low = Math.min(low, arr[i + 1] - arr[i]);
        }

        // maximum absolute difference
        int high = arr[n - 1] - arr[0];

        while (low < high) {
            int mid = (low + high) >> 1;
            if (countPairs(arr, n, mid) < k) {
                low = mid + 1;
            } else
                high = mid;
        }
        return low;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4};
        int k = 3;
        System.out.println(findSmallestDifference(arr, arr.length, k));
    }
}

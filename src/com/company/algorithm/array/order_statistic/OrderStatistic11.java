package com.company.algorithm.array.order_statistic;

import java.util.PriorityQueue;

//Minimum product of k integers in an array of positive Integers
public class OrderStatistic11 {

    private static int minProduct(int[] arr, int n, int k) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        for (int i = 0; i < n; i++)
            pq.add(arr[i]);

        int count = 0;
        int ans = 1;
        while (!pq.isEmpty() && count < k) {
            ans = ans * pq.element();
            pq.remove();
            count++;
        }
        return ans;

    }

    public static void main(String[] args) {
        int[] arr = {198, 76, 544, 123, 154, 675};
        int k = 2;
        int n = arr.length;
        System.out.println(minProduct(arr, n, k));
    }

}

package com.company.algorithm.array.order_statistic;

//Maximum Sum Path in Two Arrays
public class OrderStatistic34 {

    private static int maxPathSum(int[] arr1, int[] arr2, int m, int n) {
        int i = 0, j = 0;

        int result = 0, sum1 = 0, sum2 = 0;

        while (i < m && j < n) {
            if (arr1[i] < arr2[j])
                sum1 += arr1[i++];
            else if (arr1[i] > arr2[j])
                sum2 += arr2[j++];
            else {
                result += Math.max(sum1, sum2) + arr1[i];
                sum1 = 0;
                sum2 = 0;
                i++;
                j++;
            }
        }

        while (i < m)
            sum1 += arr1[i++];

        while (j < n)
            sum2 += arr2[j++];

        result += Math.max(sum1, sum2);
        return result;
    }

    //Space Complexity: O(1).
    //Time complexity: O(m+n).
    public static void main(String[] args) {
        int[] ar1 = {2, 3, 7, 10, 12, 15, 30, 34};
        int[] ar2 = {1, 5, 7, 8, 10, 15, 16, 19};
        int m = ar1.length;
        int n = ar2.length;

        System.out.println(maxPathSum(ar1, ar2, m, n));
    }
}

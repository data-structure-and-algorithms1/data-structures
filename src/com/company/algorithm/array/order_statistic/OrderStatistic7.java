package com.company.algorithm.array.order_statistic;

//Program to find last 3 largest element in an array
public class OrderStatistic7 {

    private static void findLargestThreeElement(int[] arr, int n) {
        int first, second, third;
        first = second = third = 0;

        if(n < 3){
            System.out.println("Invalid");
            return;
        }

        for (int i = 0; i < n; i++) {
            if (arr[i] > first) {
                third = second;
                second = first;
                first = arr[i];
            } else if (arr[i] > second) {
                third = second;
                second = arr[i];
            } else if (arr[i] > third) {
                third = arr[i];
            }
        }

        System.out.println("1st element: " + first);
        System.out.println("2nd element: " + second);
        System.out.println("3rd element: " + third);
    }

    public static void main(String[] args) {
        int[] arr = {10, 4, 3, 50, 23, 90, 28, 49};
        int n = arr.length;
        findLargestThreeElement(arr, n);
    }
}

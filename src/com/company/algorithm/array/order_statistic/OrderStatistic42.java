package com.company.algorithm.array.order_statistic;

//Find k pairs with smallest sums in two arrays
public class OrderStatistic42 {

    private static void findSmallestPair(int[] arr1, int[] arr2, int n, int k) {
        if (k > n * 2){
            System.out.println("k pairs don't exists");
            return;
        }

        int[] index2 = new int[n];

        while (k > 0){
            int minSum = Integer.MAX_VALUE;
            int minIndex = 0;

            for (int i = 0; i < n; i++) {
                if(index2[i] < n && arr1[i] + arr2[index2[i]] < minSum){
                    minIndex = i;
                    minSum = arr1[i] + arr2[index2[i]];
                }
            }

            System.out.println("[" + arr1[minIndex] + ", " + arr2[index2[minIndex]] + "]");
            index2[minIndex]++;
            k--;
        }
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 7, 11};
        int[] arr2 = {2, 4, 6};
        int n = arr1.length;
        int k = 9;

        findSmallestPair(arr1, arr2, n, k);
    }
}

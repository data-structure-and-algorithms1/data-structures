package com.company.algorithm.array.order_statistic;

//Find the minimum distance between two numbers
public class OrderStatistic27 {

    private static int minDist(int[] arr, int n, int x, int y) {
        int prevIndex = -1;
        int minDist = Integer.MAX_VALUE;

        for (int i = 0; i < n; i++) {

            if (arr[i] == x || arr[i] == y) {
                if (prevIndex != -1 && arr[i] != arr[prevIndex]) {
                    minDist = Math.min(minDist, i - prevIndex);
                }
                prevIndex = i;
            }
        }

        if (minDist == Integer.MAX_VALUE)
            return -1;
        return minDist;
    }

    public static void main(String[] args) {
        int[] arr = {3, 5, 4, 2, 6, 3, 0, 0, 5, 4, 8, 3};
        int n = arr.length;
        int x = 3;
        int y = 6;
        System.out.println("Minimum distance between " + x + " and " + y
                + " is " + minDist(arr, n, x, y));
    }
}

package com.company.algorithm.array.order_statistic;

import java.util.Stack;

//Delete array elements which are smaller than next or become smaller
public class OrderStatistic38 {

    private static void deleteElements(int[] arr, int n, int k) {
        Stack<Integer> s = new Stack<>();
        s.push(arr[0]);

        int count = 0;

        for (int i = 1; i < n; i++) {
            while (!s.isEmpty() && s.peek() < arr[i] && count < k) {
                s.pop();
                count++;
            }
            s.push(arr[i]);
        }

        int m = s.size();
        Integer[] v = new Integer[m];
        while (!s.isEmpty()) {
            v[--m] = s.peek();
            s.pop();
        }

        for (Integer x : v) {
            System.out.print(x + " ");
        }

        System.out.println();
    }

    public static void main(String[] args) {
        int[] arr = {20, 10, 25, 30, 40};
        int n = arr.length;
        int k = 2;
        deleteElements(arr, n, k);
    }
}

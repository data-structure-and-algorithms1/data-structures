package com.company.algorithm.array.order_statistic;

import java.util.PriorityQueue;

//K-th Largest Sum Contiguous Subarray
public class OrderStatistic12 {

    private static void findSumKthLargest(int[] arr, int k, int n) {
        int[] sum = new int[n + 1];
        sum[0] = 0;
        sum[1] = arr[0];
        for (int i = 2; i <= n; i++) {
            sum[i] = sum[i - 1] + arr[i - 1];
        }

        PriorityQueue<Integer> queue = new PriorityQueue<>();

        for (int i = 1; i <= n; i++) {
            for (int j = i; j <= n ; j++) {
                int x = sum[j] - sum[i-1];

                if(queue.size() < k)
                    queue.add(x);
                else {
                    if(queue.peek() < x){
                        queue.poll();
                        queue.add(x);
                    }
                }
            }
        }

        System.out.println(queue.poll());
    }

    public static void main(String[] args) {
        int[] arr = {20, -5, -1};
        int k = 3;
        int n = arr.length;
        findSumKthLargest(arr, k, n);
    }

}

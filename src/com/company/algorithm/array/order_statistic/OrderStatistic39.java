package com.company.algorithm.array.order_statistic;

import java.util.Vector;

//Find zeroes to be flipped so that number of consecutive 1’s is maximized
public class OrderStatistic39 {

    private static Vector<Integer> maximizedOne(int[] arr, int n, int m) {
        int[] left = new int[n];
        int[] right = new int[n];

        Vector<Integer> zeroPos = new Vector<>();
        int count = 0;
        int previousIndexOfZero = -1;
        for (int i = 0; i < n; i++) {
            if (arr[i] != 0) {
                count++;
            } else {
                left[i] = count;
                zeroPos.add(i);
                if (previousIndexOfZero != i && previousIndexOfZero != -1) {
                    right[previousIndexOfZero] = count;
                }
                count = 0;
                previousIndexOfZero = i;
            }
        }
        right[previousIndexOfZero] = count;

        int maxOnde = -1;
        Vector<Integer> resultIndex = new Vector<>();
        int i = 0;

        while (i <= (zeroPos.size()) - m) {
            int temp = 0;
            Vector<Integer> index = new Vector<>();
            for (int c = 0; c < m; c++) {
                temp += left[zeroPos.elementAt(i + c)]
                        + right[zeroPos.elementAt(i + c)] + 1;
                index.add(zeroPos.elementAt(i + c));
            }

            temp = temp - (m - 1);
            if (temp > maxOnde) {
                maxOnde = temp;
                resultIndex = index;
            }
            i += 1;
        }
        return resultIndex;
    }

    public static void main(String[] args) {
        int[] arr = { 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1 };
        int m = 2;
        int n = arr.length;
        System.out.print("Index of zeroes that are flipped: [");
        Vector<Integer> result_index = maximizedOne(arr, n, m);
        for (int i : result_index) {
            System.out.print(i+ " ");
        }
        System.out.print("]");
    }
}

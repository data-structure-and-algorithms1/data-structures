package com.company.algorithm.array.order_statistic;

//Find the largest pair sum in an unsorted array
public class OrderStatistic36 {

    // ======================= VERSION 1 =========================
    private static void findLargestPairSum(int[] arr, int n) {
        int first = 0;
        int second = 0;

        for (int i = 0; i < n; i++) {
            if (arr[i] > first) {
                first = arr[i];
            }
        }

        for (int i = 0; i < n; i++) {
            if (arr[i] != first && arr[i] > second) {
                second = arr[i];
            }
        }

        int sum = first + second;
        System.out.println(sum);
    }

    //======================= VERSION 2 ======================
    private static void findLargestPairSumV2(int[] arr, int n) {
        int j = 0;
        int max = n == 1 ? arr[0] + arr[1] : arr[0];

        for (int i = 0; i < n; i++) {
            int sum = arr[j] + arr[i];
            if (sum > max) {
                max = sum;
                if (arr[j] < arr[i])
                    j = i;
            }
        }

        System.out.println(max);
    }

    public static void main(String[] args) {
        int[] arr = {12, 34, 10, 6, 40};
        int n = arr.length;
        // findLargestPairSum(arr, n);
        findLargestPairSumV2(arr, n);
    }
}

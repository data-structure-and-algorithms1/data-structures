package com.company.algorithm.array.order_statistic;

import java.util.Arrays;
import java.util.HashSet;
import java.util.PriorityQueue;

//K maximum sum combinations from two arrays
public class OrderStatistic13_v_2 {

    private static class Pair {
        int l;
        int m;

        public Pair(int l, int m) {
            this.l = l;
            this.m = m;
        }
    }

    private static class PairSum implements Comparable<PairSum> {
        int sum;
        int l;
        int m;

        public PairSum(int sum, int l, int m) {
            this.sum = sum;
            this.l = l;
            this.m = m;
        }

        @Override
        public int compareTo(PairSum o) {
            return Integer.compare(o.sum, sum);
        }
    }

    private static void maxPairSum(Integer[] A, Integer[] B, int n, int k) {
        Arrays.sort(A);
        Arrays.sort(B);

        PriorityQueue<PairSum> sums = new PriorityQueue<>();

        HashSet<Pair> pairs = new HashSet<>();

        int l = n - 1;
        int m = n - 1;
        pairs.add(new Pair(l, m));
        sums.add(new PairSum(A[l] + B[m], l, m));

        for (int i = 0; i < k; i++) {
            PairSum max = sums.poll();
            System.out.println(max.sum);
            l = max.l - 1;
            m = max.m;

            if (l >= 0 && m >= 0 && !pairs.contains(new Pair(l, m))) {
                sums.add(new PairSum(A[l] + B[m], l, m));
                pairs.add(new Pair(l, m));
            }
            l = max.l;
            m = max.m - 1;

            if (l >= 0 && m >= 0 && !pairs.contains(new Pair(l, m))) {
                sums.add(new PairSum(A[l] + B[m], l, m));
                pairs.add(new Pair(l, m));
            }
        }
    }

    public static void main(String[] args) {
        Integer A[] = {1, 4, 2, 3};
        Integer B[] = {2, 5, 1, 6};
        int n = A.length;
        int k = 4;

        maxPairSum(A, B, n, k);

    }
}

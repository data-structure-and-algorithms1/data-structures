package com.company.algorithm.array.order_statistic;

//Maximum sum such that no two elements are adjacent
public class OrderStatistic22 {

    private static int findMaxSum(int[] arr, int n) {
        int incl = arr[0];
        int excl = 0;
        int exclNew;

        for (int i = 1; i < n; i++) {
            exclNew = Math.max(incl, excl);
            incl = excl + arr[i];
            excl = exclNew;
        }

        return Math.max(incl, excl);
    }


    public static void main(String[] args) {
        int[] arr = {5, 5, 10, 100, 10, 5};
        int n = arr.length;

        System.out.println(findMaxSum(arr, n));
    }
}

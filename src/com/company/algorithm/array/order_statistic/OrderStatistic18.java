package com.company.algorithm.array.order_statistic;

//Find Second largest element in an array
public class OrderStatistic18 {

    private static void findSecondLargestElement(int[] arr, int n) {
        int second;

        int largest = second = Integer.MIN_VALUE;

        for (int i = 0; i < n; i++) {
            largest = Math.max(largest, arr[i]);
        }

        for (int i = 0; i < n; i++) {
            if (arr[i] != largest) {
                second = Math.max(second, arr[i]);
            }
        }

        if (second == Integer.MIN_VALUE) {
            System.out.println("No second largest element available");
        } else {
            System.out.println("The second largest element: " + second);
        }
    }

    public static void main(String[] args) {
        int[] arr = {12, 35, 1, 10, 34, 1};

        findSecondLargestElement(arr, arr.length);
    }
}

package com.company.algorithm.array.order_statistic;

//Maximum and minimum of an array using minimum number of comparisons
public class OrderStatistic23 {

    private static class Pair {
        int min;
        int max;
    }

    private static Pair getMinMax(int[] arr, int n) {
        Pair minMax = new Pair();
        int i;

        if (n % 2 == 0) {
            if (arr[0] > arr[1]) {
                minMax.max = arr[0];
                minMax.min = arr[1];
            } else {
                minMax.min = arr[0];
                minMax.max = arr[1];
            }
            i = 2;
        } else {
            minMax.min = arr[0];
            minMax.max = arr[0];
            i = 1;
        }

        while (i < n - 1) {
            if (arr[i] > arr[i + 1]) {
                if (arr[i] > minMax.max) {
                    minMax.max = arr[i];
                }
                if (arr[i + 1] < minMax.min) {
                    minMax.min = arr[i + 1];
                }
            } else {
                if (arr[i + 1] > minMax.max) {
                    minMax.max = arr[i + 1];
                }
                if (arr[i] < minMax.min) {
                    minMax.min = arr[i];
                }
            }
            i += 2;
        }

        return minMax;

    }

    public static void main(String[] args) {
        int[] arr = {1000, 11, 445, 1, 330, 3000};
        int n = arr.length;
        Pair minMax = getMinMax(arr, n);
        System.out.println(minMax.max);
        System.out.println(minMax.min);
    }
}

package com.company.algorithm.array.order_statistic;

import java.util.Random;

//K’th Smallest/Largest Element in Unsorted Array | Set 2
public class OrderStatistic2 {

    private static int kthSmallest(int[] arr, int l, int r, int k) {
        if (k > 0 && k <= r - l + 1) {
            int pos = randomPartition(arr, l, r);

            if (pos - l == k - 1)
                return arr[pos];
            if (pos - l > k - 1)
                return kthSmallest(arr, l, pos - 1, k);
            return kthSmallest(arr, pos + 1, r, k - pos + l - 1);
        }
        return Integer.MAX_VALUE;
    }

    private static int partition(int[] arr, int l, int r) {
        int x = arr[r];
        int i = l;
        for (int j = l; j <= r - 1; j++) {
            if (arr[j] <= x) {
                swap(arr, i, j);
                i++;
            }
        }
        swap(arr, i, r);
        return i;
    }

    private static int randomPartition(int[] arr, int l, int r) {
        int n = r - l + 1;
        int pivot = new Random().nextInt(n);
        swap(arr, l + pivot, r);
        return partition(arr, l, r);
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        int[] arr = {12, 3, 5, 7, 4, 19, 26};
        int k = 3; // elementin pozisiyasi yeni index deyil
        System.out.println(kthSmallest(arr, 0, arr.length - 1, k));
    }
}

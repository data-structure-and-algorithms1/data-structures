package com.company.algorithm.array.order_statistic;

//Given an array of size n and a number k, find all elements that appear more than n/k times
public class OrderStatistic33 {

    private static class EleCount {
        int element;
        int count;
    }

    private static void moreThanNdk(int[] arr, int n, int k) {
        if (k < 2)
            return;

        /* Step 1: Create a temporary
       array (contains element
       and count) of size k-1.
       Initialize count of all
       elements as 0 */
        EleCount[] temp = new EleCount[k - 1];
        for (int i = 0; i < k - 1; i++) {
            temp[i] = new EleCount();
        }

       /* Step 2: Process all
        elements of input array */
        processAllElement(arr, n, k, temp);

        /*Step 3: Check actual counts of
         * potential candidates in temp[]*/
        checkActualCounts(arr, n, k, temp);
    }

    private static void processAllElement(int[] arr, int n, int k, EleCount[] temp) {
        for (int i = 0; i < n; i++) {
            int j;
            for (j = 0; j < k - 1; j++) {
                if (temp[j].element == arr[i]) {
                    temp[j].count += 1;
                    break;
                }
            }

            if (j == k - 1) {
                int l;

                for (l = 0; l < k - 1; l++) {
                    if (temp[l].count == 0) {
                        temp[l].element = arr[i];
                        temp[l].count = 1;
                        break;
                    }
                }

                if (l == k - 1)
                    for (l = 0; l < k - 1; l++)
                        temp[l].count -= 1;
            }
        }
    }

    private static void checkActualCounts(int[] arr, int n, int k, EleCount[] temp) {
        for (int i = 0; i < k - 1; i++) {
            int actualCount = 0;
            for (int j = 0; j < n; j++) {
                if (arr[j] == temp[i].element)
                    actualCount++;
            }

            if (actualCount > n / k) {
                System.out.println("Number:" + temp[i].element
                        + " Count:" + actualCount + "\n");
            }
        }
    }

    //Time Complexity: O(nk)
    //Auxiliary Space: O(k)
    public static void main(String[] args) {
        int[] arr1 = {4, 5, 6, 7, 8, 4, 4};
        int size = arr1.length;
        int k = 3;
        moreThanNdk(arr1, size, k);
    }
}

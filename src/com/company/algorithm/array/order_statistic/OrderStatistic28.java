package com.company.algorithm.array.order_statistic;

//Find the maximum element in an array which is first increasing and then decreasing
public class OrderStatistic28 {

    // =================== VERSION 1 ==========================
    private static void findMaximumElement(int[] arr, int n) {
        int element = -1;

        for (int i = 0; i < n - 2; i++) {
            if (arr[i] < arr[i + 1] && arr[i + 1] > arr[i + 2]) {
                element = arr[i + 1];
            }
        }
        System.out.println(element);
    }

    // ================== VERSION 2 =======================
    private static int findMaximumElementV2(int[] arr, int low, int high) {
        if (low == high)
            return arr[low];

        if ((high == low + 1) && arr[low] >= arr[high])
            return arr[low];

        if ((high == low + 1) && arr[low] < arr[high])
            return arr[high];

        int mid = (low + high) / 2;

        if (arr[mid] > arr[mid + 1] && arr[mid] > arr[mid - 1])
            return arr[mid];

        if (arr[mid] > arr[mid + 1] && arr[mid] < arr[mid - 1])
            return findMaximumElementV2(arr, low, mid - 1);
        else
            return findMaximumElementV2(arr, mid + 1, high);
    }

    public static void main(String[] args) {
        int[] arr = {8, 10, 20, 80, 100, 200, 400, 500, 3, 2, 1};
        // findMaximumElement(arr, arr.length);
        System.out.println(findMaximumElementV2(arr, 0, arr.length - 1));
    }

}

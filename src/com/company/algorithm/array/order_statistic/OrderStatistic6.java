package com.company.algorithm.array.order_statistic;

import java.util.Arrays;

//Program to find largest element in an array
public class OrderStatistic6 {

    // version 1
    private static void findLargest(int[] arr, int n) {
        Arrays.sort(arr);
        System.out.println(arr[n - 1]);
    }

    // version 2
    private static void findLargestV2(int[] arr, int n) {

        int max = arr[0];
        for (int i = 1; i < n; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        System.out.println(max);
    }

    public static void main(String[] args) {
        int[] arr = {20, 10, 20, 4, 100, 1};
        int n = arr.length;
        findLargestV2(arr, n);
    }
}

package com.company.algorithm.array.order_statistic;

import java.util.Stack;

//Next Greater Element
public class OrderStatistic45_v_2 {

    private static void printNGE(int[] arr, int n) {

        Stack<Integer> s = new Stack<>();
        int []nge = new int[n];
        for (int i = n-1; i >= 0 ; i--) {
            if(!s.isEmpty()){
                while (!s.isEmpty() && s.peek() <= arr[i]){
                    s.pop();
                }
            }
            nge[i] = s.empty() ? -1 : s.peek();
            s.push(arr[i]);
        }

        for (int i = 0; i < n; i++) {
            System.out.println(arr[i] + "--> " + nge[i]);
        }
    }

    public static void main(String[] args) {
        int[] arr = {13, 7, 6, 12};
        int n = arr.length;
        printNGE(arr, n);
    }
}

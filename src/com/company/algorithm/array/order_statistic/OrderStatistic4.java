package com.company.algorithm.array.order_statistic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

//k largest(or smallest) elements in an array | added Min Heap method
public class OrderStatistic4 {

    private static List<Integer> kLargest(int[] arr, int k) {
        Integer[] objArray = Arrays.stream(arr).boxed().toArray(Integer[]::new);
        Arrays.sort(objArray, Collections.reverseOrder());
        return new ArrayList<>(Arrays.asList(objArray).subList(0, k));
    }

    public static void main(String[] args) {
        int[] arr = new int[]{1, 23, 12, 9, 30, 2, 50};
        int k = 3;
        System.out.println(kLargest(arr, k));
    }
}

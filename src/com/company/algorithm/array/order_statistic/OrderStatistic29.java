package com.company.algorithm.array.order_statistic;

import java.util.Arrays;

//Count smaller elements on right side
public class OrderStatistic29 {

    private static void constructLowerArray(int[] arr, int[] countSmaller, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[j] < arr[i])
                    countSmaller[i]++;
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {12, 10, 5, 4, 2, 20, 6, 1, 0, 2};
        int n = arr.length;
        int[] low = new int[n];
        constructLowerArray(arr, low, n);
        System.out.println(Arrays.toString(low));
    }

}

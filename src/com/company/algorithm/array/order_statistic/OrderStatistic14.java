package com.company.algorithm.array.order_statistic;

//K maximum sums of non-overlapping contiguous sub-arrays
public class OrderStatistic14 {

    private static void kMax(int[] arr, int k, int n) {
        for (int c = 0; c < k; c++) {
            int maxSoFar = Integer.MIN_VALUE;
            int maxHere = 0;

            int start = 0, end = 0, s = 0;
            for (int i = 0; i < n; i++) {
                maxHere += arr[i];
                if (maxSoFar < maxHere) {
                    maxSoFar = maxHere;
                    start = s;
                    end = i;
                }
                if (maxHere < 0) {
                    maxHere = 0;
                    s = i + 1;
                }
            }
            System.out.println("Maximum non-overlapping sub-arraysum" +
                    (c + 1) + ": " + maxSoFar +
                    ", starting index: " + start +
                    ", ending index: " + end + ".");
            for (int l = start; l <= end; l++) {
                arr[l] = Integer.MIN_VALUE;
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int[] arr1 = {4, 1, 1, -1, -3, -5, 6, 2, -6, -2};
        int k1 = 3;
        int n1 = arr1.length;
        kMax(arr1, k1, n1);
    }
}

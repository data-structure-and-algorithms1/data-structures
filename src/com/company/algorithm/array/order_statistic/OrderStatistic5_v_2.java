package com.company.algorithm.array.order_statistic;

//Kth smallest element in a row-wise and column-wise sorted 2D array | Set 1  version 2
public class OrderStatistic5_v_2 {

    static class HeapNode {
        int val;
        int r; // row number
        int c; // column

        public HeapNode(int val, int r, int c) {
            this.val = val;
            this.r = r;
            this.c = c;
        }
    }

    private static void minHeapify(HeapNode harr[], int i, int heapSize) {
        int l = 2 * i + 1;
        int r = 2 * i + 2;

        if (l < heapSize && r < heapSize && harr[l].val < harr[i].val && harr[r].val < harr[i].val) {
            HeapNode temp = harr[r];
            harr[r] = harr[i];
            harr[i] = harr[l];
            harr[l] = temp;
            minHeapify(harr, l, heapSize);
            minHeapify(harr, r, heapSize);
        }
        if (l < heapSize && harr[l].val < harr[i].val) {
            HeapNode temp = harr[i];
            harr[i] = harr[l];
            harr[l] = temp;
            minHeapify(harr, l, heapSize);
        }
    }

    private static int kthSmallest(int[][] mat, int n, int k) {
        if (k < 0 || k >= n * n) {
            return Integer.MAX_VALUE;
        }

        HeapNode[] harr = new HeapNode[n];
        for (int i = 0; i < n; i++) {
            harr[i] = new HeapNode(mat[0][i], 0, i);
        }
        HeapNode hr = new HeapNode(0, 0, 0);

        for (int i = 1; i <= k; i++) {
            hr = harr[0];

            int nextVal = hr.r < n - 1 ? mat[hr.r + 1][hr.c] : Integer.MAX_VALUE;

            harr[0] = new HeapNode(nextVal, hr.r + 1, hr.c);
            minHeapify(harr, 0, n);
        }
        return hr.val;
    }

    public static void main(String[] args) {
        int[][] matrix = {
                {10, 20, 30, 40},
                {15, 25, 35, 45},
                {24, 29, 37, 48},
                {32, 33, 39, 50}
        };
        int k = 7;
        int res = kthSmallest(matrix, matrix.length, k);
        System.out.println(res);
    }
}

package com.company.algorithm.array.order_statistic;

import java.util.Arrays;

//Kth smallest element in a row-wise and column-wise sorted 2D array | Set 1
public class OrderStatistic5 {

    private static void kSmallest(int[][] matrix, int k) {
        int n = matrix.length;
        int[] arr = new int[n * n];
        int t = 0;
        for (int[] ints : matrix) {
            for (int j = 0; j < n; j++) {
                arr[t++] = ints[j];
            }
        }

        Arrays.sort(arr);
        System.out.println(arr[k - 1]);
    }

    public static void main(String[] args) {
        int[][] matrix = {
                {10, 20, 30, 40},
                {15, 25, 35, 45},
                {24, 29, 37, 48},
                {32, 33, 39, 50}
        };
        int k = 3;
        kSmallest(matrix, k);
    }
}

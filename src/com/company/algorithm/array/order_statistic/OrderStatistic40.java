package com.company.algorithm.array.order_statistic;

//Count Strictly Increasing Subarrays
public class OrderStatistic40 {

    private static void findIncreasingSubArrays(int[] arr, int n) {
        int cnt = 0;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[j] > arr[j - 1])
                    cnt++;
                else break;
            }
        }
        System.out.println(cnt);
    }

    // ==================== VERSION 2 ========================
    private static void findIncreasingSubArraysV2(int[] arr, int n) {
        int cnt = 0;
        int len = 1;

        for (int i = 0; i < n - 1; i++) {
            if (arr[i + 1] > arr[i])
                len++;
            else {
                cnt += (((len - 1) * len) / 2);
                len = 1;
            }
        }
        if (len > 1)
            cnt += (((len - 1) * len) / 2);

        System.out.println(cnt);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4};
        int n = arr.length;
        findIncreasingSubArraysV2(arr, n);
    }
}

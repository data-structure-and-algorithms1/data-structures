package com.company.algorithm.array.order_statistic;

import java.util.Collections;
import java.util.PriorityQueue;

//K maximum sum combinations from two arrays
public class OrderStatistic13 {

    // Version 1
    // Time complexity big O(n^2)
    private static void kMaxCombinations(int[] A, int[] B, int n, int k) {
        PriorityQueue<Integer> pq = new PriorityQueue<>(Collections.reverseOrder());

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                pq.add(A[i] + B[j]);
            }
        }

        int count = 0;

        while (count < k) {
            System.out.println(pq.peek());
            pq.remove();
            count++;
        }
    }

    public static void main(String[] args) {
        int[] A = {3, 2};
        int[] B = {1, 4};
        int n = A.length;
        int k = 2;
        kMaxCombinations(A,B,n,k);
    }
}

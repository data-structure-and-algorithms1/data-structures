package com.company.algorithm.array.order_statistic;

import java.util.Arrays;

//Maximum difference between group of k-elements and rest of the array
public class OrderStatistic43 {

    private static void maximumDifference(int[] arr, int n, int k) {
        Arrays.sort(arr);
        int group1 = 0;
        int group2 = 0;
        for (int i = 0; i < n; i++) {
            if (i >= k)
                group2 += arr[i];
            else
                group1 += arr[i];
        }
        System.out.println(group2 - group1);
    }

    //=================== VERSION 1 ================================

    private static long arraySum(int[] arr, int n) {
        long sum = 0;
        for (int i = 0; i < n; i++) {
            sum = sum + arr[i];
        }
        return sum;
    }

    private static long maxDiff(int[] arr, int n, int k) {
        Arrays.sort(arr);
        long arraySum = arraySum(arr, n);

        long diff1 = Math.abs(arraySum - 2 * arraySum(arr, k));
        int end = arr.length - 1;
        int start = 0;
        while (start < end) {
            int temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }

        long diff2 = Math.abs(arraySum - 2 * arraySum(arr, k));

        return Math.max(diff1, diff2);
    }

    public static void main(String[] args) {
        int[] arr = {1, 5, 2, 6, 3};
        int k = 2;
        int n = arr.length;
        //maximumDifference(arr, n, k);
        System.out.println(maxDiff(arr, n, k));
    }
}

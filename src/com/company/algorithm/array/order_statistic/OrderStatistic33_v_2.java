package com.company.algorithm.array.order_statistic;

import java.util.HashMap;
import java.util.Map;

//Given an array of size n and a number k, find all elements that appear more than n/k times
public class OrderStatistic33_v_2 {

    private static void moreThanNdk(int[] arr, int n) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < n; i++) {
            if (map.containsKey(arr[i])) {
                map.put(arr[i], (map.get(arr[i]) + 1));
            } else {
                map.put(arr[i], 1);
            }
        }

        int maxCount = 0;
        int value = -1;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() > maxCount) {
                maxCount = entry.getValue();
                value = entry.getKey();
            }
        }

        System.out.println("VALUE: " + value + " COUNT: " + maxCount);
    }

    public static void main(String[] args) {
        int[] arr1 = {4, 5, 6, 7, 8, 4, 4, 5};
        int size = arr1.length;
        moreThanNdk(arr1, size);
    }
}

package com.company.algorithm.array.order_statistic;

import java.util.Arrays;

//Program for Mean and median of an unsorted array
public class OrderStatistic9 {

    private static double findMean(int[] arr, int n) {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += arr[i];
        }
        return (double) sum / (double) n;
    }

    private static double findMedian(int[] arr, int n) {
        Arrays.sort(arr);
        if (n % 2 != 0)
            return arr[n / 2];
        return (double) (arr[(n - 1) / 2] + arr[n / 2]) / 2.0;
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 4, 2, 6, 5, 8, 7};
        System.out.println(findMean(arr,arr.length));
        System.out.println(findMedian(arr,arr.length));
    }
}

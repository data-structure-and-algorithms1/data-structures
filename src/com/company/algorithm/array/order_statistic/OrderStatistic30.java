package com.company.algorithm.array.order_statistic;

import java.util.Arrays;

//Longest Increasing Subsequence Size (N log N)
public class OrderStatistic30 {

    private static int longestIncreasingSubsequenceLength(int[] arr) {
        if (arr.length == 0)
            return 0;

        int[] tail = new int[arr.length];
        int length = 1;
        tail[0] = arr[0];

        for (int i = 1; i < arr.length; i++) {

            if (arr[i] > tail[length - 1]) {
                tail[length++] = arr[i];
            }
            else {
                int idx = Arrays.binarySearch(
                        tail, 0, length - 1, arr[i]);

                if (idx < 0)
                    idx = -1 * idx - 1;

                tail[idx] = arr[i];
            }
        }
        return length;
    }

    public static void main(String[] args) {
        int[] arr = {2, 5, 3, 7, 11, 8, 10, 13, 6};
        System.out.println(longestIncreasingSubsequenceLength(arr));
    }
}

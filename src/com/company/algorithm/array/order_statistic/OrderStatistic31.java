package com.company.algorithm.array.order_statistic;

//Find the smallest positive number missing from an unsorted array | Set 1
public class OrderStatistic31 {

    // ========================= VERSION 1 ===============================
    private static int findMissingSmallNumber(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            while (arr[i] >= 1 && arr[i] <= n && arr[i] != arr[arr[i] - 1]) {
                int temp = arr[arr[i] - 1];
                arr[arr[i] - 1] = arr[i];
                arr[i] = temp;
            }
        }

        for (int i = 0; i < n; i++) {
            if (arr[i] != i + 1)
                return (i + 1);
        }
        return n + 1;
    }

    //============================ VERSION 2 ==============================
    private static int solution(int[] arr, int n) {
        boolean[] present = new boolean[n + 1];

        for (int i = 0; i < n; i++) {
            if (arr[i] > 0 && arr[i] <= n)
                present[arr[i]] = true;
        }

        for (int i = 1; i <= n; i++) {
            if (!present[i])
                return i;
        }

        return n + 1;
    }

    public static void main(String[] args) {
        int[] arr = {2, 3, -7, 6, 8, 1, -10, 15};
        int n = arr.length;
//        int ans = findMissingSmallNumber(arr, n);
//        System.out.println(ans);
        System.out.println(solution(arr,n));
    }
}

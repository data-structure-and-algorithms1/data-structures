package com.company.algorithm.array.order_statistic;

//Find k pairs with smallest sums in two arrays
public class OrderStatistic16 {

    public static void findSmallestSums(int[] arr1, int[] arr2, int k) {
        int n1 = arr1.length;
        int n2 = arr2.length;

        if( k > n1*n2){
            return;
        }

        int[] index2 = new int[n1];

        while (k > 0){
            int minSum = Integer.MAX_VALUE;
            int minIndex = 0;

            for (int i = 0; i < n1 ; i++) {
                if(index2[i] < n2 && arr1[i] + arr2[index2[i]] < minSum){
                    minIndex = i;
                    minSum = arr1[i] + arr2[index2[i]];
                }
            }
            System.out.println("(" + arr1[minIndex] + ", " + arr2[index2[minIndex]] + ")");
            index2[minIndex]++;
            k--;
        }
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 7, 11};
        int[] arr2 = {2, 4, 6};
        int k = 5;

        findSmallestSums(arr1,arr2,k);
    }
}

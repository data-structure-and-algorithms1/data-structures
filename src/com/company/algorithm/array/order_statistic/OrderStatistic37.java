package com.company.algorithm.array.order_statistic;

import java.util.HashSet;
import java.util.Vector;

//Smallest greater elements in whole array
public class OrderStatistic37 {

    private static void findGreater(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            int diff = Integer.MAX_VALUE;
            int closet = -1;
            for (int j = 0; j < n; j++) {
                if (arr[i] < arr[j] && arr[j] - arr[i] < diff) {
                    diff = arr[j] - arr[i];
                    closet = j;
                }
            }

            if (closet == -1)
                System.out.print("_ ");
            else
                System.out.print(arr[closet] + " ");
        }
    }

    // ====================== VERSION 2 ==============================

    private static void smallestGreater(int[] arr, int n) {
        HashSet<Integer> set = new HashSet<>();
        for (int i = 0; i < n; i++) {
            set.add(arr[i]);
        }

        Vector<Integer> vector = new Vector<>(set);

        for (int i = 0; i < n; i++) {
            int temp = lowerBound(vector, 0, vector.size(), arr[i]);
            if (temp < n)
                System.out.print(vector.get(temp) + " ");
            else
                System.out.print("_ ");
        }
    }

    private static int lowerBound(Vector<Integer> vec, int low, int high, int element) {
        while (low < high) {
            int middle = low + (high - low) / 2;
            if (element > vec.elementAt(middle)) {
                low = middle + 1;
            } else {
                high = middle;
            }
        }
        return low + 1;
    }

    public static void main(String[] args) {
        int[] arr = {6, 3, 9, 8, 10, 2, 1, 15, 7};
        //findGreater(arr, arr.length);
        smallestGreater(arr, arr.length);
    }
}

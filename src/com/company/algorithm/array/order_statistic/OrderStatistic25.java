package com.company.algorithm.array.order_statistic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Given an array arr[], find the maximum j – i such that arr[j] > arr[i]
public class OrderStatistic25 {

    // ================== VERSION 1 =====================
    // Time Complexity: O(n^2)
    private static int maxIndexDiff(int[] arr, int n) {
        int maxDiff = -1;
        for (int i = 0; i < n; i++) {
            for (int j = n - 1; j > i; j--) {
                if (arr[j] > arr[i] && maxDiff < (j - i))
                    maxDiff = j - i;
            }
        }
        return maxDiff;
    }

    //====================== VERSION 2 ======================
    //Time complexity : O(N*log(N))
    //Space complexity: O(N)
    private static void maxIndexDiffV2(int[] arr, int n) {
        int[] maxFromEnd = new int[n + 1];
        Arrays.fill(maxFromEnd, Integer.MIN_VALUE);

        for (int i = n - 1; i >= 0; i--) {
            maxFromEnd[i] = Math.max(maxFromEnd[i + 1], arr[i]);
        }

        int result = 0;

        for (int i = 0; i < n; i++) {
            int low = i + 1, high = n - 1;
            int ans = i;

            while (low <= high) {
                int mid = (low + high) / 2;
                if (arr[i] <= maxFromEnd[mid]) {
                    ans = Math.max(ans, mid);
                    low = mid + 1;
                } else {
                    high = mid - 1;
                }
            }
            result = Math.max(result, ans - i);
        }
        System.out.println(result);
    }

    // ================= VERSION 3 ======================
    //Time complexity : O(N*log(N))
    private static void maxIndexDiffV3(List<Integer> arr, int n) {
        Map<Integer, List<Integer>> hasMap = new HashMap<>();

        for (int i = 0; i < n; i++) {
            if (hasMap.containsKey(arr.get(i))) {
                hasMap.get(arr.get(i)).add(i);
            } else {
                hasMap.put(arr.get(i), new ArrayList<>());
                hasMap.get(arr.get(i)).add(i);
            }
        }

        Collections.sort(arr);
        int maxDiff = Integer.MIN_VALUE;
        int temp = n;

        for (int i = 0; i < n; i++) {
            if (temp > hasMap.get(arr.get(i)).get(0)) {
                temp = hasMap.get(arr.get(i)).get(0);
            }
            maxDiff = Math.max(maxDiff, hasMap.get(arr.get(i)).get(
                    hasMap.get(arr.get(i)).size() - 1) - temp);
        }
        System.out.println(maxDiff);
    }

    // ====================== VERSION 4 ===================
    //Time Complexity: O(n)
    //Auxiliary Space: O(n)
    private static void maxIndexDiffV4(int[] arr, int n) {
        int maxDiff;
        int[] Rmax = new int[n];
        int[] Lmin = new int[n];

        Lmin[0] = arr[0];
        for (int i = 1; i < n; i++) {
            Lmin[i] = Math.min(arr[i], Lmin[i - 1]);
        }

        Rmax[n - 1] = arr[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            Rmax[i] = Math.max(arr[i], Rmax[i + 1]);
        }

        maxDiff = -1;
        int i = 0;
        int j = 0;

        while (j < n && i < n) {
            if (Lmin[i] <= Rmax[j]) {
                maxDiff = Math.max(maxDiff, j - i);
                j = j + 1;
            } else
                i = i + 1;
        }
        System.out.println(maxDiff);
    }

    public static void main(String[] args) {
        int[] arr = {9, 2, 3, 4, 5, 6, 7, 8, 18, 0};
        int n = arr.length;
        //System.out.println(maxIndexDiff(arr, n));
        //maxIndexDiffV2(arr, n);
        //=============================================================
        // Integer[] integers = new Integer[]{9, 2, 3, 4, 5, 6, 7, 8, 18, 0};
        // maxIndexDiffV3(Arrays.asList(integers), n);
        maxIndexDiffV4(arr, n);
    }
}

package com.company.algorithm.array.order_statistic;

//Next Greater Element
public class OrderStatistic45 {

    private static void findNextGreaterElement(int[] arr, int n) {
        int nextGreater;
        int i = 0;
        for (i = 0; i < n; i++) {
            nextGreater = -1;
            for (int j = i + 1; j < n; j++) {
                if (arr[j] > arr[i]) {
                    nextGreater = arr[j];
                    break;
                }
            }
            System.out.println("Element: " + arr[i] + " NGE: " + nextGreater);
        }
    }

    public static void main(String[] args) {
        int[] arr = {13, 7, 6, 12};
        int n = arr.length;
        findNextGreaterElement(arr, n);
    }
}

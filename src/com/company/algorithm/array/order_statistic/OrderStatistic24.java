package com.company.algorithm.array.order_statistic;

//Maximum difference between two elements such that larger element appears after the smaller number
public class OrderStatistic24 {

    // VERSION 1
    //Time Complexity : O(n^2)
    //Auxiliary Space : O(1)
    private static int maxDiff(int[] arr, int n) {
        int maxDiff = arr[1] = arr[0];
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[j] - arr[i] > maxDiff) {
                    maxDiff = arr[j] - arr[i];
                }
            }
        }
        return maxDiff;
    }

    //=================================================
    // VERSION 2
    //Time Complexity : O(n)
    //Auxiliary Space : O(1)
    private static int maxDiffV2(int[] arr, int n) {
        int maxDiff = arr[1] - arr[0];
        int minElement = arr[0];

        for (int i = 1; i < n; i++) {
            if (arr[i] - minElement > maxDiff)
                maxDiff = arr[i] - minElement;
            if (arr[i] < minElement)
                minElement = arr[i];
        }
        return maxDiff;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 90, 10, 110};
        int n = arr.length;

        // System.out.println(maxDiff(arr,n));
        System.out.println(maxDiffV2(arr, n));
    }
}

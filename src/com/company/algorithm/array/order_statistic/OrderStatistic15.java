package com.company.algorithm.array.order_statistic;

//k smallest elements in same order using O(1) extra space
public class OrderStatistic15 {

    private static void findMinimumKTime(int[] arr, int n, int k) {
        for (int i = k; i < n; i++) {
            int maxVar = arr[k - 1];
            int pos = k - 1;
            for (int j = k - 2; j >= 0; j--) {
                if(arr[j] > maxVar){
                    maxVar = arr[j];
                    pos = j;
                }
            }

            if(maxVar > arr[i]){
                int j = pos;
                while (j < k - 1){
                    arr[j] = arr[j+1];
                    j++;
                }
                arr[k-1] = arr[i];
            }
        }

        for (int i = 0; i < k; i++) {
            System.out.println(arr[i] + " ");
        }
    }

    public static void main(String[] args) {
        int[] arr = {4, 2, 6, 1, 5, 7};
        int k = 3; // yalniz k 3 ucun kecerlidi
        findMinimumKTime(arr, arr.length, k);
    }
}

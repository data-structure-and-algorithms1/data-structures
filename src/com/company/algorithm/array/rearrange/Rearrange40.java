package com.company.algorithm.array.rearrange;


import java.util.Arrays;

//Convert array into Zig-Zag fashion
public class Rearrange40 {

    private static void zigzag(int[] arr, int n) {

        boolean flag = true;
        for (int i = 0; i < n - 2; i++) {
            if (flag) {
                if (arr[i] > arr[i + 1]) {
                    int temp = arr[i + 1];
                    arr[i + 1] = arr[i];
                    arr[i] = temp;
                }
            } else {
                if (arr[i] < arr[i + 1]) {
                    int temp = arr[i + 1];
                    arr[i + 1] = arr[i];
                    arr[i] = temp;
                }
            }
            flag = !flag;
        }
    }

    public static void main(String[] args) {
        int[] arr = {4, 3, 7, 8, 6, 2, 1};
        int n = arr.length;
        zigzag(arr,n);
        System.out.println(Arrays.toString(arr));
    }
}

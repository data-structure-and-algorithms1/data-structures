package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Rearrange an array in order – smallest, largest, 2nd smallest, 2nd largest, ..
public class Rearrange11 {

    //Time Complexity : O(n Log n)
    //Auxiliary Space : O(n)
    private static void orderArray(int[] arr, int n) {
        Arrays.sort(arr);
        int[] tempArr = new int[n];

        int arrIndex = 0;

        for (int i = 0, j = n - 1; i <= n / 2; i++, j--) {

            if(arrIndex < n){
                tempArr[arrIndex] = arr[i];
                arrIndex++;
            }
            if(arrIndex < n){
                tempArr[arrIndex] = arr[j];
                arrIndex++;
            }
        }
        System.arraycopy(tempArr, 0, arr, 0, n);
    }

    public static void main(String[] args) {
        int[] arr = {5, 8, 1, 4, 2, 9, 3, 7, 6};
        orderArray(arr, arr.length);
        System.out.println(Arrays.toString(arr));
    }
}

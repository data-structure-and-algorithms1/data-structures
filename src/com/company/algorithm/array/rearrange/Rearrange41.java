package com.company.algorithm.array.rearrange;


import java.util.ArrayList;
import java.util.List;

//Form minimum number from given sequence
public class Rearrange41 {

    // ======================= VERSION 1 ========================
    private static void printMinNumberForPattern(String arr) {
        int currMax = 0;
        int lastEntry = 0;
        int j;

        for (int i = 0; i < arr.length(); i++) {
            int noOfNextD = 0;

            switch (arr.charAt(i)) {
                case 'I':
                    j = i + 1;
                    while (j < arr.length() && arr.charAt(j) == 'D') {
                        noOfNextD++;
                        j++;
                    }

                    if (i == 0) {
                        currMax = noOfNextD + 2;
                        System.out.print(" " + ++lastEntry);
                        System.out.print(" " + ++currMax);

                        lastEntry = currMax;
                    } else {
                        currMax = currMax + noOfNextD + 1;
                        lastEntry = currMax;
                        System.out.print(" " + lastEntry);
                    }

                    for (int k = 0; k < noOfNextD; k++) {
                        System.out.print(" " + --lastEntry);
                        i++;
                    }
                    break;

                case 'D':
                    if (i == 0) {
                        j = i + 1;
                        while (j < arr.length() && arr.charAt(j) == 'D') {
                            noOfNextD++;
                            j++;
                        }

                        currMax = noOfNextD + 2;
                        System.out.print(" " + currMax + " " + (currMax - 1));
                        lastEntry = currMax - 1;
                    } else {
                        System.out.print(" " + (lastEntry - 1));
                        lastEntry--;
                    }
                    break;
            }
        }
        System.out.println();
    }

    // ============================== VERSION 2 =========================
    private static void printMinNumberForPatternV2(String arr) {
        int minAvail = 1;
        int posOfI = 0;

        List<Integer> list = new ArrayList<>();

        if (arr.charAt(0) == 'I') {
            list.add(1);
            list.add(2);
            minAvail = 3;
            posOfI = 1;
        } else {
            list.add(2);
            list.add(1);
            minAvail = 3;
        }

        for (int i = 1; i < arr.length(); i++) {
            if (arr.charAt(i) == 'I') {
                list.add(minAvail);
                minAvail++;
                posOfI = i + 1;
            } else {
                list.add(list.get(i));
                for (int j = posOfI; j <= i; j++) {
                    list.set(j, list.get(j) + 1);
                }
                minAvail++;
            }
        }

        for (Integer integer : list) {
            System.out.print(integer + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
    //    printMinNumberForPattern("IIIDDIDIID");
//        printMinNumberForPattern("I");
//        printMinNumberForPattern("DD");
//        printMinNumberForPattern("II");
//        printMinNumberForPattern("IIDDD");
//        printMinNumberForPattern("DDDIID");

        //========================================
        printMinNumberForPatternV2("IDID");
        printMinNumberForPatternV2("I");
        printMinNumberForPatternV2("D");
        printMinNumberForPatternV2("II");
        printMinNumberForPatternV2("DD");
        printMinNumberForPatternV2("DIDI");
        printMinNumberForPatternV2("IIDDD");

    }
}

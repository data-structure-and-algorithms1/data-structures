package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Rearrange an array such that ‘arr[j]’ becomes ‘i’ if ‘arr[i]’ is ‘j’ | Set 1
public class Rearrange15 {

    private static void rearrange(int[] arr, int n) {
        int[] temp = new int[n];
        int i;

        for (i = 0; i < n; i++) {
            temp[arr[i]] = i;
        }

        for (i = 0; i < n; i++) {
            arr[i] = temp[i];
        }
    }

    // ====================== VERSION 2 ==========================

    private static void rearrangeV2(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            arr[arr[i] % n] += i * n;
        }

        for (int i = 0; i < n; i++) {
            arr[i] /= n;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 0, 2};
        int n = arr.length;
        //rearrange(arr, n);
        rearrangeV2(arr, n);
        System.out.println(Arrays.toString(arr));
    }
}

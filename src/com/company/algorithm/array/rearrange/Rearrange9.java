package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Rearrange positive and negative numbers using inbuilt sort function
public class Rearrange9 {

    private static void rearrangePosAndNeg(int[] arr, int n) {
        int neg = 0;
        int pos = 0;
        for (int i = 0; i < n; i++) {
            if (arr[i] < 0)
                neg++;
            else
                pos++;
        }

        int[] negative = new int[neg];
        int[] positive = new int[pos];

        for (int i = 0, j = 0, t = 0; i < n; i++) {
            if (arr[i] < 0) {
                negative[j] = arr[i];
                j++;
            } else {
                positive[t] = arr[i];
                t++;
            }
        }

        System.arraycopy(negative, 0, arr, 0, neg);
        for (int i = 0; i < pos; i++) {
            arr[pos + i + 1] = positive[i];
        }
    }

    // ===================== VERSION 2 ==========================
    private static int[] rearrangePosAndNegV2(int[] arr, int n) {
        int[] temp = new int[n];

        int i, t;
        for (i = 0, t = 0; i < n; i++) {
            if (arr[i] < 0) {
                temp[t] = arr[i];
                t++;
            }
        }

        for (int j = 0; j < n; j++) {
            if (arr[j] >= 0 && t != n) {
                temp[t] = arr[j];
                t++;
            }
        }
        return temp;
    }

    // ====================== VERSION 3 =============================

    private static void reverse(int[] arr, int start, int end) {
        int temp;
        while (start < end) {
            temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }

    private static void rearrange(int[] arr, int start, int end) {
        if (start == end) return;

        rearrange(arr, start + 1, end);
        if (arr[start] >= 0) {
            reverse(arr, start + 1, end);
            reverse(arr, start, end);
        }
    }
    //=====================================================


    public static void main(String[] args) {
        int arr[] = {12, 11, -13, -5, 6, -7, 5, -3, -6};
        //rearrangePosAndNeg(arr, arr.length);
//        arr = rearrangePosAndNegV2(arr, arr.length);
//        System.out.println(Arrays.toString(arr));
//
        // ======================================

        int[] array = {12, 11, -13, -5, 6, -7, 5, -3, -6};
        int length = array.length;
//        int countNegative = 0;
//        for (int j : array) {
//            if (j < 0) {
//                countNegative++;
//            }
//        }

        System.out.println("array: ");
        System.out.println(Arrays.toString(array));
        rearrange(array,0,length-1);
      //  reverse(array,countNegative,length-1);
        System.out.println("Rearranged array: ");
        System.out.println(Arrays.toString(array));
    }
}

package com.company.algorithm.array.rearrange;

import java.util.Vector;

//Arrange given numbers to form the biggest number | Set 1
public class Rearrange14 {

    private static void printLargest(Vector<String> arr) {
        arr.sort((o1, o2) -> {
            String XY = o1 + o2;
            String YX = o2 + o1;
            return XY.compareTo(YX) > 0 ? -1 : 1;
        });
        for (String s : arr) {
            System.out.print(s);
        }
    }

    public static void main(String[] args) {
        Vector<String> arr = new Vector<>();
        arr.add("54");
        arr.add("546");
        arr.add("548");
        arr.add("60");
        printLargest(arr);
    }

}

package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Write a program to reverse an array or string
public class Rearrange2 {

    // ============== VERSION 1 ======================
    private static void reverseArray(int[] arr, int n) {
        for (int i = 0; i < n / 2; i++) {
            int temp = arr[i];
            arr[i] = arr[n - 1 - i];
            arr[n - 1 - i] = temp;
        }
    }

    // ================= VERSION 2 ========================
    private static void reverseArrayV2(int[] arr, int start, int end) {
        int temp;
        while (start < end) {
            temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};

        //reverseArray(arr, arr.length);
        reverseArrayV2(arr, 0, arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }
}

package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Move all negative numbers to beginning and positive to end with constant extra space
public class Rearrange18 {

    // with bubble sort
    private static void rearrange(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n - i - 1; j++) {
                if (arr[j + 1] < arr[j]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    //=================================================

    private static void rearrangeV2(int[] arr, int n) {
        int j = 0;
        int temp = 0;
        for (int i = 0; i < n; i++) {
            if (arr[i] < 0) {
                if (i != j) {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
                j++;
            }
        }
    }

    // ====================================================================
    private static void rearrangeV3(int[] arr, int left, int right) {
        while (left <= right) {
            if (arr[left] < 0 && arr[right] < 0)
                left++;

            else if (arr[left] > 0 && arr[right] < 0) {
                int temp = arr[left];
                arr[left] = arr[right];
                arr[right] = temp;
                left++;
                right--;
            } else if (arr[left] > 0 && arr[right] > 0)
                right--;
            else {
                left++;
                right--;
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {-12, 11, -13, -5, 6, -7, 5, -3, -6};
        //rearrange(arr, arr.length);
        //rearrangeV2(arr, arr.length);
        rearrangeV3(arr, 0, arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }
}

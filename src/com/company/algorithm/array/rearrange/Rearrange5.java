package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Rearrange array in alternating positive & negative items with O(1) extra space | Set 1
public class Rearrange5 {

    // =========================== VERSION 1 ======================================
    private static void rightRotate(int[] arr, int n, int outOfPlace, int cur) {
        int tmp = arr[cur];
        for (int i = cur; i > outOfPlace; i--) {
            arr[i] = arr[i - 1];
        }
        arr[outOfPlace] = tmp;
    }

    private static void rearrange(int[] arr, int n) {
        int outOfPlace = -1;

        for (int index = 0; index < n; index++) {
            if (outOfPlace >= 0) {
                if (((arr[index] >= 0) && (arr[outOfPlace] < 0))
                        || ((arr[index] < 0) && (arr[outOfPlace] >= 0))) {
                    rightRotate(arr, n, outOfPlace, index);

                    if (index - outOfPlace >= 2)
                        outOfPlace = outOfPlace + 2;
                    else
                        outOfPlace = -1;
                }
            }
            if (outOfPlace == -1) {
                if (((arr[index] >= 0) && ((index & 0x01) == 0))
                        || ((arr[index] < 0) && (index & 0x01) == 1)) {
                    outOfPlace = index;
                }
            }
        }
    }

    // ======================================================================

    // ========================== VERSION 2 =================================

    //Time Complexity: O(N*logN)l
    //Space Complexity: O(1)
    // function which works in the condition when number of
    // negative numbers are lesser or equal than positive
    // numbers
    private static void fill1(int[] arr, int neg, int pos) {
        if (neg % 2 == 1) {
            for (int i = 1; i < neg; i += 2) {
                int c = arr[i];
                int d = arr[i + neg];
                int temp = c;
                arr[i] = d;
                arr[i + neg] = temp;
            }
        } else {
            for (int i = 1; i <= neg; i += 2) {
                int c = arr[i];
                int d = arr[i + neg - 1];
                int temp = c;
                arr[i] = d;
                arr[i + neg - 1] = temp;
            }
        }
    }

    // function which works in the condition when number of
    // negative numbers are greater than positive numbers
    private static void fill2(int[] arr, int neg, int pos) {
        if (pos % 2 == 1) {
            for (int i = 1; i < pos; i += 2) {
                int c = arr[i];
                int d = arr[i + pos];
                int temp = c;
                arr[i] = d;
                arr[i + pos] = temp;
            }
        } else {
            for (int i = 1; i <= pos; i += 2) {
                int c = arr[i];
                int d = arr[i + pos - 1];
                int temp = c;
                arr[i] = d;
                arr[i + pos - 1] = temp;
            }
        }
    }

    private static void reverse(int[] arr, int n) {
        int i, t;
        for (i = 0; i < n / 2; i++) {
            t = arr[i];
            arr[i] = arr[n - i - 1];
            arr[n - i - 1] = t;
        }
    }

    public static void main(String[] args) {
//        int[] arr = {-5, -2, 5, 2, 4, 7, 1, 8, 0, -8};
//        int n = arr.length;
//        rearrange(arr, n);
//        System.out.println(Arrays.toString(arr));

        int[] arr = {-5, -2, 5, -2, 4, 7, 1, 8, 0, -8};
        int n = arr.length;
        int neg = 0, pos = 0;
        for (int j : arr) {
            if (j < 0) neg++;
            else pos++;
        }
        Arrays.sort(arr);
        if (neg <= pos) {
            fill1(arr, neg, pos);
        } else {
            reverse(arr, n);
            fill2(arr, neg, pos);
        }

        System.out.println(Arrays.toString(arr));
    }
}

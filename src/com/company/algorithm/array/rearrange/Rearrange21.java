package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Positive elements at even and negative at odd positions (Relative order not maintained)
public class Rearrange21 {

    private static void rearrange(int[] arr, int n) {
        int positive = 0;
        int negative = 1;
        int temp;

        while (true) {
            while (positive < n && arr[positive] >= 0)
                positive += 2;

            while (negative < n && arr[negative] <= 0)
                negative += 2;

            if (positive < n && negative < n) {
                temp = arr[positive];
                arr[positive] = arr[negative];
                arr[negative] = temp;
            }
            else break;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, -3, 5, 6, -3, 6, 7, -4, 9, 10};
        int n = arr.length;

        rearrange(arr,n);
        System.out.println(Arrays.toString(arr));
    }
}

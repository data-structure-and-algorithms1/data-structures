package com.company.algorithm.array.rearrange;

//Find a sorted subsequence of size 3 in linear time
public class Rearrange27 {
    static int min;
    static int max;

    private static void tripleRearrange(int[] arr, int n) {
        max = n - 1;
        min = 0;
        int[] smaller = smallerSide(arr, n);
        int[] greater = greaterSide(arr, n);

        for (int i = 0; i < n; i++) {
            if (smaller[i] != -1 && greater[i] != -1) {
                System.out.println(arr[smaller[i]] + " " + arr[i] + " " + arr[greater[i]]);
                return;
            }
        }

        System.out.println("No Such Triplet Found");
        return;
    }

    private static int[] smallerSide(int[] arr, int n) {
        int[] smaller = new int[n];
        smaller[0] = -1;
        for (int i = 1; i < n; i++) {
            if (arr[i] <= arr[min]) {
                min = i;
                smaller[i] = -1;
            } else
                smaller[i] = min;
        }
        return smaller;
    }

    private static int[] greaterSide(int[] arr, int n) {
        int[] greater = new int[n];
        greater[n - 1] = -1;
        for (int i = n - 2; i >= 0; i--) {
            if (arr[i] > arr[max]) {
                max = i;
                greater[i] = -1;
            } else greater[i] = max;
        }
        return greater;
    }

    public static void main(String[] args) {
        int[] arr = {12, 11, 10, 5, 6, 2, 30};
        tripleRearrange(arr, arr.length);
    }
}

package com.company.algorithm.array.rearrange;

//Maximum Product Subarray
public class Rearrange29 {

    //Time Complexity: O(N^2)
    //Auxiliary Space: O(1)
    private static int maxSubarrayProduct(int[] arr) {
        int result = arr[0];
        int n = arr.length;

        for (int i = 0; i < n; i++) {
            int mul = arr[i];
            for (int j = i + 1; j < n; j++) {
                result = Math.max(result, mul);
                mul *= arr[j];
            }
            result = Math.max(result, mul);
        }
        return result;
    }

    // ==================== VERSION 2 =====================

    //Time Complexity: O(n)
    //Auxiliary Space: O(1)
    private static int maxSubarrayProductV2(int[] arr) {
        int maxEndingHere = 1;
        int minEndingHere = 1;
        int maxSoFar = 0;
        int flag = 0;

        for (int j : arr) {

            // if is positive
            if (j > 0) {
                maxEndingHere = maxEndingHere * j;
                minEndingHere = Math.min(minEndingHere * j, 1);
                flag = 1;
            }
            // if element is zero
            else if (j == 0) {
                maxEndingHere = 1;
                minEndingHere = 1;
            }
            // if element is negative
            else {
                int temp = maxEndingHere;
                maxEndingHere = Math.max(minEndingHere * j, 1);
                minEndingHere = temp * j;
            }

            if (maxSoFar < maxEndingHere)
                maxSoFar = maxEndingHere;
        }

        if (flag == 0 && maxSoFar == 0)
            return 0;
        return maxSoFar;
    }

    public static void main(String[] args) {
        int[] arr = {1, -2, -3, 0, 7, -8, -2};
       // System.out.println(maxSubarrayProduct(arr));
        System.out.println(maxSubarrayProductV2(arr));
    }
}

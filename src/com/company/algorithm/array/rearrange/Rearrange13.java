package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Reorder an array according to given indexes
public class Rearrange13 {

    // ==================== VERSION 1 ========================
    private static void rearrange(int[] arr, int[] index, int n) {
        int[] temp = new int[n];
        for (int i = 0; i < n; i++) {
            temp[index[i]] = arr[i];
        }

        for (int i = 0; i < n; i++) {
            arr[i] = temp[i];
            index[i] = i;
        }
    }
    public static void main(String[] args) {
        int[] arr = {10, 11, 12};
        int[] index = {1, 0, 2};
        rearrange(arr,index,arr.length);
        System.out.println(Arrays.toString(arr));
    }

}

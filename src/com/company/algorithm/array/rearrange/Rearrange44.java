package com.company.algorithm.array.rearrange;


import java.util.HashMap;

public class Rearrange44 {

    private static void distinctAdjacentElement(int[] arr, int n) {
        HashMap<Integer, Integer> m = new HashMap<>();

        for (int i = 0; i < n; i++) {
            if (m.containsKey(arr[i])) {
                int x = m.get(arr[i]) + 1;
                m.put(arr[i], x);
            } else {
                m.put(arr[i], 1);
            }
        }

        int mx = 0;

        for (int i = 0; i < n; i++)
            if (mx < m.get(arr[i]))
                mx = m.get(arr[i]);

        if (mx > (n + 1) / 2)
            System.out.println("No");
        else
            System.out.println("Yes");

    }
    //

    public static void main(String[] args) {
        int[] arr = {1, 1, 1, 2};
        int n = arr.length;
        distinctAdjacentElement(arr, arr.length);
    }
}

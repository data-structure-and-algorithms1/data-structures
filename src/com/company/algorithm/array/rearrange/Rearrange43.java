package com.company.algorithm.array.rearrange;

//Rearrange a binary string as alternate x and y occurrences
public class Rearrange43 {

    private static void rearrangeBinary(String binary) {
        int n = binary.length();
        int countOfZeros = 0;
        int countOfOne = 0;
        for (int i = 0; i < n; i++) {
            if (binary.charAt(i) == '0') {
                countOfZeros++;
            } else {
                countOfOne++;
            }
        }

        if (countOfZeros == 0) return;
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < n; i++) {
            sb.append(0);
            countOfZeros--;
            sb.append(1);
            countOfOne--;
            if (countOfOne == 0 || countOfZeros == 0)
                break;
        }

        sb.append("1".repeat(Math.max(0, countOfOne)));

        System.out.println(sb);
    }

    public static void main(String[] args) {
        String binary = "0011011011";
        rearrangeBinary(binary);
    }
}

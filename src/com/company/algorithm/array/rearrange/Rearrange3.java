package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Rearrange array such that arr[i] >= arr[j] if i is even and arr[i]<=arr[j] if i is odd and j < i
public class Rearrange3 {

    private static void rearrange(int[] arr, int n) {
        int evenPos = n / 2;
        int oddPos = n - evenPos;

        int[] tempArr = new int[n];

        System.arraycopy(arr, 0, tempArr, 0, n);
        Arrays.sort(tempArr);

        int j = oddPos - 1;
        for (int i = 0; i < n; i += 2) {
            arr[i] = tempArr[j];
            j--;
        }

        j = oddPos;

        for (int i = 1; i < n ; i+=2) {
            arr[i] = tempArr[j];
            j++;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        rearrange(arr,arr.length);
        System.out.println(Arrays.toString(arr));
    }
}

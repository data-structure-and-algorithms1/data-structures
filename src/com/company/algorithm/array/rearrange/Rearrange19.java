package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Move all negative elements to end in order with extra space allowed
public class Rearrange19 {

    private static void rearrange(int[] arr, int n) {
        int[] temp = new int[n];
        int j = 0;
        for (int i = 0; i < n; i++) {
            if (arr[i] >= 0)
                temp[j++] = arr[i];
        }

        if (j == n || j == 0) return;

        for (int i = 0; i < n; i++) {
            if (arr[i] < 0)
                temp[j++] = arr[i];
        }
        System.arraycopy(temp, 0, arr, 0, n);
    }

    public static void main(String[] args) {
        int arr[] = {1, -1, 3, 2, -7, -5, 11, 6};
        int n = arr.length;
        rearrange(arr,n);
        System.out.println(Arrays.toString(arr));
    }
}

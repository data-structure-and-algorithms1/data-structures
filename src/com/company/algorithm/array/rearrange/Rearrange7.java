package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Move all zeroes to end of array | Set-2 (Using single traversal)
public class Rearrange7 {

    private static void moveAllZeros(int[] arr, int n) {
        int count = 0;
        int temp;
        for (int i = 0; i < n; i++) {
            if (arr[i] != 0) {
                temp = arr[count];
                arr[count] = arr[i];
                arr[i] = temp;
                count++;
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 0, 0, 0, 3, 6};
        int n = arr.length;
        moveAllZeros(arr, n);
        System.out.println(Arrays.toString(arr));
    }
}

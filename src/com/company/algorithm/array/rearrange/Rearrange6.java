package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Move all zeroes to end of array
public class Rearrange6 {

    private static void moveZeroToEnd(int[] arr, int n) {

        int count = 0;
        for (int i = 0, j = 0; i < n; i++) {
            if (arr[i] == 0) {
                count++;
            } else {
                arr[j] = arr[i];
                j++;
            }
        }

        for (int i = n - count; i < n; i++) {
            arr[i] = 0;
        }
    }

    // ==================== VERSION 2 ============================
    private static void moveZeroToEndV2(int[] arr, int n) {
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (arr[i] != 0) {
                arr[count++] = arr[i];
            }
        }

        while (count < n) {
            arr[count++] = 0;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 0, 4, 3, 0, 5, 0};
        int n = arr.length;
       // moveZeroToEnd(arr, n);
        moveZeroToEndV2(arr,n);
        System.out.println(Arrays.toString(arr));
    }
}

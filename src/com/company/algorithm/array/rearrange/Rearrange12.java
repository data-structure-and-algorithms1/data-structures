package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Double the first element and move zero to end
public class Rearrange12 {

    private static int[] rearrange(int[] arr, int n) {
        int[] temp = new int[n];
        for (int i = 0; i < n - 1; i++) {
            if (arr[i] == arr[i + 1] && arr[i] != 0) {
                arr[i] = arr[i] + arr[i + 1];
                arr[i + 1] = 0;
            }
        }

        for (int i = 0, j = 0; i < n; i++) {
            if (arr[i] != 0) {
                temp[j] = arr[i];
                j++;
            }
        }
        return temp;
    }

    // ================== VERSION 2 ======================
    private static int[] rearrangeV2(int[] arr, int n) {
        int[] temp = new int[n];
        int j = 0;
        for (int i = 0; i < n - 1; i++) {
            if (arr[i] == arr[i + 1] && arr[i] != 0) {
                arr[i] = arr[i] + arr[i + 1];
                arr[i + 1] = 0;
            }
            if (arr[i] != 0) {
                temp[j] = arr[i];
                j++;
            }
        }
        if (arr[n - 1] != 0) {
            temp[j] = arr[n - 1];
        }

        return temp;
    }

    // ====================== VERSION 3 =========================
    private static void pushZerosToEnd(int[] arr, int n) {
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (arr[i] != 0)
                arr[count++] = arr[i];
        }

        while (count < n)
            arr[count++] = 0;
    }

    private static void modifyAndRearrangeArr(int[] arr, int n) {
        if (n == 1) return;

        for (int i = 0; i < n - 1; i++) {
            if ((arr[i] != 0) && (arr[i] == arr[i + 1])) {
                arr[i] = 2 * arr[i];
                arr[i + 1] = 0;
                i++;
            }
        }
        pushZerosToEnd(arr, n);
    }


    public static void main(String[] args) {
        int[] arr = {0, 2, 2, 2, 0, 6, 6, 0, 0, 8};
        // arr = rearrange(arr, arr.length);
        //  arr = rearrangeV2(arr, arr.length);
        modifyAndRearrangeArr(arr, arr.length);
        System.out.println(Arrays.toString(arr));
    }

}

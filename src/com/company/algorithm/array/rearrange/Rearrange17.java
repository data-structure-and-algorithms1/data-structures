package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Rearrange an array in maximum minimum form | Set 2 (O(1) extra space)
public class Rearrange17 {

    private static void rearrange(int[] arr, int n) {
        int maxIdx = n - 1;
        int minIdx = 0;
        int maxElem = arr[n - 1] + 1;

        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                arr[i] += (arr[maxIdx] % maxElem) * maxElem;
                maxIdx--;
            } else {
                arr[i] += (arr[minIdx] % maxElem) * maxElem;
                minIdx++;
            }
        }

        for (int i = 0; i < n; i++) {
            arr[i] = arr[i] / maxElem;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        int n = arr.length;
        rearrange(arr,n);
        System.out.println(Arrays.toString(arr));
    }
}

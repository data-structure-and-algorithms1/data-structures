package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Rearrange array such that even positioned are greater than odd
public class Rearrange10 {

    //Time Complexity: O(n * log n)
    //Auxiliary Space: O(n)
    private static void rearrange(int[] arr, int n) {
        Arrays.sort(arr);
        int[] ans = new int[n];
        int p = 0, q = n - 1;
        for (int i = 0; i < n; i++) {
            if ((i + 1) % 2 == 0)
                ans[i] = arr[q--];
            else
                ans[i] = arr[p++];
        }
        System.out.println(Arrays.toString(ans));
    }

    // =================== VERSION 2 =====================
    //Time Complexity: O(n)
    //Auxiliary Space: O(1)
    private static void rearrangeV2(int[] arr, int n) {
        for (int i = 1; i < n; i++) {
            if (i % 2 == 0) {
                if (arr[i] > arr[i - 1]) {
                    int temp = arr[i];
                    arr[i] = arr[i - 1];
                    arr[i - 1] = temp;
                }
            }
            else {
                if(arr[i] < arr[i - 1]){
                    int temp = arr[i];
                    arr[i] = arr[i - 1];
                    arr[i - 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 2, 2, 5};
       // rearrange(arr, arr.length);
        rearrangeV2(arr,arr.length);
    }
}

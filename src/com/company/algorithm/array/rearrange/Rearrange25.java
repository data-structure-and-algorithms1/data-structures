package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Segregate 0s and 1s in an array
public class Rearrange25 {

    private static void segregate(int[] arr, int n) {
        int[] temp = new int[n];
        int j = 0;
        for (int i = 0; i < n; i++) {
            if (arr[i] == 0) {
                temp[j++] = 0;
            }
        }

        for (int i = 0; i < n; i++) {
            if (arr[i] == 1) {
                temp[j++] = 1;
            }
        }
        System.out.println(Arrays.toString(temp));
    }

    // =======================================================

    // ======================== VERSIOn 2 ===================

    private static void segregateV2(int[] arr, int n) {
        int countOfZeros = 0;
        for (int i = 0; i < n; i++)
            if (arr[i] == 0)
                countOfZeros++;

        for (int i = 0; i < n; i++) {
            if (i < countOfZeros) {
                arr[i] = 0;
            } else {
                arr[i] = 1;
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {0, 1, 0, 1, 0, 0, 1, 1, 1, 0};
        //segregate(arr, arr.length);
        segregateV2(arr,arr.length);
    }
}

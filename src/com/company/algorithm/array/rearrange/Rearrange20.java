package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Rearrange array such that even index elements are smaller and odd index elements are greater
public class Rearrange20 {

    private static void rearrange(int[] arr, int n) {
        for (int i = 0; i < n - 1; i++) {
            if (i % 2 == 0 && arr[i] > arr[i + 1]) {
                swap(arr, i, i + 1);
            } else if (i % 2 != 0 && arr[i] < arr[i + 1]) {
                swap(arr, i, i + 1);
            }
        }
    }

    private static void swap(int[] arr, int index1, int index2) {
        int temp = arr[index1];
        arr[index1] = arr[index2];
        arr[index1 + 1] = temp;
    }


    public static void main(String[] args) {
        int[] arr = { 6, 4, 2, 1, 8, 3 };
        int n = arr.length;
        rearrange(arr, n);
        System.out.println(Arrays.toString(arr));
    }
}

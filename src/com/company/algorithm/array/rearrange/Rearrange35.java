package com.company.algorithm.array.rearrange;

//Find Index of 0 to be replaced with 1 to get longest continuous sequence of 1s in a binary array
public class Rearrange35 {

    private static int maxOnesIndex(int[]arr, int n){
        int maxCount = 0;
        int maxIndex = 0;
        int prevZero = -1;
        int prevPrevZero = -1;

        for (int curr = 0; curr < n; curr++) {
            if(arr[curr] == 0){
                if(curr - prevPrevZero > maxCount){
                    maxCount = curr - prevPrevZero;
                    maxIndex = prevZero;
                }

                prevPrevZero = prevZero;
                prevZero = curr;
            }
        }

        if(n - prevPrevZero > maxCount)
            maxIndex = prevZero;

        return maxIndex;
    }

    public static void main(String[] args) {

        int[] arr = {1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1};
        int n = arr.length;
        System.out.println("Index of 0 to be replaced is "+
                maxOnesIndex(arr, n));
    }
}

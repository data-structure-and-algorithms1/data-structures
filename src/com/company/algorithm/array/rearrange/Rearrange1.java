package com.company.algorithm.array.rearrange;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

//Rearrange an array such that arr[i] = i
public class Rearrange1 {

    //Time Complexity: O(n2)
    private static void fixArray(int[] arr, int n) {
        int i, j, temp;
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if (arr[j] == i) {
                    temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                    break;
                }
            }
        }

        for (i = 0; i < n; i++) {
            if (arr[i] != i) {
                arr[i] = -1;
            }
        }
    }

    // ============================ VERSION 2 ===============================
    // Time Complexity: O(n)
    private static void fixArrayV2(int[] arr, int n) {
        for (int i = 0; i < n; ) {
            if (arr[i] >= 0 && arr[i] != i) {
                int ele = arr[arr[i]];
                arr[arr[i]] = arr[i];
                arr[i] = ele;
            } else {
                i++;
            }
        }
    }

    // =============================== VERSION 3 ===============================
    private static void fixArrayV3(int[] arr, int n) {
        Set<Integer> set = new HashSet<>();
        for (int j : arr) {
            set.add(j);
        }

        for (int i = 0; i < arr.length; i++) {
            if(set.contains(i))
                arr[i] = i;
            else
                arr[i] = -1;
        }
    }


    public static void main(String[] args) {
        int[] arr = {-1, -1, 6, 1, 9,
                3, 2, -1, 4, -1};
        int n = arr.length;

        //fixArray(arr, n);
       // fixArrayV2(arr, n);
        fixArrayV3(arr,n);
        System.out.println(Arrays.toString(arr));
    }
}

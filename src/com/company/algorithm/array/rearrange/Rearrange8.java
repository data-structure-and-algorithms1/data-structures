package com.company.algorithm.array.rearrange;

//Minimum swaps required to bring all elements less than or equal to k together
public class Rearrange8 {

    private static int minimumSwaps(int[] arr, int n, int k) {
        int count = 0;
        // Find count of elements which are
        // less than equals to k
        for (int i = 0; i < n; i++) {
            if (arr[i] <= k)
                count++;
        }

        // Find unwanted elements in current
        // window of size 'count'
        int bad = 0;
        for (int i = 0; i < count; i++) {
            if (arr[i] > k)
                ++bad;
        }

        // Initialize answer with 'bad' value of
        // current window
        int ans = bad;
        for (int i = 0, j = count; j < n; i++, j++) {
            if (arr[i] > k) {
                --bad;
            }

            if (arr[j] > k)
                ++bad;

            ans = Math.min(ans, bad);
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] arr = {2, 1, 5, 6, 3};
        int n = arr.length;
        int k = 3;
        System.out.println(minimumSwaps(arr,n,k));
        System.out.println("=".repeat(30));


        int[] arr2 = {2, 7, 9, 5, 8, 7, 4};
        int n2 = arr2.length;
        int k2 = 5;
        System.out.println(minimumSwaps(arr2,n2,k2));

    }
}

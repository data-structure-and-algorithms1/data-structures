package com.company.algorithm.array.rearrange;

//Replace two consecutive equal values with one greater
public class Rearrange42 {

    private static void replaceConsecutiveValues(int[] arr, int n) {
        int pos = 0;

        for (int i = 0; i < n; i++) {
            arr[pos++] = arr[i];
            while (pos > 1 && arr[pos - 2] == arr[pos - 1]) {
                pos--;
                arr[pos - 1]++;
            }
        }

        for (int i = 0; i < pos; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    public static void main(String[] args) {
        int[] arr = {5, 2, 1, 1, 2, 2};
        replaceConsecutiveValues(arr, arr.length);
    }
}

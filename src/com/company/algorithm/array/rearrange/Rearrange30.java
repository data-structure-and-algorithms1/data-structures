package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Replace every element with the greatest element on right side
public class Rearrange30 {

    private static void nextGreatest(int[] arr) {
        int size = arr.length;
        int maxFromRight = arr[size-1];

        arr[size-1] = -1;

        for (int i = size-2; i >= 0 ; i--) {
            int temp = arr[i];
            arr[i] = maxFromRight;

            if(maxFromRight < temp)
                maxFromRight = temp;
        }
    }

    public static void main(String[] args) {
        int[] arr = {16, 17, 4, 3, 5, 2};
        nextGreatest(arr);
        System.out.println(Arrays.toString(arr));
    }
}

package com.company.algorithm.array.rearrange;

//Minimum number of swaps required for arranging pairs adjacent to each other
public class Rearrange39 {

    private static void updateIndex(int[] index, int a, int ai, int b, int bi) {
        index[a] = ai;
        index[b] = bi;
    }

    private static int minSwapUtil(int[] arr, int[] pairs, int[] index, int i, int n) {
        if (i > n) return 0;

        if (pairs[arr[i]] == arr[i + 1])
            return minSwapUtil(arr, pairs, index, i + 2, n);

        int one = arr[i + 1];
        int indexTwo = i + 1;
        int indexOne = index[pairs[arr[i]]];
        int two = arr[index[pairs[arr[i]]]];
        arr[i + 1] = arr[i + 1] ^ arr[indexOne] ^ (arr[indexOne] = arr[i + 1]);
        updateIndex(index, one, indexTwo, two, indexOne);
        int a = minSwapUtil(arr, pairs, index, i + 2, n);
        arr[i + 1] = arr[i + 1] ^ arr[indexOne] ^ (arr[indexOne] = arr[i + 1]);
        updateIndex(index, one, indexTwo, two, indexOne);
        one = arr[i];
        indexOne = index[pairs[arr[i + 1]]];

        two = arr[index[pairs[arr[i + 1]]]];
        indexTwo = i;
        arr[i] = arr[i] ^ arr[indexOne] ^ (arr[indexOne] = arr[i]);
        updateIndex(index, one, indexOne, two, indexTwo);
        int b = minSwapUtil(arr, pairs, index, i + 2, n);

        arr[i] = arr[i] ^ arr[indexOne] ^ (arr[indexOne] = arr[i]);
        updateIndex(index, one, indexTwo, two, indexOne);

        return 1 + Math.min(a, b);
    }

    private static int minSwaps(int n, int[] pairs, int[] arr) {
        int index[] = new int[2 * n + 1];
        for (int i = 1; i <= 2 * n; i++) {
            index[arr[i]] = i;
        }
        return minSwapUtil(arr, pairs, index, 1, 2 * n);
    }

    public static void main(String[] args) {

        int[] arr = {0, 3, 5, 6, 4, 1, 2};
        int[] pairs = {0, 3, 6, 1, 5, 4, 2};
        int m = pairs.length;
        int n = m / 2;

        System.out.println(minSwaps(n, pairs, arr));
    }

}

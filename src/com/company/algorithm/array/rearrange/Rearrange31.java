package com.company.algorithm.array.rearrange;

//Maximum circular subarray sum
public class Rearrange31 {

    private static int kadane(int[] arr, int n) {
        int res = 0;
        int x = arr[0];
        for (int i = 0; i < n; i++) {
            res = Math.min(arr[i], res + arr[i]);
            x = Math.max(x, res);
        }
        return x;
    }

    private static int reverseKadane(int[] arr, int n) {
        int total = 0;
        for (int i = 0; i < n; i++) {
            total += arr[i];
        }

        for (int i = 0; i < n; i++) {
            arr[i] = -arr[i];
        }

        int k = kadane(arr, n);
        int res = total + k;
        if (total == -k)
            return total;
        else
            return res;
    }

    public static void main(String[] args) {
        int[] arr = {1,4,6,4,-3,8,-1};
        int n = 7;
        if(n==1){
            System.out.println("Maximum circular sum is " + arr[0]);
        }
        else {
            System.out.println("Maximum circular sum is " + Integer.max(kadane(arr,n),reverseKadane(arr,n)));
        }
    }
}

package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Rearrange an array in maximum minimum form | Set 1
public class Rearrange16 {

    private static void rearrange(int[] arr, int n) {
        int[] temp = arr.clone();
        int small = 0;
        int large = n - 1;
        boolean flag = true;

        for (int i = 0; i < n; i++) {
            if (flag)
                arr[i] = temp[large--];
            else
                arr[i] = temp[small++];
            flag = !flag;
        }
    }

    //Time Complexity: O(n)
    //Auxiliary Space: O(n)
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        int n = arr.length;
        rearrange(arr, n);
        System.out.println(Arrays.toString(arr));
    }
}

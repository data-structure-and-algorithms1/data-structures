package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Maximize sum of consecutive differences in a circular array
public class Rearrange33 {

    private static int maxSum(int[] arr, int n) {
        int sum = 0;
        Arrays.sort(arr);

        for (int i = 0; i < n / 2; i++) {
            sum -= 2 * arr[i];
            sum += 2 * arr[n - i - 1];
        }
        return sum;
    }

    public static void main(String[] args) {
        int[] arr = {4, 2, 1, 8};
        System.out.println(maxSum(arr,arr.length));
    }
}

package com.company.algorithm.array.rearrange;

import java.util.Arrays;

//Segregate even and odd numbers | Set 3
public class Rearrange24 {

    private static int[] segregate(int[] arr, int n) {
        int[] temp = new int[n];

        int j = 0;

        for (int i = 0; i < n; i++) {
            if (arr[i] % 2 == 0) {
                temp[j] = arr[i];
                j++;
            }
        }

        for (int i = 0; i < n; i++) {
            if (arr[i] % 2 != 0) {
                temp[j] = arr[i];
                j++;
            }
        }

        return temp;
    }


    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6};
        System.out.println(Arrays.toString(segregate(arr,arr.length)));
    }
}

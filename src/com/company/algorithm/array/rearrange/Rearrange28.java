package com.company.algorithm.array.rearrange;

//Largest subarray with equal number of 0s and 1s
public class Rearrange28 {

    public static int largestSubarray(int[] arr, int n) {
        int sum = 0;
        int maxSize = -1;
        int startIndex = 0;
        int endIndex = 0;

        for (int i = 0; i < n - 1; i++) {
            sum = (arr[i] == 0) ? -1 : 1;
            for (int j = i + 1; j < n; j++) {
                if (arr[j] == 0)
                    sum += -1;
                else
                    sum += 1;

                if (sum == 0 && maxSize < j - i + 1) {
                    maxSize = j - i + 1;
                    startIndex = i;
                }
            }
        }
        endIndex = startIndex + maxSize - 1;
        if (maxSize == -1) {
            System.out.println("No Such sub array");
        } else
            System.out.println(startIndex + " to " + endIndex);
        return maxSize;
    }

    public static void main(String[] args) {
        int[] arr = { 1, 0, 0, 1, 0, 1, 1 };
        int size = arr.length;

        largestSubarray(arr, size);
    }
}

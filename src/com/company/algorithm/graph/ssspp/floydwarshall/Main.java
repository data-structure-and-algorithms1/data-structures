package com.company.algorithm.graph.ssspp.floydwarshall;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<WeightedNode> nodeList = new ArrayList<>();
        nodeList.add(new WeightedNode("A",0));
        nodeList.add(new WeightedNode("B",1));
        nodeList.add(new WeightedNode("C",2));
        nodeList.add(new WeightedNode("D",3));

        FloydWarshall floydWarshall = new FloydWarshall(nodeList);
        floydWarshall.addWeightedEdge(0,3,1);
        floydWarshall.addWeightedEdge(0,1,8);
        floydWarshall.addWeightedEdge(1,2,1);
        floydWarshall.addWeightedEdge(2,0,4);
        floydWarshall.addWeightedEdge(3,1,2);
        floydWarshall.addWeightedEdge(3,2,9);
        floydWarshall.floydWarshall();
    }
}

package com.company.algorithm.graph.ssspp.floydwarshall;

import java.util.ArrayList;

public class FloydWarshall {
    ArrayList<WeightedNode> nodeList;

    public FloydWarshall(ArrayList<WeightedNode> nodeList) {
        this.nodeList = nodeList;
    }

    public void floydWarshall() {
        int size = nodeList.size();
        int[][] v = new int[size][size];

        for (int i = 0; i < size; i++) {
            WeightedNode first = nodeList.get(i);
            for (int j = 0; j < size; j++) {
                WeightedNode second = nodeList.get(j);
                if (i == j) {
                    v[i][j] = 0;
                } else {
                    v[i][j] = first.weightMap.getOrDefault(second, Integer.MAX_VALUE / 10);
                }
            }
        }
        // end of loop

        // floyd Warshall Algorithm
        for (int k = 0; k < nodeList.size(); k++) {
            for (int i = 0; i < nodeList.size(); i++) {
                for (int j = 0; j < nodeList.size(); j++) {
                    if (v[i][j] > v[i][k] + v[k][j]) {
                        v[i][j] = v[i][k] + v[k][j];
                    }
                }
            }
        }// end of loop

        for (int i = 0; i < size; i++) {
            System.out.print("Printing distance list for node " + nodeList.get(i) + ": ");
            for (int j = 0; j < size; j++) {
                System.out.print(v[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void addWeightedEdge(int i, int j, int d) {
        WeightedNode first = nodeList.get(i);
        WeightedNode second = nodeList.get(j);
        first.neighbors.add(second);
        first.weightMap.put(second, d);
    }
}

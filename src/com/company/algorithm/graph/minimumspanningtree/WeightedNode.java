package com.company.algorithm.graph.minimumspanningtree;

import java.util.ArrayList;
import java.util.HashMap;

public class WeightedNode implements Comparable<WeightedNode> {

    public String name;
    public ArrayList<com.company.algorithm.graph.ssspp.belmanford.WeightedNode> neighbors = new ArrayList<>();
    public HashMap<com.company.algorithm.graph.ssspp.belmanford.WeightedNode, Integer> weightMap = new HashMap<>();
    public boolean isVisited = false;
    public com.company.algorithm.graph.ssspp.belmanford.WeightedNode parent;
    public int distance;
    public int index;
    public DisjointSet set;

    public WeightedNode(String name) {
        this.name = name;
        this.distance = Integer.MAX_VALUE;
    }

    @Override
    public int compareTo(WeightedNode weightedNode) {
        return this.distance - weightedNode.distance;
    }

    @Override
    public String toString() {
        return name;
    }
}

package com.company.algorithm.stack_and_queue.interviews.setoftsack;

public class Main {
    public static void main(String[] args) {
        SetOfStack stack = new SetOfStack(3);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        System.out.println(stack.popAt(0));
        System.out.println(stack.popAt(0));
    }
}

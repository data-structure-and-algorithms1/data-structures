package com.company.algorithm.stack_and_queue.interviews.setoftsack;

public class StackNode {
    StackNode above;
    StackNode below;
    int value;

    StackNode(int value) {
        this.value = value;
    }
}

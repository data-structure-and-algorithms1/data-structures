package com.company.algorithm.stack_and_queue.interviews.queueviastacks;

public class Main {


    public static void main(String[] args) {
        QueueViaStack newQueue = new QueueViaStack();
        newQueue.enqueue(1);
        newQueue.enqueue(2);
        newQueue.enqueue(3);
        newQueue.enqueue(4);
        System.out.println(newQueue.dequeue());
        newQueue.enqueue(5);
        System.out.println(newQueue.dequeue());
    }
}

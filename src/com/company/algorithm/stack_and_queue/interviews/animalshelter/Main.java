package com.company.algorithm.stack_and_queue.interviews.animalshelter;

public class Main {

    public static void main(String[] args) {
        AnimalQueue animalQueue = new AnimalQueue();
        animalQueue.enqueue(new Cat("Kiki"));
        animalQueue.enqueue(new Dog("Riki"));
        animalQueue.enqueue(new Dog("Miki"));
        animalQueue.enqueue(new Cat("Lucy"));
        animalQueue.enqueue(new Cat("Maki"));
        animalQueue.enqueue(new Dog("Bars"));

        System.out.println(animalQueue.dequeueAny().name);
        System.out.println(animalQueue.dequeueDogs().name);
        System.out.println(animalQueue.dequeueCats().name);
    }
}

package com.company.algorithm.stack_and_queue.interviews.animalshelter;

public class Dog extends Animal{

    public Dog(String n) {
        super(n);
    }

    @Override
    public String name() {
        return "Dog: " + name;
    }
}

package com.company.algorithm.stack_and_queue.interviews.animalshelter;

public class Cat extends Animal {
    public Cat(String n) {
        super(n);
    }

    @Override
    public String name() {
        return "Cat: " + name;
    }
}

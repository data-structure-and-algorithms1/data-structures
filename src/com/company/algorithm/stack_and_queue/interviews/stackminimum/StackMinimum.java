package com.company.algorithm.stack_and_queue.interviews.stackminimum;

public class StackMinimum {
    Node top;
    Node min;

    public StackMinimum() {
        top = null;
        min = null;
    }

    public int min(){
        return min.value;
    }

    public void push(int value){
        if(min == null){
            min = new Node(value,min);
        }
        else if( min.value < value){
            min = new Node(min.value, min);
        }
        else {
            min = new Node(value, min);
        }
        top = new Node(value,top);
    }

    public int pop(){
        min = min.next;
        int result = top.value;
        top = top.next;
        return result;
    }

    public static void main(String[] args) {
        StackMinimum stackMinimum = new StackMinimum();
        stackMinimum.push(3);
        stackMinimum.push(2);
        stackMinimum.push(5);
        stackMinimum.push(1);
        System.out.println(stackMinimum.min.value);
        stackMinimum.pop();
        System.out.println(stackMinimum.min.value);
    }
}

package com.company.algorithm.stack_and_queue.interviews.stackminimum;

public class Node {
    int value;
    Node next;

    public Node(int value, Node next) {
        this.value = value;
        this.next = next;
    }
}

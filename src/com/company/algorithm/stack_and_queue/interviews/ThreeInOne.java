package com.company.algorithm.stack_and_queue.interviews;

public class ThreeInOne {
    private int numberOfStacks = 3;
    private int stackCapacity;
    private int[] values;
    private int[] sizes;

    public ThreeInOne(int stackSize) {
        stackCapacity = stackSize;
        values = new int[stackSize * numberOfStacks];
        sizes = new int[numberOfStacks];
    }

    public boolean isFull(int stackNum) {
        return sizes[stackNum] == stackCapacity;
    }

    public boolean isEmpty(int stackNum) {
        return sizes[stackNum] == 0;
    }

    public int indexOfTop(int stackNum) {
        int offset = stackNum * stackCapacity;
        int size = sizes[stackNum];
        return offset + size - 1;
    }

    public void push(int stackNum, int value) {
        if (isFull(stackNum)) {
            System.out.println("the Stack is full");
        } else {
            sizes[stackNum]++;
            values[indexOfTop(stackNum)] = value;
        }
    }

    public int pop(int stackNum) {
        if (isEmpty(stackNum)) {
            System.out.println("the Stack is empty");
            return -1;
        } else {
            int topIndex = indexOfTop(stackNum);
            int value = values[topIndex];
            values[topIndex] = 0;
            sizes[stackNum]--;
            return value;
        }
    }

    public int peek(int stackNum) {
        if (isEmpty(stackNum)) {
            System.out.println("the Stack is empty");
            return -1;
        } else return values[indexOfTop(stackNum)];
    }

    public static void main(String[] args) {
        ThreeInOne threeInOne = new ThreeInOne(3);
        threeInOne.push(0,1);
        threeInOne.push(0,2);
        threeInOne.push(1,4);
        threeInOne.push(1,5);
        threeInOne.push(2,8);
        int result = threeInOne.pop(1);
        System.out.println(result);
        int result2 = threeInOne.peek(1);
        System.out.println(result2);
    }
}

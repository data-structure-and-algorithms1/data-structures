package com.company.algorithm.bigO;

/**
 * Question 1
 *  sum product array and find time complexity
 */
public class SumAndProdFunction {

    public static void main(String[] args) {
        int [] arr = {1,2,3,4,5};
        sumOfProdArray(arr);
    }

    public static void sumOfProdArray(int[] array) {
        int sum = 0;        // ---------  O(1)
        int product = 1;    // ---------  O(1)
        for (int j : array) // ---------  O(N)
            sum += j;       // ---------  O(1)

        for (int j : array) // ---------  O(N)
            product *= j;   // ---------  O(1)

        System.out.println(sum + ", " + product); // ---------  O(1)

        //   O(N)  time complexity
    }

}

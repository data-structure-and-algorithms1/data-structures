package com.company.algorithm.bigO;

// Question 4
public class Question4PairsFromArray {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int[] arr2 = {1, 2, 3};
        printPairs(arr,arr2);
    }

    public static void printPairs(int[] arrayA, int[] arrayB) {
        for (int i = 0; i < arrayA.length; i++) {
            for (int j = i + 1; j < arrayB.length; j++) {
                System.out.println(arrayA[i] + "," + arrayB[j]);
            }
        }
        /**
         * Burda inner for olsada ancaq bu O(N^2) time complexity olmayacaq
         * burda multi oldugu ucun -> O(ab)  time complexity olacaq
         */
    }

}

package com.company.algorithm.bigO;

// Question 3
public class PrintUnOrderedPairsFromArray {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        printPairs(arr);
    }

    public static void printPairs(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                System.out.println(array[i] + "," + array[j]);
            }
        }
        /** counting the iterations
         *  1st ---> n-1
         *  2nd ---> n-2
         *      .
         *      .
         *      1
         *
         *  (n-1)+(n-2)+(n-3)+ .. + 2 + 1
         *  = 1 + 2 + .. + (n-3) + (n-2) + (n-1)
         *  = n(n-1)/2
         *  = n^2/2 - n/2
         *  = n^2
         *
         *  2. Average Work
         *  Outer Loop -N times
         *  inner Loop?
         *
         *  1st ---> 10  \
         *  2nd ---> 9    | = 5 ---> 10/2
         *      .         | n ---> n/2
         *      .
         *
         *  n * n/2 = n^2 / 2 ----> O(N^2)
         */
    }

}

package com.company.algorithm.bigO;

// Question 2
public class PrintPairsFromArray {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        printPairs(arr);
    }

    public static void printPairs(int[] array) {
        for (int i = 0; i < array.length; i++) {     // ------- O(N) her iki for bitlikde O(N^2)
            for (int j = 0; j < array.length; j++) { // ------- O(N)
                System.out.println(array[i] + "," + array[j]); // ------ O(1)
            }
        }
    }

}

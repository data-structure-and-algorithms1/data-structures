package com.company.algorithm.bigO;

import java.util.Arrays;

public class Question6 {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        reverse(arr);
    }

    public static void reverse(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {  // O(N/2) --> O(N)
            int other = array.length - i - 1;         // O(1)
            int temp = array[i];                      // O(1)
            array[i] = array[other];                  // O(1)
            array[other] = temp;                      // O(1)
        }
        System.out.println(Arrays.toString(array));   // O(1)    TIME COMPLEXITY O(N)
    }
}

package com.company.algorithm.bigO;

public class Question5 {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int[] arr2 = {1, 2, 3};
        printPairs(arr,arr2);
    }

    public static void printPairs(int[] arrayA, int[] arrayB) {
        for (int i = 0; i < arrayA.length; i++) {          //
            for (int j = i + 1; j < arrayB.length; j++) {  //  O(ab)
                for (int k = 0; k < 100000; k++) {
                    System.out.println(arrayA[i] + "," + arrayB[j]);
                }
            }
        }

        // Burda 1ci for ve 2 for ayri massivler oldugundan O(ab) olacaq
        // 3 cu for da O(1)  time complexity di
        // indi sual burdakidi aralarindaki umumi time complexity ne qeder olacaq
        // 3cu forda sadece milyona kimi dovr edir constantdir buna gorede O(1) olacaq

    }
}

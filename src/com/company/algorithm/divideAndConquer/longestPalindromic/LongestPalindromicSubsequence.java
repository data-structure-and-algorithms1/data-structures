package com.company.algorithm.divideAndConquer.longestPalindromic;


public class LongestPalindromicSubsequence {
    private int findLPS(String st, int startIndex, int endIndex) {
        if (startIndex > endIndex) return 0;

        if (startIndex == endIndex) return 1;

        int count1 = 0;
        if (st.charAt(startIndex) == st.charAt(endIndex)) {
            count1 = 2 + findLPS(st, startIndex + 1, endIndex - 1);
        }
        int count2 = findLPS(st, startIndex + 1, endIndex);
        int count3 = findLPS(st, startIndex, endIndex - 1);

        return Math.max(count1, Math.max(count2, count3));
    }

    public int findLPS(String st) {
        return findLPS(st, 0, st.length() - 1);
    }

    public static void main(String[] args) {
       LongestPalindromicSubsequence subsequence = new LongestPalindromicSubsequence();
        System.out.println(subsequence.findLPS("cocochanel"));
    }
}

package com.company.algorithm.divideAndConquer.houseRobber;

public class HouseRobber {

    private int maxMoneyRecursive(int[] houseNetWorth, int currentIndex) {
        if (currentIndex >= houseNetWorth.length) {
            return 0;
        }

        int stealCurrentHouse = houseNetWorth[currentIndex] + maxMoneyRecursive(houseNetWorth, currentIndex + 2);
        int skipCurrentHouse = maxMoneyRecursive(houseNetWorth, currentIndex + 1);
        return Math.max(skipCurrentHouse, stealCurrentHouse);
    }

    public int maxMoney(int[] houseNetWorth) {
        return maxMoneyRecursive(houseNetWorth, 0);
    }

    public static void main(String[] args) {
        int[] house = {6, 7, 1, 30, 8, 2, 4};
        HouseRobber robber = new HouseRobber();
        System.out.println(robber.maxMoney(house));
    }
}

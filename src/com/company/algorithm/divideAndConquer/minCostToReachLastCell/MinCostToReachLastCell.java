package com.company.algorithm.divideAndConquer.minCostToReachLastCell;

public class MinCostToReachLastCell {
    public static int findMinCost(int[][] cost, int row, int col) {
        if (row == -1 || col == -1) {
            return Integer.MAX_VALUE;
        }

        if (row == 0 && col == 0) {
            return cost[0][0];
        }

        int minCost1 = findMinCost(cost, row, col - 1);
        int minCost2 = findMinCost(cost, row - 1, col);
        int last = Integer.min(minCost1, minCost2);
        return last + cost[row][col];
    }

    public static void main(String[] args) {
        int[][] cost = {
                {4, 7, 8, 6, 4},
                {6, 7, 3, 9, 2},
                {3, 8, 1, 2, 4},
                {7, 1, 7, 3, 7},
                {2, 9, 8, 9, 3},
        };

        System.out.println(findMinCost(cost, cost.length - 1, cost[0].length - 1));
    }
}

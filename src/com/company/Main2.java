package com.company;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Map;

public class Main2 {

    public static final String FIXED_START_HOUR = "08:59:59";
    public static final String FIXED_END_HOUR = "23:59:59";

    public static final LocalTime FIXED_START_TIME = LocalTime.parse(FIXED_START_HOUR);
    public static final LocalTime FIXED_END_TIME = LocalTime.parse(FIXED_END_HOUR);

    public static void main(String[] args) {

//        LocalDateTime start = LocalDate.now().minusDays(1).atStartOfDay();
//        LocalDateTime end = LocalDateTime.of(LocalDate.now().minusDays(1), LocalTime.MAX);
//
//        System.out.println(start);     .minus(4, ChronoUnit.HOURS)
//        System.out.println(end);

//        long startTime = LocalDate.now(ZoneId.of("Asia/Baku")).minusDays(1).atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli();
//        long endTime = LocalDateTime.now().minusDays(1).toInstant(ZoneOffset.UTC).plus(4, ChronoUnit.HOURS).toEpochMilli();
//
//        long endTime2 = LocalDateTime.now().minusDays(1).withHour(23).withMinute(59).withSecond(59).toInstant(ZoneOffset.UTC).toEpochMilli();
//        System.out.println(startTime);
//        System.out.println(endTime);
//        System.out.println(endTime2);

//        LocalDate start = LocalDate.now(ZoneId.of("Asia/Baku")).withDayOfMonth(1);
//        LocalDate end = LocalDate.now(ZoneId.of("Asia/Baku"));
//
//        System.out.println(start);
//        System.out.println(end);
//
//        int startday = start.getDayOfMonth();
//        int endday = end.getDayOfMonth();
//        System.out.println(startday);
//        System.out.println(endday);
//        for (int i = startday; i <= endday; i++) {
//            System.out.println(start.getDayOfMonth());
//            start = start.plusDays(1);
//
//        }
        //2021-11-02T19:59:59.999
        //   long startTime = LocalDate.now(ZoneId.of(DEFAULT_TIMEZONE)).atStartOfDay().toInstant(ZoneOffset.UTC).minus(4, ChronoUnit.HOURS).toEpochMilli();
        //   long endTime = LocalDateTime.now().toInstant(ZoneOffset.UTC).plus(4, ChronoUnit.HOURS).toEpochMilli();

//        LocalDateTime start = LocalDateTime.of(2021, 11, 1, 20, 0, 0);
//        LocalDateTime end = LocalDateTime.of(2021, 11, 2, 19, 59, 59);
//
//        long startTime = LocalDate.from(start).atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli();
//        System.out.println(startTime);
//        long endTime = end.toInstant(ZoneOffset.UTC).plus(4, ChronoUnit.HOURS).toEpochMilli();
//        System.out.println(endTime);


//        long startTime = LocalDate.now(ZoneId.of("Asia/Baku")).minusMonths(1).withDayOfMonth(1)
//                .atStartOfDay(ZoneId.of("Asia/Baku")).toInstant().toEpochMilli();
//
//        LocalDateTime localDateTime = Instant.ofEpochMilli(startTime).atOffset(ZoneOffset.UTC).toLocalDateTime();
//
//        long endTime = LocalDate.now(ZoneId.of("Asia/Baku")).minusMonths(1).with(TemporalAdjusters.lastDayOfMonth()).atTime(LocalTime.MAX)
//                .atZone(ZoneId.of("Asia/Baku")).toInstant().toEpochMilli();
//
//        LocalDateTime localDateTime2 = Instant.ofEpochMilli(endTime).atOffset(ZoneOffset.UTC).toLocalDateTime();
//
//        System.out.println(localDateTime);
//        System.out.println(localDateTime2);
//
//
//        LocalDateTime test = LocalDateTime.from(LocalDate.now(ZoneOffset.UTC).minusMonths(1).withDayOfMonth(1)
//                .atStartOfDay(ZoneOffset.UTC).minus(4, ChronoUnit.HOURS));
//
//        System.out.println(test);
        //LocalTime localTime = LocalTime.now(ZoneId.of("Asia/Baku"));
//      final String FIXED_START_HOUR = "08:59:59";
//      final String FIXED_END_HOUR = "23:59:59";
//
//        LocalTime localTime = LocalTime.of(23,59,59);
//        LocalTime start = LocalTime.parse(FIXED_START_HOUR);
//        LocalTime end = LocalTime.parse(FIXED_END_HOUR);
//
//        if ((localTime.isAfter(FIXED_START_TIME) && localTime.isBefore(FIXED_END_TIME)) || localTime.equals(FIXED_END_TIME)){
//            System.out.println("OKEY");
//        }

//        Long time = 1630440000000L;
//        LocalDateTime localDateTime = Instant.ofEpochMilli(time).atOffset(ZoneOffset.UTC).atZoneSameInstant(ZoneId.of("Asia/Baku")).toLocalDateTime();
//        System.out.println(localDateTime);

//
//        LocalDateTime localDateTime = LocalDateTime.of(2022, 1, 12, 12, 0, 0);
//
//        System.out.println(localDateTime);
//        long s = localDateTime.toInstant(ZoneOffset.UTC).atZone(ZoneId.of("Asia/Baku")).toInstant().toEpochMilli();
//        System.out.println(s);

        String time = "2021-11-02T19:59:59";
        if(time.contains(".")) {
            time = time.substring(0, time.indexOf("."));
        }
        System.out.println(time);

    }
}

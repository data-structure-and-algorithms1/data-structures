package com.company.dataStructure.array;

import java.util.Arrays;

public class SingleDimensionArray {
    int arr[] = null;

    public SingleDimensionArray(int sizeOfArray){
        arr = new int[sizeOfArray];
        Arrays.fill(arr, Integer.MIN_VALUE);
    }

    public void insert(int pos,int value){
        try{
            if(arr[pos] == Integer.MIN_VALUE){
                arr[pos] = value;
                System.out.println("Succesfully inserted");
            }else {
                System.out.println("This cell is already occupied");
            }
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Invalid index to access array!");
        }
    }

    public void traverseArray(){
        try{
            for (int j : arr) {
                System.out.println(j + " ");
            }
        }catch (Exception e){
            System.out.println("Array no longer exists!");
        }
    }

    public void searchInArray(int value){
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] == value){
                System.out.println("Value is found athe index of: " + i);
                return;
            }
        }
        System.out.println(value + " is not found");
    }

    public void deleteValue(int pos){
        try{
            arr[pos] = Integer.MIN_VALUE;
            System.out.println("The value has been deleted successfully");

        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("The value that is provided is not in the range of array");
        }
    }

    public static void main(String[] args) {
        SingleDimensionArray singleDimensionArray = new SingleDimensionArray(5);
        singleDimensionArray.insert(0,10);
        singleDimensionArray.insert(1,20);
        singleDimensionArray.insert(2,30);
        singleDimensionArray.insert(3,40);
        singleDimensionArray.insert(4,50);

        System.out.println(Arrays.toString(singleDimensionArray.arr));
        singleDimensionArray.traverseArray();

        singleDimensionArray.searchInArray(20);
        singleDimensionArray.deleteValue(1);
        System.out.println(Arrays.toString(singleDimensionArray.arr));
    }
}

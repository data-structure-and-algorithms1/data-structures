package com.company.dataStructure.array;

import java.util.Arrays;

public class TwoDimensionalArray {
    private int[][] arr = null;

    public TwoDimensionalArray(int numberOfRows, int numberOfColumns) {
        this.arr = new int[numberOfRows][numberOfColumns];
        for (int row = 0; row < numberOfRows; row++) {
            for (int col = 0; col < numberOfColumns; col++) {
                arr[row][col] = Integer.MIN_VALUE;
            }
        }
    }

    public void insertValueInTheArray(int row, int col, int value){
        try{
            if(arr[row][col] == Integer.MIN_VALUE){
                arr[row][col] = value;
                System.out.println("Successfully Inserted");
            }
            else {
                System.out.println("this cell is already occupied");
            }
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("invalid index for 2D array");
        }
    }

    public void accessCell(int row,int col){
        System.out.println("Accessing Row#" + row + " , Col#" + col);
        try{
            System.out.println("Cell value is: " + arr[row][col]);
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Invalid index for 2D array");
        }
    }

    public static void main(String[] args) {
        TwoDimensionalArray array = new TwoDimensionalArray(3,3);
        array.insertValueInTheArray(0,0,10);
        array.insertValueInTheArray(0,1,20);
        array.insertValueInTheArray(0,2,30);
        array.insertValueInTheArray(1,0,40);
        array.insertValueInTheArray(1,1,50);
        array.insertValueInTheArray(1,2,60);
        array.insertValueInTheArray(2,0,70);
        array.insertValueInTheArray(2,1,80);
        array.insertValueInTheArray(2,2,90);

        System.out.println(Arrays.deepToString(array.arr));

        array.accessCell(0,2);
    }
}

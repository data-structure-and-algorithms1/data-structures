package com.company.dataStructure.binaryheap;

public class Main {

    public static void main(String[] args) {
        BinaryHeap binaryHeap = new BinaryHeap(10);
        binaryHeap.insert(10,"Min");
        binaryHeap.insert(5,"Min");
        binaryHeap.insert(15,"Min");
        binaryHeap.insert(1,"Min");
        binaryHeap.insert(7,"Min");
        binaryHeap.insert(12,"Min");
        binaryHeap.levelOrder();
        binaryHeap.extractHeadOfBP("Min");
        binaryHeap.levelOrder();
        binaryHeap.extractHeadOfBP("Min");
        binaryHeap.levelOrder();
        binaryHeap.extractHeadOfBP("Min");
        binaryHeap.levelOrder();

    }
}

package com.company.dataStructure.queue.usingarray;

public class Main {

    public static void main(String[] args) {
        QueueArray queueArray = new QueueArray(3);
        System.out.println(queueArray.isFull());
        System.out.println(queueArray.isEmpty());
        queueArray.enQueue(1);
        queueArray.enQueue(2);
        queueArray.enQueue(3);
        queueArray.enQueue(4);

        System.out.println(queueArray.deQueue());
        System.out.println(queueArray.peek());
    }
}

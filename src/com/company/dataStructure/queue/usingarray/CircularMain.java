package com.company.dataStructure.queue.usingarray;

public class CircularMain {
    public static void main(String[] args) {
        CircularQueue circularQueue = new CircularQueue(3);
        System.out.println(circularQueue.isEmpty());
        System.out.println(circularQueue.isFull());
        circularQueue.enQueue(1);
        circularQueue.enQueue(2);
        circularQueue.enQueue(3);
        circularQueue.enQueue(4);
        circularQueue.enQueue(5);

//        System.out.println(circularQueue.deQueue());
//        System.out.println(circularQueue.deQueue());

        System.out.println(circularQueue.peek());
        circularQueue.deleteQueue();

    }
}

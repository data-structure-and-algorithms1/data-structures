package com.company.dataStructure.queue.usinglinkedlist;

public class MainQueueList {

    public static void main(String[] args) {
        QueueLinkedList queueLinkedList = new QueueLinkedList();
        queueLinkedList.enQueue(1);
        queueLinkedList.enQueue(2);
        queueLinkedList.enQueue(3);
        queueLinkedList.enQueue(4);

        System.out.println(queueLinkedList.isEmpty());

        System.out.println(queueLinkedList.deQueue());
        System.out.println(queueLinkedList.deQueue());
        queueLinkedList.deleteQueue();
    }
}

package com.company.dataStructure.queue.usinglinkedlist;

import com.company.dataStructure.linkedlist.singlylinkedlist.SinglyLinkedList;

public class QueueLinkedList {
    SinglyLinkedList linkedList;

    public QueueLinkedList() {
        linkedList = new SinglyLinkedList();
        System.out.println("The Queue is successfully created");
    }

    public boolean isEmpty() {
        return linkedList.head == null;
    }

    public void enQueue(int value) {
        linkedList.insertLinkedList(value, linkedList.size);
        System.out.println("Successfully inserted " + value + " in teh queue");
    }

    public int deQueue() {
        int value = -1;
        if (isEmpty()) {
            System.out.println("The Queue is empty");
        } else {
            value = linkedList.head.value;
            linkedList.deletionOfNode(0);
        }
        return value;
    }

    public int peek() {
        if (isEmpty()) {
            System.out.println("The Queue is empty");
            return -1;
        } else {
            return linkedList.head.value;
        }
    }

    public void deleteQueue(){
        linkedList.head = null;
        linkedList.tail = null;
        System.out.println("The Queue is successfully deleted");
    }
}

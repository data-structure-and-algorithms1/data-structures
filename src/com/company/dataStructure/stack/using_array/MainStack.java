package com.company.dataStructure.stack.using_array;

public class MainStack {

    public static void main(String[] args) {
        Stack stack = new Stack(4);
//        System.out.println(stack.isEmpty());
//        System.out.println(stack.isFull());
//        stack.push(1);
//        stack.push(2);
//        stack.push(3);
//        stack.push(4);
//        stack.push(5);
//        System.out.println(stack.isEmpty());
//        System.out.println(stack.isFull());

        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);

        int result = stack.pop();
        System.out.println(result);

        System.out.println(stack.peek());
        System.out.println(stack.peek());

        stack.deleteStack();
    }
}

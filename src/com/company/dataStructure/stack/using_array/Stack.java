package com.company.dataStructure.stack.using_array;

public class Stack {
    int[] arr;
    int topOffStack;

    public Stack(int size) {
        this.arr = new int[size];
        this.topOffStack = -1;
        System.out.println("The Stacked is created with size of: " + size);
    }

    public boolean isEmpty() {
        return topOffStack == -1;
    }

    public boolean isFull() {
        return topOffStack == arr.length - 1;
    }

    public void push(int value) {
        if (isFull()) {
            System.out.println("The stack is full!");
        } else {
            arr[topOffStack + 1] = value;
            topOffStack++;
            System.out.println("The value is successfully inserted");
        }
    }

    public int pop() {
        if (isEmpty()) {
            System.out.println("The stack is empty!");
            return -1;
        }
        else{
            int topStack = arr[topOffStack];
            topOffStack--;
            return topStack;
        }
    }

    public int peek() {
        if (isEmpty()) {
            System.out.println("The stack is empty!");
            return -1;
        }
        else{
            return arr[topOffStack];
        }
    }

    public void deleteStack(){
        arr = null;
        System.out.println("Stack is successfully deleted");
    }
}

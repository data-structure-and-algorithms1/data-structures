package com.company.dataStructure.stack.using_linkedlist;

import com.company.dataStructure.linkedlist.singlylinkedlist.SinglyLinkedList;

public class Stack {
    public SinglyLinkedList linkedList;

    public Stack() {
        linkedList = new SinglyLinkedList();
    }

    public void push(int value) {
        linkedList.insertLinkedList(value, 0);
        System.out.println("Inserted " + value + " in Stack");
    }

    public boolean isEmpty() {
        return linkedList.head == null;
    }

    public int pop() {
        int result = -1;
        if(isEmpty()) {
            System.out.println("Stack is empty");
        }
        else {
            result = linkedList.head.value;
            linkedList.deletionOfNode(0);
        }
        return result;
    }

    public int peek(){
        int result = -1;
        if(isEmpty()) {
            System.out.println("Stack is empty");
        }
        else {
            result = linkedList.head.value;
        }
        return result;
    }

    public void deleteStack(){
        linkedList.head = null;
        System.out.println("The Stack is deleted");
    }
}

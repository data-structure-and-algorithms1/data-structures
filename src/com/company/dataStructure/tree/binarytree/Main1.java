package com.company.dataStructure.tree.binarytree;

public class Main1 {
    public static void main(String[] args) {
        BinaryTreeLL binaryTreeLL = new BinaryTreeLL();
        binaryTreeLL.insert("N1");
        binaryTreeLL.insert("N2");
        binaryTreeLL.insert("N3");
        binaryTreeLL.insert("N4");
        binaryTreeLL.insert("N5");
        binaryTreeLL.insert("N6");
        binaryTreeLL.levelOrder();
        binaryTreeLL.deleteNode("N3");
        binaryTreeLL.levelOrder();
    }
}

package com.company.dataStructure.tree.avltree;

public class Main {

    public static void main(String[] args) {
        AvlTree avlTree = new AvlTree();
//        avlTree.insert(10);
//        avlTree.insert(15);
//        avlTree.insert(7);
//        avlTree.insert(20);
//        avlTree.insert(25);
//        avlTree.insert(4);
//        avlTree.insert(30);
//        avlTree.insert(32);
//        avlTree.insert(27);
        avlTree.insert(10);
        avlTree.insert(20);
        avlTree.insert(15);
        avlTree.insert(30);
        avlTree.insert(35);
        avlTree.insert(25);
        avlTree.insert(5);
        avlTree.insert(8);
        avlTree.insert(3);
        avlTree.insert(18);
        avlTree.levelOrder();
    }
}

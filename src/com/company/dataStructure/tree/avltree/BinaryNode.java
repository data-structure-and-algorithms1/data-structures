package com.company.dataStructure.tree.avltree;

public class BinaryNode {
    int value;
    int height;
    BinaryNode left;
    BinaryNode right;

    public BinaryNode() {
        this.height = 0;
    }
}

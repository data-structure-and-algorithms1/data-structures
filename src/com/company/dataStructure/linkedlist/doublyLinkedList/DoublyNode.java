package com.company.dataStructure.linkedlist.doublyLinkedList;

public class DoublyNode {
    public int value;
    public DoublyNode prev;
    public DoublyNode next;
}

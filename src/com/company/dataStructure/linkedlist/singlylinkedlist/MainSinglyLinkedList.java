package com.company.dataStructure.linkedlist.singlylinkedlist;

public class MainSinglyLinkedList {
    public static void main(String[] args) {
        SinglyLinkedList sll = new SinglyLinkedList();
        sll.createSinglyLinkedList(5);
        sll.insertLinkedList(6,1);
        sll.insertLinkedList(7,2);
        sll.insertLinkedList(8,3);
        sll.insertLinkedList(9,3);
        sll.traverseSinglyLinkedList();
//        sll.deletionOfNode(3);
//        sll.traverseSinglyLinkedList();
//        sll.deleteSLL();
//        sll.traverseSinglyLinkedList();
    }
}

package com.company.dataStructure.linkedlist.circularDoublyLinkedList;

public class MainCircularDoublyLinkedList {

    public static void main(String[] args) {
        CircularDoublyLinkedList cdll = new CircularDoublyLinkedList();
        cdll.createCDLL(1);
        cdll.insertNode(2,1);
        cdll.insertNode(3,2);
        cdll.insertNode(4,3);
        cdll.insertNode(5,4);
        cdll.traverseCDLL();
        cdll.reverseTraverseCDLL();
    }
}

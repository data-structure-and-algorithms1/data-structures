package com.company.dataStructure.linkedlist.circularDoublyLinkedList;

public class DoublyNode {
    public int value;
    public DoublyNode prev;
    public DoublyNode next;
}

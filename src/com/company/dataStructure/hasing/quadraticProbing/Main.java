package com.company.dataStructure.hasing.quadraticProbing;

public class Main {

    public static void main(String[] args) {
        QuadraticProbing quadraticProbing = new QuadraticProbing(16);
        quadraticProbing.insertKeyInHashTable("ABC");
        quadraticProbing.insertKeyInHashTable("ABCD");
        quadraticProbing.insertKeyInHashTable("ABCDE");
        quadraticProbing.insertKeyInHashTable("CAB");
        quadraticProbing.insertKeyInHashTable("BDEA");
        quadraticProbing.display();
    }
}

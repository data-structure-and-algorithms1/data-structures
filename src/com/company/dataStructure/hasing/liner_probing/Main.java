package com.company.dataStructure.hasing.liner_probing;

public class Main {

    public static void main(String[] args) {
        LinearProbing linearProbing = new LinearProbing(16);
        linearProbing.insertInHashTable("Natiq");
        linearProbing.insertInHashTable("Ali");
        linearProbing.insertInHashTable("Muzaffer");
        linearProbing.insertInHashTable("Elgun");
        linearProbing.insertInHashTable("Lola");
        linearProbing.insertInHashTable("Sofiya");
        linearProbing.insertInHashTable("Aliye");
        linearProbing.insertInHashTable("Intiqam");
        linearProbing.insertInHashTable("Leman");
        linearProbing.insertInHashTable("Malik");
        linearProbing.insertInHashTable("Ilham");
        linearProbing.insertInHashTable("Elchin");
        linearProbing.insertInHashTable("Niyaz");
        linearProbing.insertInHashTable("Leyla");
        linearProbing.insertInHashTable("Cavansir");
        linearProbing.insertInHashTable("Meleyke");
        linearProbing.displayHashTable();

    }
}

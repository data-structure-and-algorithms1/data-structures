package com.company.dataStructure.hasing.directchain;

public class Main {

    public static void main(String[] args) {
        DirectChaining directChaining = new DirectChaining(16);
        directChaining.insertHashTable("Elchin");
        directChaining.insertHashTable("Elgun");
        directChaining.insertHashTable("Namiq");
        directChaining.insertHashTable("Saleh");
        directChaining.insertHashTable("Intiqam");
        directChaining.displayHashTable();

    }
}

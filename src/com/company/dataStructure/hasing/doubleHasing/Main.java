package com.company.dataStructure.hasing.doubleHasing;

public class Main {
    public static void main(String[] args) {
        DoubleHashing doubleHashing = new DoubleHashing(16);
        doubleHashing.insertKeyInHashTable("ABC");
        doubleHashing.insertKeyInHashTable("ABCD");
        doubleHashing.insertKeyInHashTable("ABCDE");
        doubleHashing.insertKeyInHashTable("BCA");
        doubleHashing.insertKeyInHashTable("BDEQ");
        doubleHashing.insertKeyInHashTable("SPOQ");
        doubleHashing.displayHashTable();
    }
}

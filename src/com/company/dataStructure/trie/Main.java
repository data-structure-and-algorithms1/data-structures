package com.company.dataStructure.trie;

public class Main {

    public static void main(String[] args) {
        Trie trie = new Trie();
        trie.insert("CAT");
        trie.insert("CAR");
        trie.insert("LAMP");
       // trie.search("CAR");
        trie.delete("CAR");
        System.out.println(trie);

    }
}
